# Welcome

Welcome to the CSP engine, a simple CSP problem sovler developed in Java. It is
composed of several modules : the [constraint module](constraint/README.md)
define the core mechanisms to solve constraint problems. the
[scheduler module](scheduler/README.md) is an extension of the CSP module
that solves scheduling problems.

# License

All modules are licensed by Laurent COCAULT under the Apache License Version 2.0.
A copy of this license is provided in the LICENSE.txt file of each module.

