# Scope

CSP (Constraint Satisfaction Problem) constraint is a free java library
providing basic constraint satisfaction problem solving mechanisms.

# License

It is licensed by Laurent COCAULT under the Apache License Version 2.0. A copy
of this license is provided in the LICENSE.txt file.

# Content

The src/main/java directory contains the library sources.
The src/main/resources directory contains the library data.
The src/test/java directory contains the tests sources.
The src/test/resources directory contains the tests data.
The src/examples/java directory contains sources for example use of the library.
The src/example/resources directory contains example data.

# Dependencies

CSP constraint relies on the following free software, all released under
business friendly free licenses.

test-time dependency:

  - JUnit 4.12 from Erich Gamma and Kent Beck
    http://www.junit.org/
    released under the Common Public License Version 1.0
