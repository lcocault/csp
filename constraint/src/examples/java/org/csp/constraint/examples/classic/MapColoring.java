/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.examples.classic;

/* ------------------------------------------------------------

 Problem Description
 -------------------

 A map-coloring problem involves choosing colors for the 
 countries on a map in such a way that at most four colors are 
 used and no two neighboring countries are the same color.  

 For our example, we will consider six countries: Belgium, 
 Denmark, France, Germany, Netherlands, and Luxembourg.  The 
 colors can be blue, white, red or green.

 ------------------------------------------------------------ */

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.general.NotEqual;
import org.csp.constraint.model.integer.EqualToMaxOfIntArray;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.solver.BasicSolver;
import org.csp.constraint.solver.IntegerProblem;
import org.csp.constraint.solver.SingleThreadIntegerProblem;
import org.csp.constraint.solver.Solver;
import org.csp.constraint.solver.SolverDotRenderer;

public class MapColoring {

    // Color index association
    private static String[] Names = {"blue", "white", "red", "green", "yellow"};

    public static void main(String[] args) {

        // --------------------//
        // Problem definition //
        // --------------------//

        IntegerProblem pb = new SingleThreadIntegerProblem();

        // Variables
        IntVar belgium = pb.addVariable(0, 4, "Belgium");
        IntVar denmark = pb.addVariable(0, 4, "Denmark");
        IntVar france = pb.addVariable(0, 4, "France");
        IntVar germany = pb.addVariable(0, 4, "Germany");
        IntVar netherlands = pb.addVariable(0, 4, "Netherlands");
        IntVar luxembourg = pb.addVariable(0, 4, "Luxembourg");
        IntVar suisse = pb.addVariable(0, 4, "Suisse");
        // Constraints
        pb.add(new NotEqual<IntValue>(belgium, france));
        pb.add(new NotEqual<IntValue>(france, luxembourg));
        pb.add(new NotEqual<IntValue>(france, germany));
        pb.add(new NotEqual<IntValue>(denmark, germany));
        pb.add(new NotEqual<IntValue>(belgium, netherlands));
        pb.add(new NotEqual<IntValue>(germany, netherlands));
        pb.add(new NotEqual<IntValue>(luxembourg, germany));
        pb.add(new NotEqual<IntValue>(luxembourg, belgium));
        pb.add(new NotEqual<IntValue>(belgium, germany));
        pb.add(new NotEqual<IntValue>(suisse, germany));
        pb.add(new NotEqual<IntValue>(suisse, france));

        // Render the problem
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        SolverDotRenderer<IntValue> renderer = new SolverDotRenderer<IntValue>();
        renderer.render(pb, os);
        System.out.println(new String(os.toByteArray()));

        // Basic solver
        Solver<IntValue> solver = new BasicSolver<IntValue>(pb);

        // Search for a solution
        if (pb.startSearch() &&
                solver.branchAndBound() == Solver.SearchResult.SOLUTION_FOUND) {

            // Values
            IntValue b = (IntValue) belgium.getValue();
            IntValue d = (IntValue) denmark.getValue();
            IntValue f = (IntValue) france.getValue();
            IntValue g = (IntValue) germany.getValue();
            IntValue n = (IntValue) netherlands.getValue();
            IntValue l = (IntValue) luxembourg.getValue();
            IntValue s = (IntValue) suisse.getValue();

            // Display result
            System.out.println("One solution");
            System.out.println("Belgium:     " + Names[b.getIntValue()]);
            System.out.println("Denmark:     " + Names[d.getIntValue()]);
            System.out.println("France:      " + Names[f.getIntValue()]);
            System.out.println("Germany:     " + Names[g.getIntValue()]);
            System.out.println("Netherlands: " + Names[n.getIntValue()]);
            System.out.println("Luxembourg:  " + Names[l.getIntValue()]);
            System.out.println("Suisse:      " + Names[s.getIntValue()]);

            // Check assertions
            if (b.getIntValue() == f.getIntValue() ||
                    d.getIntValue() == g.getIntValue() ||
                    g.getIntValue() == f.getIntValue() ||
                    b.getIntValue() == n.getIntValue() ||
                    g.getIntValue() == n.getIntValue() ||
                    f.getIntValue() == l.getIntValue() ||
                    l.getIntValue() == g.getIntValue() ||
                    l.getIntValue() == b.getIntValue() ||
                    b.getIntValue() == g.getIntValue() ||
                    s.getIntValue() == g.getIntValue() ||
                    s.getIntValue() == f.getIntValue()) {
                System.err.println("Assertions failed");
            }

        } else {
            System.err.println("No solution found");
        }

        // Stop the search
        pb.stopSearch();

        // Add a criterion
        IntVar crit = (IntVar) pb.addVariable("Max");
        List<IntVar> array = new ArrayList<IntVar>();
        array.add(belgium);
        array.add(denmark);
        array.add(france);
        array.add(germany);
        array.add(netherlands);
        array.add(luxembourg);
        array.add(suisse);
        pb.add(new EqualToMaxOfIntArray(crit, array));

        if (pb.startSearch() &&
                solver.branchAndBound(crit) == Solver.SearchResult.SOLUTION_FOUND) {

            // Values
            IntValue b = (IntValue) belgium.getValue();
            IntValue d = (IntValue) denmark.getValue();
            IntValue f = (IntValue) france.getValue();
            IntValue g = (IntValue) germany.getValue();
            IntValue n = (IntValue) netherlands.getValue();
            IntValue l = (IntValue) luxembourg.getValue();
            IntValue s = (IntValue) suisse.getValue();

            // Display result
            System.out.println("Best solution");
            System.out.println("Belgium:     " + Names[b.getIntValue()]);
            System.out.println("Denmark:     " + Names[d.getIntValue()]);
            System.out.println("France:      " + Names[f.getIntValue()]);
            System.out.println("Germany:     " + Names[g.getIntValue()]);
            System.out.println("Netherlands: " + Names[n.getIntValue()]);
            System.out.println("Luxembourg:  " + Names[l.getIntValue()]);
            System.out.println("Suisse:      " + Names[s.getIntValue()]);

            // Check assertions
            if (b.getIntValue() == f.getIntValue() ||
                    d.getIntValue() == g.getIntValue() ||
                    g.getIntValue() == f.getIntValue() ||
                    b.getIntValue() == n.getIntValue() ||
                    g.getIntValue() == n.getIntValue() ||
                    f.getIntValue() == l.getIntValue() ||
                    l.getIntValue() == g.getIntValue() ||
                    l.getIntValue() == b.getIntValue() ||
                    b.getIntValue() == g.getIntValue() ||
                    s.getIntValue() == g.getIntValue() ||
                    s.getIntValue() == f.getIntValue()) {
                System.err.println("Assertions failed");
            }

            // Stop the search
            pb.stopSearch();

        } else {
            System.err.println("No solution found");
        }
    }
}
