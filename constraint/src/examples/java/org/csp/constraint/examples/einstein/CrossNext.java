/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.examples.einstein;

import java.util.List;

import org.csp.constraint.model.Value;
import org.csp.constraint.model.Variable;
import org.csp.constraint.model.general.CrossEqual;

/**
 * Variant of the CrossEqual specifying the "lives next" in the Einstein's
 * Riddle.
 */
public class CrossNext<T> extends CrossEqual<T> {

	/**
	 * Constructor.
	 * 
	 * @param leftArray
	 *            First array
	 * @param leftValue
	 *            Expected value for the first array
	 * @param rightArray
	 *            Second array
	 * @param rightValue
	 *            Expected value for the second array
	 */
	public CrossNext(List<Variable<T>> leftArray, Value<T> leftValue,
			List<Variable<T>> rightArray, Value<T> rightValue) {
		super(leftArray, leftValue, rightArray, rightValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void propagate(List<Variable<T>> from, Value<T> fromValue,
			List<Variable<T>> to, Value<T> toValue) {

		// Iterate with the index to find the corresponding variable in "to"
		for (int index = 0; index < from.size(); index++) {

			// Get the "from" variable
			Variable<T> var = from.get(index);

			// Get the potentially affected "to" variables
			Variable<T> left = null;
			Variable<T> right = null;
			if (index > 0) {
				left = to.get(index - 1);
			}
			if (index < from.size() - 1) {
				right = to.get(index + 1);
			}

			if (var.isBound() && var.getValue().equals(fromValue)
					&& (right == null || !right.contains(toValue))
					&& left != null) {
				// If the variable has the expected value and the associated
				// right variable cannot have the other expected value, then the
				// other expected value is for the left one.
				left.setValue(toValue);
			}
			if (var.isBound() && var.getValue().equals(fromValue)
					&& (left == null || !left.contains(toValue))
					&& right != null) {
				// If the variable has the expected value and the associated
				// left variable cannot have the other expected value, then the
				// other expected value is for the right one.
				right.setValue(toValue);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "LIVE-NEXT-" + super.toString();
	}

}
