/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.examples.einstein;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.csp.constraint.model.UnsizedConstraint;
import org.csp.constraint.model.Value;
import org.csp.constraint.model.Variable;

/**
 * Variant of the CrossEqual specifying the "is to the left" in the Einstein's
 * Riddle.
 */
public class CrossLeft<T> extends UnsizedConstraint<T> {
    /** Left array. */
    private List<Variable<T>> left_;

    /** Left value. */
    private Value<T> leftValue_;

    /** Right array. */
    private List<Variable<T>> right_;

    /** Right value. */
    private Value<T> rightValue_;

    /**
     * Constructor.
     * 
     * @param leftArray
     *            First array
     * @param leftValue
     *            Expected value for the first array
     * @param rightArray
     *            Second array
     * @param rightValue
     *            Expected value for the second array
     */
    public CrossLeft(List<Variable<T>> leftArray, Value<T> leftValue,
            List<Variable<T>> rightArray, Value<T> rightValue) {
        super("CROSS_EQUAL", new ArrayList<Variable<T>>());
        // Initialize attributes
        left_ = leftArray;
        leftValue_ = leftValue;
        right_ = rightArray;
        rightValue_ = rightValue;
        // Both vector variables are implied in the constraint
        getVariables().addAll(leftArray);
        getVariables().addAll(rightArray);
        // Add constraints
        for (Variable<T> var : getVariables()) {
            var.addNAryConstraint(this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propagate() {

        // Iterate with the index to find the corresponding variable in "right"
        for (int index = 0; index < left_.size() - 1; index++) {

            // Get the "left" variable
            Variable<T> var = left_.get(index);

            if (var.isBound() && var.getValue().equals(leftValue_)) {

                // When the "left" variable is bound with the expected value,
                // the corresponding "right" variable should also be bound.
                if (right_.get(index + 1).setValue(rightValue_)) {
                    addRecentChangedVariable(right_.get(index + 1));
                }
            } else if (!var.contains(leftValue_)) {

                // When the expected value is no more in the "left" variable
                // domain, the corresponding "right" value must not be in the
                // domain of the "right" variable.
                if (right_.get(index + 1).removeValue(rightValue_)) {
                    addRecentChangedVariable(right_.get(index + 1));
                }
            }
        }

        // Iterate with the index to find the corresponding variable in "left"
        for (int index = 1; index < right_.size(); index++) {

            // Get the "left" variable
            Variable<T> var = right_.get(index);

            if (var.isBound() && var.getValue().equals(rightValue_)) {

                // When the "right" variable is bound with the expected value,
                // the corresponding "left" variable should also be bound.
                if (left_.get(index - 1).setValue(leftValue_)) {
                    addRecentChangedVariable(left_.get(index - 1));
                }
            } else if (!var.contains(rightValue_)) {

                // When the expected value is no more in the "right" variable
                // domain, the corresponding "left" value must not be in the
                // domain of the "left" variable.
                if (left_.get(index - 1).removeValue(leftValue_)) {
                    addRecentChangedVariable(left_.get(index - 1));
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {

        // String builder
        StringBuilder builder = new StringBuilder();

        // Display the constraint
        builder.append("CROSS_LEFT(" + leftValue_ + " in (");
        Iterator<Variable<T>> vars = left_.iterator();
        while (vars.hasNext()) {
            // Display every variable
            builder.append(vars.next().getName());
            if (vars.hasNext()) {
                // Next variable
                builder.append(", ");
            } else {
                // Last variable
                builder.append(") equals (" + rightValue_ + " in (");
            }
        }
        vars = right_.iterator();
        while (vars.hasNext()) {
            // Display every variable
            builder.append(vars.next().getName());
            if (vars.hasNext()) {
                // Next variable
                builder.append(", ");
            } else {
                // Last variable
                builder.append("))");
            }
        }

        return builder.toString();
    }

}
