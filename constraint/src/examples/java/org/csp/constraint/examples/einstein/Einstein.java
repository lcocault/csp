/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.examples.einstein;

/* ------------------------------------------------------------

 Problem Description
 -------------------

 There are 5 houses that are each a different color.
 There is a person of a different nation in each house.
 The 5 owners drink a certain drink. They each smoke a certain brand of
 cigarettes and also have a certain pet. No owner has the same pet, smokes the
 same brand of cigarettes nor drinks the same drink.

 The question is. "Who has the fish?"

 CLUES

 1. The British man lives in the red house.
 2. The Swedish man has a dog for a pet.
 3. The Danish man drinks tea.
 4. The green house is to the left of the white house.
 5. The owner of the green house drinks coffee.
 6. The person that smokes Pall Mall has a bird.
 7. The owner of the yellow house smokes Dunhill.
 8. The person that lives in the middle house drinks milk.
 9. The Norwegian lives in the first house.
 10. The person that smokes Blend, lives next to the one that has a cat.
 11. The person that has a horse lives next to the one that smokes Dunhill.
 12. The one that smokes Bluemaster drinks beer.
 13. The German smokes Prince.
 14. The Norwegian lives next to a blue house.
 15. The person that smokes Blend, has a neighbour that drinks water.

 ------------------------------------------------------------ */

import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.Variable;
import org.csp.constraint.model.general.AllDiff;
import org.csp.constraint.model.general.CrossEqual;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.solver.BasicSolver;
import org.csp.constraint.solver.IntegerProblem;
import org.csp.constraint.solver.SingleThreadIntegerProblem;
import org.csp.constraint.solver.Solver;

public class Einstein {

	// Color index association
	private static String[] COLOR = { "blue", "green", "red", "white", "yellow" };
	private static IntValue BLUE = new IntValue(0);
	private static IntValue GREEN = new IntValue(1);
	private static IntValue RED = new IntValue(2);
	private static IntValue WHITE = new IntValue(3);
	private static IntValue YELLOW = new IntValue(4);

	// Drink index association
	private static String[] DRINK = { "beer", "coffee", "milk", "tea", "water" };
	private static IntValue BEER = new IntValue(0);
	private static IntValue COFFEE = new IntValue(1);
	private static IntValue MILK = new IntValue(2);
	private static IntValue TEA = new IntValue(3);
	private static IntValue WATER = new IntValue(4);

	// Nation index association
	private static String[] NATION = { "british", "danish", "german",
			"norwegian", "swedish" };
	private static IntValue BRITISH = new IntValue(0);
	private static IntValue DANISH = new IntValue(1);
	private static IntValue GERMAN = new IntValue(2);
	private static IntValue NORWEGIAN = new IntValue(3);
	private static IntValue SWEDISH = new IntValue(4);

	// Pet index association
	private static String[] PET = { "bird", "cat", "dog", "fish", "horse" };
	private static IntValue BIRD = new IntValue(0);
	private static IntValue CAT = new IntValue(1);
	private static IntValue DOG = new IntValue(2);
	// private static IntValue FISH = new IntValue(3);
	private static IntValue HORSE = new IntValue(4);

	// Smoke index association
	private static String[] SMOKE = { "blend", "blue master", "dunhill",
			"pall mall", "prince" };
	private static IntValue BLEND = new IntValue(0);
	private static IntValue BLUEMASTER = new IntValue(1);
	private static IntValue DUNHILL = new IntValue(2);
	private static IntValue PALLMALL = new IntValue(3);
	private static IntValue PRINCE = new IntValue(4);

	public static void main(String args[]) {

		// -------------------//
		// Problem definition //
		// -------------------//

		IntegerProblem pb = new SingleThreadIntegerProblem();

		// Colors
		IntVar color1 = pb.addVariable(0, 4, "Color1");
		IntVar color2 = pb.addVariable(0, 4, "Color2");
		IntVar color3 = pb.addVariable(0, 4, "Color3");
		IntVar color4 = pb.addVariable(0, 4, "Color4");
		IntVar color5 = pb.addVariable(0, 4, "Color5");
		List<Variable<IntValue>> colors = new ArrayList<Variable<IntValue>>();
		colors.add(color1);
		colors.add(color2);
		colors.add(color3);
		colors.add(color4);
		colors.add(color5);

		// Drinks
		IntVar drink1 = pb.addVariable(0, 4, "Drink1");
		IntVar drink2 = pb.addVariable(0, 4, "Drink2");
		IntVar drink3 = pb.addVariable(0, 4, "Drink3");
		IntVar drink4 = pb.addVariable(0, 4, "Drink4");
		IntVar drink5 = pb.addVariable(0, 4, "Drink5");
		List<Variable<IntValue>> drinks = new ArrayList<Variable<IntValue>>();
		drinks.add(drink1);
		drinks.add(drink2);
		drinks.add(drink3);
		drinks.add(drink4);
		drinks.add(drink5);

		// Nations
		IntVar nation1 = pb.addVariable(0, 4, "Nation1");
		IntVar nation2 = pb.addVariable(0, 4, "Nation2");
		IntVar nation3 = pb.addVariable(0, 4, "Nation3");
		IntVar nation4 = pb.addVariable(0, 4, "Nation4");
		IntVar nation5 = pb.addVariable(0, 4, "Nation5");
		List<Variable<IntValue>> nations = new ArrayList<Variable<IntValue>>();
		nations.add(nation1);
		nations.add(nation2);
		nations.add(nation3);
		nations.add(nation4);
		nations.add(nation5);

		// Pets
		IntVar pet1 = pb.addVariable(0, 4, "Pet1");
		IntVar pet2 = pb.addVariable(0, 4, "Pet2");
		IntVar pet3 = pb.addVariable(0, 4, "Pet3");
		IntVar pet4 = pb.addVariable(0, 4, "Pet4");
		IntVar pet5 = pb.addVariable(0, 4, "Pet5");
		List<Variable<IntValue>> pets = new ArrayList<Variable<IntValue>>();
		pets.add(pet1);
		pets.add(pet2);
		pets.add(pet3);
		pets.add(pet4);
		pets.add(pet5);

		// Smokes
		IntVar smoke1 = pb.addVariable(0, 4, "Smoke1");
		IntVar smoke2 = pb.addVariable(0, 4, "Smoke2");
		IntVar smoke3 = pb.addVariable(0, 4, "Smoke3");
		IntVar smoke4 = pb.addVariable(0, 4, "Smoke4");
		IntVar smoke5 = pb.addVariable(0, 4, "Smoke5");
		List<Variable<IntValue>> smokes = new ArrayList<Variable<IntValue>>();
		smokes.add(smoke1);
		smokes.add(smoke2);
		smokes.add(smoke3);
		smokes.add(smoke4);
		smokes.add(smoke5);

		// Constraints
		pb.add(new AllDiff<IntValue>(colors));
		pb.add(new AllDiff<IntValue>(drinks));
		pb.add(new AllDiff<IntValue>(nations));
		pb.add(new AllDiff<IntValue>(pets));
		pb.add(new AllDiff<IntValue>(smokes));

		// 1. The British man lives in the red house.
		pb.add(new CrossEqual<IntValue>(colors, RED, nations, BRITISH));

		// 2. The Swedish man has a dog for a pet.
		pb.add(new CrossEqual<IntValue>(pets, DOG, nations, SWEDISH));

		// 3. The Danish man drinks tea.
		pb.add(new CrossEqual<IntValue>(drinks, TEA, nations, DANISH));

		// 4. The green house is to the left of the white house.
		pb.add(new CrossLeft<IntValue>(colors, GREEN, colors, WHITE));

		// 5. The owner of the green house drinks coffee.
		pb.add(new CrossEqual<IntValue>(drinks, COFFEE, colors, GREEN));

		// 6. The person that smokes Pall Mall has a bird.
		pb.add(new CrossEqual<IntValue>(smokes, PALLMALL, pets, BIRD));

		// 7. The owner of the yellow house smokes Dunhill.
		pb.add(new CrossEqual<IntValue>(smokes, DUNHILL, colors, YELLOW));

		// 8. The person that lives in the middle house drinks milk.
		drink3.setValue(MILK);

		// 9. The Norwegian lives in the first house.
		nation1.setValue(NORWEGIAN);

		// 10. The person that smokes Blend, lives next to the one that has a
		// cat.
		pb.add(new CrossNext<IntValue>(smokes, BLEND, pets, CAT));

		// 11. The person that has a horse lives next to the one that smokes
		// Dunhill.
		pb.add(new CrossNext<IntValue>(smokes, DUNHILL, pets, HORSE));

		// 12. The one that smokes Bluemaster drinks beer.
		pb.add(new CrossEqual<IntValue>(smokes, BLUEMASTER, drinks, BEER));

		// 13. The German smokes Prince.
		pb.add(new CrossEqual<IntValue>(smokes, PRINCE, nations, GERMAN));

		// 14. The Norwegian lives next to a blue house.
		pb.add(new CrossNext<IntValue>(nations, NORWEGIAN, colors, BLUE));

		// 15. The person that smokes Blend, has a neighbour that drinks water.
		pb.add(new CrossNext<IntValue>(smokes, BLEND, drinks, WATER));

		// Display the problem
		System.out.println("Problem...");
		System.out.println(pb.toString());

		// Basic solver
		Solver<IntValue> solver = new BasicSolver<IntValue>(pb);

		// Search for a solution
		if (pb.startSearch()
				&& solver.branchAndBound() == Solver.SearchResult.SOLUTION_FOUND) {

			// Display result
			System.out.println("One solution");
			System.out.println("House 1 : "
					+ COLOR[color1.getValue().value().getIntValue()] + ", "
					+ DRINK[drink1.getValue().value().getIntValue()] + ", "
					+ NATION[nation1.getValue().value().getIntValue()] + ", "
					+ PET[pet1.getValue().value().getIntValue()] + ", "
					+ SMOKE[smoke1.getValue().value().getIntValue()]);
			System.out.println("House 2 : "
					+ COLOR[color2.getValue().value().getIntValue()] + ", "
					+ DRINK[drink2.getValue().value().getIntValue()] + ", "
					+ NATION[nation2.getValue().value().getIntValue()] + ", "
					+ PET[pet2.getValue().value().getIntValue()] + ", "
					+ SMOKE[smoke2.getValue().value().getIntValue()]);
			System.out.println("House 3 : "
					+ COLOR[color3.getValue().value().getIntValue()] + ", "
					+ DRINK[drink3.getValue().value().getIntValue()] + ", "
					+ NATION[nation3.getValue().value().getIntValue()] + ", "
					+ PET[pet3.getValue().value().getIntValue()] + ", "
					+ SMOKE[smoke3.getValue().value().getIntValue()]);
			System.out.println("House 4 : "
					+ COLOR[color4.getValue().value().getIntValue()] + ", "
					+ DRINK[drink4.getValue().value().getIntValue()] + ", "
					+ NATION[nation4.getValue().value().getIntValue()] + ", "
					+ PET[pet4.getValue().value().getIntValue()] + ", "
					+ SMOKE[smoke4.getValue().value().getIntValue()]);
			System.out.println("House 5 : "
					+ COLOR[color5.getValue().value().getIntValue()] + ", "
					+ DRINK[drink5.getValue().value().getIntValue()] + ", "
					+ NATION[nation5.getValue().value().getIntValue()] + ", "
					+ PET[pet5.getValue().value().getIntValue()] + ", "
					+ SMOKE[smoke5.getValue().value().getIntValue()]);

			// Propagation efficiency
			System.out.println("Fails : " + pb.getFailsCounter());
			System.out
					.println("Choice points : " + pb.getChoicePointsCounter());
			
		} else {
			System.err.println("No solution found");
		}
		
        // Stop the problem search
        pb.stopSearch();

	}
}
