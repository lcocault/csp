/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestEqualToMaxOfIntArray {

    @Test
    public void testBasic() {

        // Define a test constraint
        IntVar variable = new IntVarImpl(1, 9, "x");
        IntVar a = new IntVarImpl(3, 8, "a");
        IntVar b = new IntVarImpl(4, 9, "b");
        IntVar c = new IntVarImpl(5, 10, "c");
        List<IntVar> array = new ArrayList<IntVar>();
        array.add(a);
        array.add(b);
        array.add(c);
        EqualToMaxOfIntArray cst = new EqualToMaxOfIntArray(variable, array);

        // Accessors
        String name = "x=MAX[a / [3;8](changed = false), b / [4;9](changed = false), c / [5;10](changed = false)]";
        String str = "x=MAX(a, b, c)";
        assertEquals(name, cst.getName());
        assertEquals(str, cst.toString());
        assertTrue(cst.isVariableReferenced(a));
        assertTrue(cst.isVariableReferenced(b));
        assertTrue(cst.isVariableReferenced(c));
        assertTrue(cst.isVariableReferenced(variable));
        assertFalse(cst.isVariableReferenced(new IntVarImpl("foo")));

        // Type of constraint
        assertFalse(cst.isUnary());
        assertFalse(cst.isBinary());
        assertFalse(cst.isTernary());
        assertTrue(cst.isUnsized());

        // Before applying the constraint
        checkBeforePropagation(variable, a, b, c);

        // The constraint applied is not NAry (N=2, binary)
        assertEquals(1, variable.getNAryConstraintsNumber());
        assertEquals(cst, variable.getNAryConstraints().get(0));
        
        // Dry run
        assertTrue(cst.revise());
        assertEquals(0, cst.getRecentChangedVariables().size());

        // Propagate the constraint
        cst.enablePropagate(true);
        assertTrue(cst.revise());
        checkAfterFirstPropagation(variable, a, b, c);
        assertTrue(cst.getRecentChangedVariables().size() > 0);

        // A new propagation should not change anything
        assertTrue(cst.revise());
        checkAfterFirstPropagation(variable, a, b, c);
        assertEquals(0, cst.getRecentChangedVariables().size());

        // Remove values to have empty domains and check the propagation
        for (int i = 5; i <= 9; i++) {
            variable.removeValue(new IntValue(i));
        }
        for (int i = 3; i <= 8; i++) {
            a.removeValue(new IntValue(i));
        }
        assertFalse(cst.revise());
    }

    /**
     * Check the values before the first propagation
     * @param variable
     *            Max variable
     * @param a
     *            One of the variables in the array
     * @param b
     *            Another variable in the array
     * @param c
     *            And one more
     */
    private void checkBeforePropagation(IntVar variable, IntVar a, IntVar b,
            IntVar c) {
        // Variable between 1 and 9
        assertEquals(1, variable.getMinValue().getIntValue());
        assertEquals(9, variable.getMaxValue().getIntValue());
        assertEquals(9, variable.getDomainSize());
        assertEquals(false, variable.isBound());
        assertEquals(false, variable.isDomainEmpty());
        // "a" between 3 and 8
        assertEquals(3, a.getMinValue().getIntValue());
        assertEquals(8, a.getMaxValue().getIntValue());
        assertEquals(6, a.getDomainSize());
        assertEquals(false, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // "b" between 4 and 9
        assertEquals(4, b.getMinValue().getIntValue());
        assertEquals(9, b.getMaxValue().getIntValue());
        assertEquals(6, b.getDomainSize());
        assertEquals(false, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // "c" between 5 and 10
        assertEquals(5, c.getMinValue().getIntValue());
        assertEquals(10, c.getMaxValue().getIntValue());
        assertEquals(6, c.getDomainSize());
        assertEquals(false, c.isBound());
        assertEquals(false, c.isDomainEmpty());
    }

    /**
     * Check the values after the first propagation
     * @param variable
     *            Max variable
     * @param a
     *            One of the variables in the array
     * @param b
     *            Another variable in the array
     * @param c
     *            And one more
     */
    private void checkAfterFirstPropagation(IntVar variable, IntVar a,
            IntVar b, IntVar c) {
        // Variable between 5 and 9
        assertEquals(5, variable.getMinValue().getIntValue());
        assertEquals(9, variable.getMaxValue().getIntValue());
        assertEquals(5, variable.getDomainSize());
        assertEquals(false, variable.isBound());
        assertEquals(false, variable.isDomainEmpty());
        // "a" between 3 and 8
        assertEquals(3, a.getMinValue().getIntValue());
        assertEquals(8, a.getMaxValue().getIntValue());
        assertEquals(6, a.getDomainSize());
        assertEquals(false, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // "b" between 4 and 9
        assertEquals(4, b.getMinValue().getIntValue());
        assertEquals(9, b.getMaxValue().getIntValue());
        assertEquals(6, b.getDomainSize());
        assertEquals(false, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // "c" between 5 and 9
        assertEquals(5, c.getMinValue().getIntValue());
        assertEquals(9, c.getMaxValue().getIntValue());
        assertEquals(5, c.getDomainSize());
        assertEquals(false, c.isBound());
        assertEquals(false, c.isDomainEmpty());
    }

}
