/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.csp.constraint.model.integer.IntValue;
import org.junit.Test;

public class TestIntervalDomain {

    @Test
    public void testBasic() {

        // Create the interval domain
        IntValue min = new IntValue(1);
        IntValue max = new IntValue(9);
        IntervalDomain<IntValue> domain = new IntervalDomain<IntValue>(min, max);

        // Getters
        assertEquals(min, domain.getMinValue());
        assertEquals(min, domain.getFirstValue());
        assertEquals(max, domain.getMaxValue());
        assertEquals(9, domain.getSize());
        assertTrue(domain.contains(new IntValue(5)));
        assertFalse(domain.contains(new IntValue(10)));
        assertTrue(domain.isInDomain(new IntValue(5)));
        assertFalse(domain.isInDomain(new IntValue(10)));
        assertEquals("[1;9]", domain.toString());

        // It is not possible to get the value if the interval has not a single
        // value
        try {
            domain.getValue();
            fail("Should raise an exception");
        } catch (UnboundDomainException e) {
            // Nominal path
        }

        // Change bounds
        IntValue newMin = new IntValue(3);
        IntValue newMax = new IntValue(7);
        assertTrue(domain.reduceWithMinValue(newMin));
        assertTrue(domain.reduceWithMaxValue(newMax));
        assertEquals(newMin, domain.getMinValue());
        assertEquals(newMin, domain.getFirstValue());
        assertEquals(newMax, domain.getMaxValue());
        assertEquals(5, domain.getSize());
        assertTrue(domain.contains(new IntValue(5)));
        assertFalse(domain.contains(new IntValue(9)));

        // Same reduction does not change anything
        assertFalse(domain.reduceWithMinValue(newMin));
        assertFalse(domain.reduceWithMaxValue(newMax));

        // Remove values in the middle...
        IntValue middle = new IntValue(5);
        domain.removeValue(middle);
        assertEquals("[3;4]U[6;7]", domain.toString());
        assertTrue(domain.isInDomain(new IntValue(7)));
        assertFalse(domain.isInDomain(new IntValue(8)));

        // ...at the beginning and at the end
        domain.removeValue(newMin);
        domain.removeValue(newMax);
        assertEquals(newMin.nextValue(), domain.getMinValue());
        assertEquals(newMin.nextValue(), domain.getFirstValue());
        assertEquals(newMax.previousValue(), domain.getMaxValue());
        assertEquals(2, domain.getSize());
        assertTrue(domain.contains(new IntValue(4)));
        assertFalse(domain.contains(new IntValue(5)));

    }

    @Test
    public void testDomainIntersection() {

        // Create the interval domain
        IntValue min1 = new IntValue(1);
        IntValue max1 = new IntValue(7);
        IntervalDomain<IntValue> dom1 = new IntervalDomain<IntValue>(min1, max1);
        IntValue min2 = new IntValue(3);
        IntValue max2 = new IntValue(9);
        IntervalDomain<IntValue> dom2 = new IntervalDomain<IntValue>(min2, max2);

        // Domains have a hole in the middle
        dom1.removeValue(new IntValue(4));
        dom2.removeValue(new IntValue(6));

        // Reduce the domain to the intersection
        assertTrue(dom1.reduceToDomainIntersection(dom2));

        // Getters
        assertEquals(min2, dom1.getMinValue());
        assertEquals(max1, dom1.getMaxValue());
        assertEquals(3, dom1.getSize());
        assertEquals("[3;3]U[5;5]U[7;7]", dom1.toString());
        assertFalse(dom1.isEmpty());

        // Reduce once more does not change
        assertFalse(dom1.reduceToDomainIntersection(dom2));

        // Remove an interval to empty the domain
        assertTrue(dom1.removeInterval(min2, max1));
        assertTrue(dom1.isEmpty());
        // It is not possible to get the min and max values if the interval is
        // empty
        try {
            dom1.getMinValue();
            fail("Should raise an exception");
        } catch (EmptyDomainException e) {
            // Nominal path
        }
        try {
            dom1.getMaxValue();
            fail("Should raise an exception");
        } catch (EmptyDomainException e) {
            // Nominal path
        }

        // Remove once more does not change anything
        assertFalse(dom1.removeInterval(min2, max1));
        // In any way
        dom2.reduceToDomainIntersection(dom1);

        // Restore the first interval and remove punctual values
        dom1 = new IntervalDomain<IntValue>(min1, max1);
        DiscreteDomain<IntValue> dom3 = new DiscreteDomain<IntValue>();
        dom3.addValue(new IntValue(3));
        dom3.addValue(new IntValue(6));
        assertTrue(dom1.reduceToDomainIntersection(dom3));
        assertEquals("[1;2]U[4;5]U[7;7]", dom1.toString());
    }

    @Test
    public void testRemoveIntervals() {

        // Create the interval domain
        IntValue min = new IntValue(1);
        IntValue max = new IntValue(9);

        // to remove.. |--------|
        // current.. |---------|
        IntervalDomain<IntValue> domain = new IntervalDomain<IntValue>(min, max);
        domain.removeInterval(new IntValue(3), new IntValue(10));
        assertEquals("[1;2]", domain.toString());

        // to remove.. |-----|
        // current.. |---------|
        domain = new IntervalDomain<IntValue>(min, max);
        domain.removeInterval(new IntValue(3), new IntValue(7));
        assertEquals("[1;2]U[8;9]", domain.toString());

        // to remove |----------|
        // current.. |---------|
        domain = new IntervalDomain<IntValue>(min, max);
        domain.removeInterval(new IntValue(1), new IntValue(10));
        assertTrue(domain.isEmpty());

        // to remove |--------|
        // current... |---------|
        domain = new IntervalDomain<IntValue>(min, max);
        domain.removeInterval(new IntValue(0), new IntValue(7));
        assertEquals("[8;9]", domain.toString());
    }

    @Test
    public void testSetMinMax() {

        // Create the interval domain
        IntValue min = new IntValue(1);
        IntValue max = new IntValue(9);
        IntValue middle = new IntValue(5);
        IntervalDomain<IntValue> domain = new IntervalDomain<IntValue>(min, max);
        domain.removeValue(middle);
        assertEquals("[1;4]U[6;9]", domain.toString());

        // Set the min and max to change the bounds
        IntValue newMin = new IntValue(3);
        IntValue newMax = new IntValue(7);
        domain.setMinValue(newMin);
        domain.setMaxValue(newMax);
        assertEquals("[3;4]U[6;7]", domain.toString());

        // Extend the bounds
        domain.setMinValue(min);
        domain.setMaxValue(max);
        assertEquals("[1;4]U[6;9]", domain.toString());

        // Reduce the upper part
        domain.setMaxValue(middle);
        assertEquals("[1;4]", domain.toString());

        // Intersect with the upper part
        domain.reduceToDomainIntersection(new IntervalDomain<IntValue>(middle,
                max));
        assertTrue(domain.isEmpty());

        // Restore the interval and reduce the lower part
        domain.setMinValue(min);
        domain.setMaxValue(max);
        domain.removeValue(middle);
        domain.setMinValue(middle);
        assertEquals("[6;9]", domain.toString());

        // Intersect with the lower part
        domain.reduceToDomainIntersection(new IntervalDomain<IntValue>(min,
                middle));
        assertTrue(domain.isEmpty());

        // Restore the interval
        domain.setMaxValue(max);
        domain.setMinValue(min);
        domain.removeValue(middle);
        assertEquals("[1;4]U[6;9]", domain.toString());

    }
}
