/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestIntValue {

    @Test
    public void testBasic() {

        // Define a test value
        IntValue value = new IntValue(7);

        // Check values via getters
        assertEquals(7, value.getIntValue());
        assertEquals("7", value.toString());

        // Compare values
        IntValue other = new IntValue(4);
        IntValue same = new IntValue(7);
        assertTrue(value.compareTo(other) > 0);
        assertTrue(value.compareTo(same) == 0);
        assertTrue(other.compareTo(value) < 0);

        // Equality
        assertFalse(value.equals(other));
        assertTrue(value.equals(same));
        assertFalse(value.equals(7));

        // Compute the distance
        assertEquals(3, value.getDistance(other));
        assertEquals(0, value.getDistance(same));
        assertEquals(3, other.getDistance(value));

        // Hash code
        assertEquals(7, value.hashCode());

        // Next and previous values
        assertEquals(6, value.previousValue().getIntValue());
        assertEquals(8, value.nextValue().getIntValue());

        // Extremum
        assertEquals(Integer.MIN_VALUE / 2 + 1, value.minValue().getIntValue());
        assertEquals(Integer.MAX_VALUE / 2 - 1, value.maxValue().getIntValue());

        // Static functions - Diff
        assertEquals(3, IntValue.diff(value, other).getIntValue());
        assertEquals(-3, IntValue.diff(other, value).getIntValue());
        assertEquals(0, IntValue.diff(value, same).getIntValue());

        // Static functions - Add
        assertEquals(11, IntValue.sum(value, other).getIntValue());
        assertEquals(11, IntValue.sum(other, value).getIntValue());
        assertEquals(14, IntValue.sum(value, same).getIntValue());
    }

}
