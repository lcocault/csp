/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.csp.constraint.model.UnboundDomainException;
import org.csp.constraint.model.integer.IntVar;
import org.junit.Test;

public class TestIntVar {

    @Test
    public void testBasic() {

        // Define a test variable
        IntVar var = new IntVarImpl(1, 9, "myVar");

        // Check values via getters
        assertEquals("myVar", var.getName());
        assertEquals(1, var.getMinValue().getIntValue());
        assertEquals(9, var.getMaxValue().getIntValue());
        assertEquals(9, var.getDomainSize());
        assertEquals(false, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(true, var.canBeSelected());
        assertEquals(0, var.getNAryConstraintsNumber());
        assertTrue(var.getNAryConstraints().isEmpty());
        assertEquals("myVar / [1;9](changed = false)", var.toString());

        // The variable should contain all the values between 1 and 9
        for (int i = 1; i <= 9; i++) {
            assertTrue(var.contains(new IntValue(i)));
        }
        assertFalse(var.contains(new IntValue(0)));
        assertFalse(var.contains(new IntValue(10)));

        // Change name
        var.setName("Renamed");
        assertEquals("Renamed", var.getName());

        // Disable selection
        var.disableSelection();
        assertEquals(false, var.canBeSelected());

        // Save the domain and remove one value in the middle of the domain
        var.saveDomain();
        assertTrue(var.removeValue(new IntValue(5)));
        assertEquals(1, var.getMinValue().getIntValue());
        assertEquals(9, var.getMaxValue().getIntValue());
        assertEquals(8, var.getDomainSize());
        assertEquals(false, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(true, var.canBeSelected());

        // Save the domain once more and remove the min and max values
        var.saveDomain();
        assertTrue(var.removeValue(new IntValue(1)));
        assertTrue(var.removeValue(new IntValue(9)));
        assertEquals(2, var.getMinValue().getIntValue());
        assertEquals(8, var.getMaxValue().getIntValue());
        assertEquals(6, var.getDomainSize());
        assertEquals(false, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(true, var.canBeSelected());

        // Restore the domain
        var.restoreDomain();
        assertEquals(1, var.getMinValue().getIntValue());
        assertEquals(9, var.getMaxValue().getIntValue());
        assertEquals(8, var.getDomainSize());
        assertEquals(false, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(true, var.canBeSelected());

        // It should not be possible to get the value while it is not bound
        try {
            // Attempt to get the value...
            var.getValue();
            // ...should fail and this code should not be reached
            fail();
        } catch (UnboundDomainException e) {
            // Nominal execution path
        }

        // Save the domain and bind the variable
        var.saveDomain();
        assertTrue(var.setValue(new IntValue(4)));
        assertEquals(4, var.getMinValue().getIntValue());
        assertEquals(4, var.getMaxValue().getIntValue());
        assertEquals(new IntValue(4), var.getValue());
        assertEquals(1, var.getDomainSize());
        assertEquals(true, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(true, var.canBeSelected());

        // Save the domain and remove the value to have an empty domain
        var.saveDomain();
        assertTrue(var.removeValue(new IntValue(4)));
        assertEquals(true, var.isDomainEmpty());

        // Restore the domain twice
        var.restoreDomain();
        var.restoreDomain();
        assertEquals(1, var.getMinValue().getIntValue());
        assertEquals(9, var.getMaxValue().getIntValue());
        assertEquals(8, var.getDomainSize());
        assertEquals(false, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(true, var.canBeSelected());

        // Saving an unchanged domain (even multiple times) does not modify the
        // values
        var.saveDomain();
        var.saveDomain();
        assertEquals(1, var.getMinValue().getIntValue());
        assertEquals(9, var.getMaxValue().getIntValue());
        assertEquals(8, var.getDomainSize());
        assertEquals(false, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(true, var.canBeSelected());

        // Domains can be restored without limitation (initial state supports
        // restoration)
        for (int i = 0; i < 10; i++) {
            var.restoreDomain();
        }
        assertEquals("Renamed", var.getName());
        assertEquals(1, var.getMinValue().getIntValue());
        assertEquals(9, var.getMaxValue().getIntValue());
        assertEquals(9, var.getDomainSize());
        assertEquals(false, var.isBound());
        assertEquals(false, var.isDomainEmpty());
        assertEquals(false, var.canBeSelected());
        assertEquals("Renamed / [1;9](changed = false)", var.toString());
    }

}
