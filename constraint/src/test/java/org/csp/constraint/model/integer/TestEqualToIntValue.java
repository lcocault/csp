/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestEqualToIntValue {

    @Test
    public void testBasic() {
    	
        // Define a test constraint
        IntVar variable = new IntVarImpl(1, 9, "x");
        IntValue value = new IntValue(7);
        EqualToIntValue cst = new EqualToIntValue(variable, value);

        // Accessors
        assertEquals("x=7", cst.getName());
        assertEquals(value, cst.getValue());
        assertEquals(variable, cst.getVariable());
        assertEquals("x=7", cst.toString());
        assertTrue(cst.isVariableReferenced(variable));
        assertFalse(cst.isVariableReferenced(new IntVarImpl("foo")));

        // Type of constraint
        assertTrue(cst.isUnary());
        assertFalse(cst.isBinary());
        assertFalse(cst.isTernary());
        assertFalse(cst.isUnsized());

        // Before applying the constraint
        assertEquals(1, variable.getMinValue().getIntValue());
        assertEquals(9, variable.getMaxValue().getIntValue());
        assertEquals(9, variable.getDomainSize());
        assertEquals(false, variable.isBound());
        assertEquals(false, variable.isDomainEmpty());

        // The constraint applied is not NAry
        assertEquals(0, variable.getNAryConstraintsNumber());
        assertTrue(variable.getNAryConstraints().isEmpty());
        
        // Dry run
        assertTrue(cst.revise());
        assertEquals(9, variable.getDomainSize());

        // Propagate the constraint
        cst.enablePropagate(true);
        assertTrue(cst.revise());

        // The variable should have been assigned
        assertEquals(7, variable.getMinValue().getIntValue());
        assertEquals(7, variable.getMaxValue().getIntValue());
        assertEquals(1, variable.getDomainSize());
        assertEquals(true, variable.isBound());
        assertEquals(false, variable.isDomainEmpty());

    }

}
