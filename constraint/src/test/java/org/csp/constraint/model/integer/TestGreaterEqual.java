/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.csp.constraint.model.integer.GreaterEqual;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.junit.Test;

public class TestGreaterEqual {

    @Test
    public void testBasic() {

        // Define initial variables to assure "left>=right+delta"
        IntVar left = new IntVarImpl(1, 3, "left");
        IntVar right = new IntVarImpl(1, 4, "right");
        IntValue delta = new IntValue(1);

        // Define the constraint
        GreaterEqual cst = new GreaterEqual(left, right, delta);

        // Accessors
        assertEquals("left>=right+1", cst.getName());
        assertEquals(left, cst.getFirstVariable());
        assertEquals(right, cst.getSecondVariable());
        assertEquals(delta, cst.getDelta());
        assertEquals("left>=right+1", cst.toString());
        assertEquals(1, left.getNAryConstraints().size());
        assertEquals(1, right.getNAryConstraints().size());
        assertTrue(cst.isVariableReferenced(right));
        assertTrue(cst.isVariableReferenced(left));
        assertFalse(cst.isVariableReferenced(new IntVarImpl("foo")));

        // Type of constraint
        assertFalse(cst.isUnary());
        assertTrue(cst.isBinary());
        assertFalse(cst.isTernary());
        assertFalse(cst.isUnsized());

        // Dry run
        assertTrue(cst.revise());
        assertEquals(0, cst.getRecentChangedVariables().size());

        // Propagate the constraint
        cst.enablePropagate(true);
        assertTrue(cst.revise());
        checkAfterPropagation(left, right);
        assertEquals(2, cst.getRecentChangedVariables().size());

        // Propagate once again
        assertTrue(cst.revise());
        checkAfterPropagation(left, right);
        assertEquals(0, cst.getRecentChangedVariables().size());
    }

    /**
     * Check the values after the propagation
     * @param variable
     *            Variable (left)
     * @param input
     *            Input (right)
     */
    private void checkAfterPropagation(IntVar left, IntVar right) {
        // Left is between 2 and 3
        assertEquals(2, left.getMinValue().getIntValue());
        assertEquals(3, left.getMaxValue().getIntValue());
        assertEquals(false, left.isBound());
        assertEquals(false, left.isDomainEmpty());
        // Right is between 1 and 2
        assertEquals(1, right.getMinValue().getIntValue());
        assertEquals(2, right.getMaxValue().getIntValue());
        assertEquals(false, right.isBound());
        assertEquals(false, right.isDomainEmpty());
    }

}
