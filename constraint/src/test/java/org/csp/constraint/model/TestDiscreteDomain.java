/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.csp.constraint.model.integer.IntValue;
import org.junit.Test;

public class TestDiscreteDomain {

    @Test
    public void testBasic() {

        // Test mixture
        IntValue min = new IntValue(1);
        IntValue middle = new IntValue(5);
        IntValue additional = new IntValue(7);
        IntValue max = new IntValue(9);
        DiscreteDomain<IntValue> domain = new DiscreteDomain<IntValue>();

        // Empty domain
        assertFalse(domain.contains(middle));
        assertFalse(domain.isInDomain(middle));
        assertEquals(0, domain.getSize());
        assertTrue(domain.isEmpty());

        // It should not be possible to get "the" value
        try {
            domain.getValue();
            fail();
        } catch (UnboundDomainException e) {
            // Nominal path
        }

        // It should not be possible to get "the" first value
        try {
            domain.getFirstValue();
            fail();
        } catch (EmptyDomainException e) {
            // Nominal path
        }

        // Set the value
        assertTrue(domain.setValue(middle));
        assertFalse(domain.setValue(middle));
        assertTrue(domain.contains(middle));
        assertTrue(domain.isInDomain(middle));
        assertEquals(1, domain.getSize());
        assertEquals(middle, domain.getFirstValue());
        assertEquals(middle, domain.getValue());
        assertFalse(domain.isEmpty());

        // Add two other values
        domain.addValue(min);
        domain.addValue(max);
        assertTrue(domain.contains(middle));
        assertTrue(domain.isInDomain(middle));
        assertEquals(3, domain.getSize());
        assertEquals(min, domain.getFirstValue());
        assertFalse(domain.isEmpty());
        assertEquals("[1, 5, 9]", domain.toString());

        // It should not be possible to get "the" value
        try {
            domain.getValue();
            fail();
        } catch (UnboundDomainException e) {
            // Nominal path
        }

        // Copy the domain, add a value to the original and remove a value from
        // the copy
        Domain<IntValue> copy = domain.copy();
        domain.addValue(additional);
        copy.removeValue(middle);
        assertEquals(4, domain.getSize());
        assertEquals(2, copy.getSize());
        
        // Compute the intersection of domains
        domain.reduceToDomainIntersection(copy);
        assertFalse(domain.contains(middle));
        assertEquals(2, domain.getSize());
        assertEquals(min, domain.getFirstValue());

    }
}
