/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestEqualToIntVar {

    @Test
    public void testBasic() {

        // Define a test constraint
        IntVar variable = new IntVarImpl(1, 9, "x");
        IntVar input = new IntVarImpl(4, 10, "y");
        IntValue delta = new IntValue(3);
        EqualToIntVar cst = new EqualToIntVar(variable, input, delta);
        cst.enablePropagate(true);

        // Accessors
        assertEquals("x=y+3", cst.getName());
        assertEquals(variable, cst.getLeft());
        assertEquals(input, cst.getRight());
        assertEquals(delta, cst.getDelta());
        assertEquals("x=y+3", cst.toString());

        // Type of constraint
        assertFalse(cst.isUnary());
        assertTrue(cst.isBinary());
        assertFalse(cst.isTernary());
        assertFalse(cst.isUnsized());

        // Before applying the constraint
        checkBeforePropagation(variable, input);

        // The constraint applied is not NAry (N=2, binary)
        assertEquals(1, variable.getNAryConstraintsNumber());
        assertEquals(cst, variable.getNAryConstraints().get(0));

        // Propagate the constraint
        assertTrue(cst.revise());
        checkAfterFirstPropagation(variable, input);
        assertEquals(2, cst.getRecentChangedVariables().size());

        // A new propagation should not change anything
        assertTrue(cst.revise());
        checkAfterFirstPropagation(variable, input);
        assertEquals(0, cst.getRecentChangedVariables().size());

        // Apply additional restrictions
        input.removeValue(new IntValue(6));
        variable.removeValue(new IntValue(7));
        assertTrue(cst.revise());
        checkAfterSecondPropagation(variable, input);
        assertEquals(2, cst.getRecentChangedVariables().size());
    }

    /**
     * Check the values before the first propagation
     * @param variable
     *            Variable (left)
     * @param input
     *            Input (right)
     */
    private void checkBeforePropagation(IntVar variable, IntVar input) {
        // Variable between 1 and 9
        assertEquals(1, variable.getMinValue().getIntValue());
        assertEquals(9, variable.getMaxValue().getIntValue());
        assertEquals(9, variable.getDomainSize());
        assertEquals(false, variable.isBound());
        assertEquals(false, variable.isDomainEmpty());
        // Input between 4 and 10
        assertEquals(4, input.getMinValue().getIntValue());
        assertEquals(10, input.getMaxValue().getIntValue());
        assertEquals(7, input.getDomainSize());
        assertEquals(false, input.isBound());
        assertEquals(false, input.isDomainEmpty());
    }

    /**
     * Check the values after the first propagation
     * @param variable
     *            Variable (left)
     * @param input
     *            Input (right)
     */
    private void checkAfterFirstPropagation(IntVar variable, IntVar input) {
        // Variable between 7 and 9
        assertEquals(7, variable.getMinValue().getIntValue());
        assertEquals(9, variable.getMaxValue().getIntValue());
        assertEquals(3, variable.getDomainSize());
        assertEquals(false, variable.isBound());
        assertEquals(false, variable.isDomainEmpty());
        // Input between 4 and 6
        assertEquals(4, input.getMinValue().getIntValue());
        assertEquals(6, input.getMaxValue().getIntValue());
        assertEquals(3, input.getDomainSize());
        assertEquals(false, input.isBound());
        assertEquals(false, input.isDomainEmpty());
    }

    /**
     * Check the values after the second propagation
     * @param variable
     *            Variable (left)
     * @param input
     *            Input (right)
     */
    private void checkAfterSecondPropagation(IntVar variable, IntVar input) {
        // Variable is 8
        assertEquals(8, variable.getMinValue().getIntValue());
        assertEquals(8, variable.getMaxValue().getIntValue());
        assertEquals(1, variable.getDomainSize());
        assertEquals(true, variable.isBound());
        assertEquals(false, variable.isDomainEmpty());
        // Input is 5
        assertEquals(5, input.getMinValue().getIntValue());
        assertEquals(5, input.getMaxValue().getIntValue());
        assertEquals(1, input.getDomainSize());
        assertEquals(true, input.isBound());
        assertEquals(false, input.isDomainEmpty());
    }

}
