/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.Variable;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.model.integer.IntVarImpl;
import org.junit.Test;

public class TestAllDiff {

    @Test
    public void testBasic() {

        // Define a test constraint
        IntVar a = new IntVarImpl(1, 9, "a");
        IntVar b = new IntVarImpl(6, 6, "b");
        IntVar c = new IntVarImpl(6, 7, "c");
        List<Variable<IntValue>> variables = new ArrayList<Variable<IntValue>>();
        variables.add(a);
        variables.add(b);
        variables.add(c);
        AllDiff<IntValue> cst = new AllDiff<IntValue>(variables);
        cst.enablePropagate(true);

        // Accessors
        String name = "ALLDIFF[a / [1;9](changed = false), b / [6;6](changed = false), c / [6;7](changed = false)]";
        String str = "ALLDIFF(a, b, c)";
        assertEquals(name, cst.getName());
        assertEquals(str, cst.toString());

        // Type of constraint
        assertFalse(cst.isUnary());
        assertFalse(cst.isBinary());
        assertFalse(cst.isTernary());
        assertTrue(cst.isUnsized());

        // Before applying the constraint
        checkBeforePropagation(a, b, c);

        // The constraint applied is not NAry (N=3, ternary)
        assertEquals(1, a.getNAryConstraintsNumber());
        assertEquals(cst, a.getNAryConstraints().get(0));

        // Propagate the constraint
        assertTrue(cst.revise());
        checkAfterFirstPropagation(a, b, c);
        assertEquals(3, cst.getRecentChangedVariables().size());

        // A new propagation should change again
        assertTrue(cst.revise());
        checkAfterFirstPropagation(a, b, c);
        assertEquals(0, cst.getRecentChangedVariables().size());

    }

    /**
     * Check the values of variables before the first propagation.
     * @param a
     *            One variable in the constraint
     * @param b
     *            Another variable in the constraint
     * @param c
     *            One more variable in the constraint
     */
    private void checkBeforePropagation(IntVar a, IntVar b, IntVar c) {
        // Variable "a" between 1 and 9
        assertEquals(1, a.getMinValue().getIntValue());
        assertEquals(9, a.getMaxValue().getIntValue());
        assertEquals(9, a.getDomainSize());
        assertEquals(false, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // Variable "b" is 6
        assertEquals(6, b.getMinValue().getIntValue());
        assertEquals(6, b.getMaxValue().getIntValue());
        assertEquals(1, b.getDomainSize());
        assertEquals(true, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // Variable "c" between 6 and 7
        assertEquals(6, c.getMinValue().getIntValue());
        assertEquals(7, c.getMaxValue().getIntValue());
        assertEquals(2, c.getDomainSize());
        assertEquals(false, c.isBound());
        assertEquals(false, c.isDomainEmpty());
    }

    /**
     * Check the values of variables after the first propagation.
     * @param a
     *            One variable in the constraint
     * @param b
     *            Another variable in the constraint
     * @param c
     *            One more variable in the constraint
     */
    private void checkAfterFirstPropagation(IntVar a, IntVar b, IntVar c) {
        // Variable "a" between 1 and 9 without 6
        assertEquals(1, a.getMinValue().getIntValue());
        assertEquals(9, a.getMaxValue().getIntValue());
        assertEquals(7, a.getDomainSize());
        assertFalse(a.contains(new IntValue(6)));
        assertEquals(false, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // Variable "b" is 6
        assertEquals(6, b.getMinValue().getIntValue());
        assertEquals(6, b.getMaxValue().getIntValue());
        assertEquals(1, b.getDomainSize());
        assertEquals(true, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // Variable "c" is 7
        assertEquals(7, c.getMinValue().getIntValue());
        assertEquals(7, c.getMaxValue().getIntValue());
        assertEquals(1, c.getDomainSize());
        assertEquals(true, c.isBound());
        assertEquals(false, c.isDomainEmpty());
    }

}
