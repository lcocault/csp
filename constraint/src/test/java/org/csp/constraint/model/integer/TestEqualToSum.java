/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.csp.constraint.model.integer.EqualToSum;
import org.csp.constraint.model.integer.IntVar;
import org.junit.Test;

public class TestEqualToSum {

    @Test
    public void testSumAdjustment() {

        // Define initial variables to assure "a+b=c"
        IntVar a = new IntVarImpl(1, 3, "a");
        IntVar b = new IntVarImpl(3, 5, "b");
        IntVar c = new IntVarImpl("c");

        // Define the constraint
        EqualToSum cst = new EqualToSum(a, b, c);

        // Accessors
        assertEquals("a+b=c", cst.getName());
        assertEquals(a, cst.getFirstVariable());
        assertEquals(b, cst.getSecondVariable());
        assertEquals(c, cst.getThirdVariable());
        assertEquals("a+b=c", cst.toString());
        assertEquals(1, a.getNAryConstraints().size());
        assertEquals(1, c.getNAryConstraints().size());
        assertTrue(cst.isVariableReferenced(a));
        assertTrue(cst.isVariableReferenced(b));
        assertTrue(cst.isVariableReferenced(c));
        assertFalse(cst.isVariableReferenced(new IntVarImpl("foo")));

        // Type of constraint
        assertFalse(cst.isUnary());
        assertFalse(cst.isBinary());
        assertTrue(cst.isTernary());
        assertFalse(cst.isUnsized());

        // Dry run
        assertTrue(cst.revise());
        assertEquals(0, cst.getRecentChangedVariables().size());

        // Propagate the constraint
        cst.enablePropagate(true);
        assertTrue(cst.revise());
        checkAfterSumPropagation(a, b, c);
        assertEquals(1, cst.getRecentChangedVariables().size());

        // Another propagation does not change anything
        assertTrue(cst.revise());
        checkAfterSumPropagation(a, b, c);
        assertEquals(0, cst.getRecentChangedVariables().size());
    }

    /**
     * Check the variables after the propagation.
     * @param a
     *            Variable "a" in "a+b=c"
     * @param b
     *            Variable "b" in "a+b=c"
     * @param c
     *            Variable "c" in "a+b=c"
     */
    private void checkAfterSumPropagation(IntVar a, IntVar b, IntVar c) {
        // "a" is between 1 and 3
        assertEquals(1, a.getMinValue().getIntValue());
        assertEquals(3, a.getMaxValue().getIntValue());
        // "b" is between 3 and 5
        assertEquals(3, b.getMinValue().getIntValue());
        assertEquals(5, b.getMaxValue().getIntValue());
        // "c" is between 4 and 8
        assertEquals(4, c.getMinValue().getIntValue());
        assertEquals(8, c.getMaxValue().getIntValue());
    }

    @Test
    public void testArgumentAdjustment() {

        // Define initial variables to assure "a+b=c"
        IntVar a = new IntVarImpl("a");
        IntVar b = new IntVarImpl(3, 5, "b");
        IntVar c = new IntVarImpl(4, 8, "c");

        // Define the constraint
        EqualToSum cst = new EqualToSum(a, b, c);
        cst.enablePropagate(true);

        // Propagate the constraint
        assertTrue(cst.revise());
        checkAfterAdjustmentPropagation(a, b, c);
        assertEquals(1, cst.getRecentChangedVariables().size());

        // Another propagation does not change anything
        assertTrue(cst.revise());
        checkAfterAdjustmentPropagation(a, b, c);
    }

    /**
     * Check the variables after the propagation.
     * @param a
     *            Variable "a" in "a+b=c"
     * @param b
     *            Variable "b" in "a+b=c"
     * @param c
     *            Variable "c" in "a+b=c"
     */
    private void checkAfterAdjustmentPropagation(IntVar a, IntVar b, IntVar c) {
        // "a" is between -1 and 5
        assertEquals(-1, a.getMinValue().getIntValue());
        assertEquals(5, a.getMaxValue().getIntValue());
        // "b" is between 3 and 5
        assertEquals(3, b.getMinValue().getIntValue());
        assertEquals(5, b.getMaxValue().getIntValue());
        // "c" is between 4 and 8
        assertEquals(4, c.getMinValue().getIntValue());
        assertEquals(8, c.getMaxValue().getIntValue());
    }

    @Test
    public void testUnsatisfiedConstraint() {

        // Define initial variables to assure "a+b=c"
        IntVar a = new IntVarImpl(1, 2, "a");
        IntVar b = new IntVarImpl(1, 2, "b");
        IntVar c = new IntVarImpl(5, 6, "c");

        // Define the constraint
        EqualToSum cst = new EqualToSum(a, b, c);

        // Propagate the constraint
        cst.enablePropagate(true);
        assertFalse(cst.revise());
    }

}
