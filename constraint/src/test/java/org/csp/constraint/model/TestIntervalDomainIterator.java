/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Iterator;

import org.csp.constraint.model.integer.IntValue;
import org.junit.Test;

public class TestIntervalDomainIterator {

    @Test
    public void testBasic() {

        // Create the interval domain
        IntValue min = new IntValue(1);
        IntValue middleMin = new IntValue(3);
        IntValue middleMax = new IntValue(6);
        IntValue max = new IntValue(9);
        IntervalDomain<IntValue> domain = new IntervalDomain<IntValue>(min, max);
        domain.removeValue(middleMin);
        domain.removeValue(middleMax);

        // Get the iterator and make a run
        Iterator<IntValue> it = domain.iterator();
        int count = 0;
        while (it.hasNext()) {
            assertNotNull(it.next());
            count++;
        }
        assertEquals(7, count);

        // Get the iterator and remove all the values
        it = domain.iterator();
        try {
            it.remove();
            fail("Should raise an exception");
        } catch (IllegalStateException e) {
            // Nominal path
        }
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
        assertTrue(domain.isEmpty());

        // Try to get an iterator on an empty domain
        try {
            domain.iterator().hasNext();
            fail("Should raise an exception");
        } catch (EmptyDomainException e) {
            // Nominal path
        }
    }
}
