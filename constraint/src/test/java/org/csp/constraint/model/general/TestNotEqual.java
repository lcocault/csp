/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.model.integer.IntVarImpl;
import org.junit.Test;

public class TestNotEqual {

    @Test
    public void testBasic() {

        // Define a test constraint
        IntVar a = new IntVarImpl(6, 7, "a");
        IntVar b = new IntVarImpl(6, 6, "b");
        IntVar c = new IntVarImpl(7, 7, "c");
        IntVar d = new IntVarImpl(7, 8, "d");
        NotEqual<IntValue> cst0 = new NotEqual<IntValue>(a, a);
        NotEqual<IntValue> cst1 = new NotEqual<IntValue>(a, b);
        NotEqual<IntValue> cst2 = new NotEqual<IntValue>(c, a);
        NotEqual<IntValue> cst3 = new NotEqual<IntValue>(a, d);
        cst0.enablePropagate(true);
        cst1.enablePropagate(true);
        cst2.enablePropagate(true);
        cst3.enablePropagate(true);

        // Accessors
        assertEquals("a != b", cst1.getName());
        assertEquals("a != b", cst1.toString());

        // Type of constraint
        assertFalse(cst1.isUnary());
        assertTrue(cst1.isBinary());
        assertFalse(cst1.isTernary());
        assertFalse(cst1.isUnsized());

        // Before applying the constraint
        checkBeforePropagation(a, b, c, d);

        // The constraint applied is not NAry (N=2, binary)
        assertEquals(5, a.getNAryConstraintsNumber());
        assertEquals(1, b.getNAryConstraintsNumber());

        // Propagate with an inefficient constraint
        assertTrue(cst0.revise());
        checkBeforePropagation(a, b, c, d);
        assertEquals(0, cst1.getRecentChangedVariables().size());

        // Propagate the first constraint
        assertTrue(cst1.revise());
        checkAfterFirstPropagation(a, b, c, d);
        assertEquals(1, cst1.getRecentChangedVariables().size());

        // A new propagation should change again
        assertTrue(cst1.revise());
        checkAfterFirstPropagation(a, b, c, d);
        assertEquals(0, cst1.getRecentChangedVariables().size());

        // Propagate the second constraint
        assertFalse(cst2.revise());
        checkAfterSecondPropagation(a, b, c, d);
        assertEquals(1, cst2.getRecentChangedVariables().size());

        // Propagate the third constraint
        assertTrue(cst3.revise());
        checkAfterThirdPropagation(a, b, c, d);
        assertEquals(1, cst3.getRecentChangedVariables().size());

    }

    /**
     * Check the values of variables before the first propagation.
     * @param a
     *            One variable in the constraint
     * @param b
     *            Another variable in the constraint
     * @param c
     *            One more variable in the constraint
     * @param d
     *            And the last one
     */
    private void checkBeforePropagation(IntVar a, IntVar b, IntVar c, IntVar d) {
        // Variable "a" between 6 and 7
        assertEquals(6, a.getMinValue().getIntValue());
        assertEquals(7, a.getMaxValue().getIntValue());
        assertEquals(2, a.getDomainSize());
        assertEquals(false, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // Variable "b" is 6
        assertEquals(6, b.getMinValue().getIntValue());
        assertEquals(6, b.getMaxValue().getIntValue());
        assertEquals(1, b.getDomainSize());
        assertEquals(true, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // Variable "c" is 7
        assertEquals(7, c.getMinValue().getIntValue());
        assertEquals(7, c.getMaxValue().getIntValue());
        assertEquals(1, c.getDomainSize());
        assertEquals(true, c.isBound());
        assertEquals(false, c.isDomainEmpty());
        // Variable "d" between 7 and 8
        assertEquals(7, d.getMinValue().getIntValue());
        assertEquals(8, d.getMaxValue().getIntValue());
        assertEquals(2, d.getDomainSize());
        assertEquals(false, d.isBound());
        assertEquals(false, d.isDomainEmpty());
    }

    /**
     * Check the values of variables after the first propagation.
     * @param a
     *            One variable in the constraint
     * @param b
     *            Another variable in the constraint
     * @param c
     *            One more variable in the constraint
     * @param d
     *            And the last one
     */
    private void checkAfterFirstPropagation(IntVar a, IntVar b, IntVar c,
            IntVar d) {
        // Variable "a" is 7
        assertEquals(7, a.getMinValue().getIntValue());
        assertEquals(7, a.getMaxValue().getIntValue());
        assertEquals(1, a.getDomainSize());
        assertEquals(true, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // Variable "b" is 6
        assertEquals(6, b.getMinValue().getIntValue());
        assertEquals(6, b.getMaxValue().getIntValue());
        assertEquals(1, b.getDomainSize());
        assertEquals(true, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // Variable "c" is 7
        assertEquals(7, c.getMinValue().getIntValue());
        assertEquals(7, c.getMaxValue().getIntValue());
        assertEquals(1, c.getDomainSize());
        assertEquals(true, c.isBound());
        assertEquals(false, c.isDomainEmpty());
        // Variable "d" between 7 and 8
        assertEquals(7, d.getMinValue().getIntValue());
        assertEquals(8, d.getMaxValue().getIntValue());
        assertEquals(2, d.getDomainSize());
        assertEquals(false, d.isBound());
        assertEquals(false, d.isDomainEmpty());
    }

    /**
     * Check the values of variables after the second propagation.
     * @param a
     *            One variable in the constraint
     * @param b
     *            Another variable in the constraint
     * @param c
     *            One more variable in the constraint
     * @param d
     *            And the last one
     */
    private void checkAfterSecondPropagation(IntVar a, IntVar b, IntVar c,
            IntVar d) {
        // Variable "a" is 7
        assertEquals(7, a.getMinValue().getIntValue());
        assertEquals(7, a.getMaxValue().getIntValue());
        assertEquals(1, a.getDomainSize());
        assertEquals(true, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // Variable "b" is 6
        assertEquals(6, b.getMinValue().getIntValue());
        assertEquals(6, b.getMaxValue().getIntValue());
        assertEquals(1, b.getDomainSize());
        assertEquals(true, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // Variable "c" is empty
        assertEquals(0, c.getDomainSize());
        assertEquals(false, c.isBound());
        assertEquals(true, c.isDomainEmpty());
        // Variable "d" between 7 and 8
        assertEquals(7, d.getMinValue().getIntValue());
        assertEquals(8, d.getMaxValue().getIntValue());
        assertEquals(2, d.getDomainSize());
        assertEquals(false, d.isBound());
        assertEquals(false, d.isDomainEmpty());
    }

    /**
     * Check the values of variables after the third propagation.
     * @param a
     *            One variable in the constraint
     * @param b
     *            Another variable in the constraint
     * @param c
     *            One more variable in the constraint
     * @param d
     *            And the last one
     */
    private void checkAfterThirdPropagation(IntVar a, IntVar b, IntVar c,
            IntVar d) {
        // Variable "a" is 7
        assertEquals(7, a.getMinValue().getIntValue());
        assertEquals(7, a.getMaxValue().getIntValue());
        assertEquals(1, a.getDomainSize());
        assertEquals(true, a.isBound());
        assertEquals(false, a.isDomainEmpty());
        // Variable "b" is 6
        assertEquals(6, b.getMinValue().getIntValue());
        assertEquals(6, b.getMaxValue().getIntValue());
        assertEquals(1, b.getDomainSize());
        assertEquals(true, b.isBound());
        assertEquals(false, b.isDomainEmpty());
        // Variable "c" is empty
        assertEquals(0, c.getDomainSize());
        assertEquals(false, c.isBound());
        assertEquals(true, c.isDomainEmpty());
        // Variable "d" is 8
        assertEquals(8, d.getMinValue().getIntValue());
        assertEquals(8, d.getMaxValue().getIntValue());
        assertEquals(1, d.getDomainSize());
        assertEquals(true, d.isBound());
        assertEquals(false, d.isDomainEmpty());
    }

}
