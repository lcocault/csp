/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.Variable;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.model.integer.IntVarImpl;
import org.junit.Test;

public class TestCrossEqual {

    @Test
    public void testBasic() {

        // Define a test constraint
        IntValue a0 = new IntValue(3);
        IntVar a1 = new IntVarImpl(1, 3, "a1");
        IntVar a2 = new IntVarImpl(3, 6, "a2");
        IntVar a3 = new IntVarImpl(7, 9, "a3");
        IntValue b0 = new IntValue(6);
        IntVar b1 = new IntVarImpl(1, 9, "b1");
        IntVar b2 = new IntVarImpl(1, 9, "b2");
        IntVar b3 = new IntVarImpl(1, 9, "b3");
        List<Variable<IntValue>> a = new ArrayList<Variable<IntValue>>();
        List<Variable<IntValue>> b = new ArrayList<Variable<IntValue>>();
        a.add(a1);
        a.add(a2);
        a.add(a3);
        b.add(b1);
        b.add(b2);
        b.add(b3);
        CrossEqual<IntValue> cst = new CrossEqual<IntValue>(a, a0, b, b0);
        cst.enablePropagate(true);

        // Accessors
        String name = "CROSS_EQUAL";
        String str = "CROSS_EQUAL(3 in (a1, a2, a3) equals 6 in (b1, b2, b3))";
        assertEquals(name, cst.getName());
        assertEquals(str, cst.toString());

        // Type of constraint
        assertFalse(cst.isUnary());
        assertFalse(cst.isBinary());
        assertFalse(cst.isTernary());
        assertTrue(cst.isUnsized());

        // Before applying the constraint
        checkBeforePropagation(a1, a2, a3, b1, b2, b3);

        // The constraint applied is not NAry (N=6, ternary)
        assertEquals(1, a1.getNAryConstraintsNumber());
        assertEquals(cst, a1.getNAryConstraints().get(0));

        // Propagate the constraint
        cst.revise();
        checkAfterFirstPropagation(a1, a2, a3, b1, b2, b3);
        assertEquals(1, cst.getRecentChangedVariables().size());

        // A new propagation should change again
        assertTrue(cst.revise());
        checkAfterFirstPropagation(a1, a2, a3, b1, b2, b3);

        // Set the value of a1 to 3 propagate
        a1.setValue(new IntValue(3));
        assertTrue(cst.revise());
        checkAfterSecondPropagation(a1, a2, a3, b1, b2, b3);
        assertEquals(2, cst.getRecentChangedVariables().size());

        // Set the value of b2 to 6 propagate
        b2.setValue(new IntValue(6));
        assertTrue(cst.revise());
        checkAfterThirdPropagation(a1, a2, a3, b1, b2, b3);
        assertEquals(1, cst.getRecentChangedVariables().size());
    }

    /**
     * Check the values of variables before the first propagation.
     * @param a1
     *            First variable of the first array
     * @param a2
     *            Second variable of the first array
     * @param a3
     *            Third variable of the first array
     * @param b1
     *            First variable of the second array
     * @param b2
     *            Second variable of the second array
     * @param b3
     *            Third variable of the second array
     */
    private void checkBeforePropagation(IntVar a1, IntVar a2, IntVar a3,
            IntVar b1, IntVar b2, IntVar b3) {
        // Variable "a1" between 1 and 3
        assertEquals(1, a1.getMinValue().getIntValue());
        assertEquals(3, a1.getMaxValue().getIntValue());
        assertEquals(3, a1.getDomainSize());
        assertEquals(false, a1.isBound());
        assertEquals(false, a1.isDomainEmpty());
        // Variable "a2" between 3 and 6
        assertEquals(3, a2.getMinValue().getIntValue());
        assertEquals(6, a2.getMaxValue().getIntValue());
        assertEquals(4, a2.getDomainSize());
        assertEquals(false, a2.isBound());
        assertEquals(false, a2.isDomainEmpty());
        // Variable "a1" between 7 and 9
        assertEquals(7, a3.getMinValue().getIntValue());
        assertEquals(9, a3.getMaxValue().getIntValue());
        assertEquals(3, a3.getDomainSize());
        assertEquals(false, a3.isBound());
        assertEquals(false, a3.isDomainEmpty());
        // Variable "b1" between 1 and 9
        assertEquals(1, b1.getMinValue().getIntValue());
        assertEquals(9, b1.getMaxValue().getIntValue());
        assertEquals(9, b1.getDomainSize());
        assertEquals(false, b1.isBound());
        assertEquals(false, b1.isDomainEmpty());
        // Variable "b2" between 1 and 9
        assertEquals(1, b2.getMinValue().getIntValue());
        assertEquals(9, b2.getMaxValue().getIntValue());
        assertEquals(9, b2.getDomainSize());
        assertEquals(false, b2.isBound());
        assertEquals(false, b2.isDomainEmpty());
        // Variable "b3" between 1 and 9
        assertEquals(1, b3.getMinValue().getIntValue());
        assertEquals(9, b3.getMaxValue().getIntValue());
        assertEquals(9, b3.getDomainSize());
        assertEquals(false, b3.isBound());
        assertEquals(false, b3.isDomainEmpty());
    }

    /**
     * Check the values of variables after the first propagation.
     * @param a1
     *            First variable of the first array
     * @param a2
     *            Second variable of the first array
     * @param a3
     *            Third variable of the first array
     * @param b1
     *            First variable of the second array
     * @param b2
     *            Second variable of the second array
     * @param b3
     *            Third variable of the second array
     */
    private void checkAfterFirstPropagation(IntVar a1, IntVar a2, IntVar a3,
            IntVar b1, IntVar b2, IntVar b3) {
        // Variable "a1" between 1 and 3
        assertEquals(1, a1.getMinValue().getIntValue());
        assertEquals(3, a1.getMaxValue().getIntValue());
        assertEquals(3, a1.getDomainSize());
        assertEquals(false, a1.isBound());
        assertEquals(false, a1.isDomainEmpty());
        // Variable "a2" between 3 and 6
        assertEquals(3, a2.getMinValue().getIntValue());
        assertEquals(6, a2.getMaxValue().getIntValue());
        assertEquals(4, a2.getDomainSize());
        assertEquals(false, a2.isBound());
        assertEquals(false, a2.isDomainEmpty());
        // Variable "a1" between 7 and 9
        assertEquals(7, a3.getMinValue().getIntValue());
        assertEquals(9, a3.getMaxValue().getIntValue());
        assertEquals(3, a3.getDomainSize());
        assertEquals(false, a3.isBound());
        assertEquals(false, a3.isDomainEmpty());
        // Variable "b1" between 1 and 9
        assertEquals(1, b1.getMinValue().getIntValue());
        assertEquals(9, b1.getMaxValue().getIntValue());
        assertEquals(9, b1.getDomainSize());
        assertEquals(false, b1.isBound());
        assertEquals(false, b1.isDomainEmpty());
        // Variable "b2" between 1 and 9
        assertEquals(1, b2.getMinValue().getIntValue());
        assertEquals(9, b2.getMaxValue().getIntValue());
        assertEquals(9, b2.getDomainSize());
        assertEquals(false, b2.isBound());
        assertEquals(false, b2.isDomainEmpty());
        // Variable "b3" between 1 and 9 without 6
        assertEquals(1, b3.getMinValue().getIntValue());
        assertEquals(9, b3.getMaxValue().getIntValue());
        assertEquals(8, b3.getDomainSize());
        assertFalse(b3.contains(new IntValue(6)));
        assertEquals(false, b3.isBound());
        assertEquals(false, b3.isDomainEmpty());
    }

    /**
     * Check the values of variables after the second propagation.
     * @param a1
     *            First variable of the first array
     * @param a2
     *            Second variable of the first array
     * @param a3
     *            Third variable of the first array
     * @param b1
     *            First variable of the second array
     * @param b2
     *            Second variable of the second array
     * @param b3
     *            Third variable of the second array
     */
    private void checkAfterSecondPropagation(IntVar a1, IntVar a2, IntVar a3,
            IntVar b1, IntVar b2, IntVar b3) {
        // Variable "a1" is 3
        assertEquals(3, a1.getMinValue().getIntValue());
        assertEquals(3, a1.getMaxValue().getIntValue());
        assertEquals(1, a1.getDomainSize());
        assertEquals(true, a1.isBound());
        assertEquals(false, a1.isDomainEmpty());
        // Variable "a2" between 3 and 6
        assertEquals(3, a2.getMinValue().getIntValue());
        assertEquals(6, a2.getMaxValue().getIntValue());
        assertEquals(4, a2.getDomainSize());
        assertEquals(false, a2.isBound());
        assertEquals(false, a2.isDomainEmpty());
        // Variable "a1" between 7 and 9
        assertEquals(7, a3.getMinValue().getIntValue());
        assertEquals(9, a3.getMaxValue().getIntValue());
        assertEquals(3, a3.getDomainSize());
        assertEquals(false, a3.isBound());
        assertEquals(false, a3.isDomainEmpty());
        // Variable "b1" is 6
        assertEquals(6, b1.getMinValue().getIntValue());
        assertEquals(6, b1.getMaxValue().getIntValue());
        assertEquals(1, b1.getDomainSize());
        assertEquals(true, b1.isBound());
        assertEquals(false, b1.isDomainEmpty());
        // Variable "b2" between 1 and 9
        assertEquals(1, b2.getMinValue().getIntValue());
        assertEquals(9, b2.getMaxValue().getIntValue());
        assertEquals(9, b2.getDomainSize());
        assertEquals(false, b2.isBound());
        assertEquals(false, b2.isDomainEmpty());
        // Variable "b3" between 1 and 9 without 6
        assertEquals(1, b3.getMinValue().getIntValue());
        assertEquals(9, b3.getMaxValue().getIntValue());
        assertEquals(8, b3.getDomainSize());
        assertFalse(b3.contains(new IntValue(6)));
        assertEquals(false, b3.isBound());
        assertEquals(false, b3.isDomainEmpty());
    }

    /**
     * Check the values of variables after the third propagation.
     * @param a1
     *            First variable of the first array
     * @param a2
     *            Second variable of the first array
     * @param a3
     *            Third variable of the first array
     * @param b1
     *            First variable of the second array
     * @param b2
     *            Second variable of the second array
     * @param b3
     *            Third variable of the second array
     */
    private void checkAfterThirdPropagation(IntVar a1, IntVar a2, IntVar a3,
            IntVar b1, IntVar b2, IntVar b3) {
        // Variable "a1" is 3
        assertEquals(3, a1.getMinValue().getIntValue());
        assertEquals(3, a1.getMaxValue().getIntValue());
        assertEquals(1, a1.getDomainSize());
        assertEquals(true, a1.isBound());
        assertEquals(false, a1.isDomainEmpty());
        // Variable "a2" is 3
        assertEquals(3, a2.getMinValue().getIntValue());
        assertEquals(3, a2.getMaxValue().getIntValue());
        assertEquals(1, a2.getDomainSize());
        assertEquals(true, a2.isBound());
        assertEquals(false, a2.isDomainEmpty());
        // Variable "a1" between 7 and 9
        assertEquals(7, a3.getMinValue().getIntValue());
        assertEquals(9, a3.getMaxValue().getIntValue());
        assertEquals(3, a3.getDomainSize());
        assertEquals(false, a3.isBound());
        assertEquals(false, a3.isDomainEmpty());
        // Variable "b1" is 6
        assertEquals(6, b1.getMinValue().getIntValue());
        assertEquals(6, b1.getMaxValue().getIntValue());
        assertEquals(1, b1.getDomainSize());
        assertEquals(true, b1.isBound());
        assertEquals(false, b1.isDomainEmpty());
        // Variable "b2" between 1 and 9
        assertEquals(6, b2.getMinValue().getIntValue());
        assertEquals(6, b2.getMaxValue().getIntValue());
        assertEquals(1, b2.getDomainSize());
        assertEquals(true, b2.isBound());
        assertEquals(false, b2.isDomainEmpty());
        // Variable "b3" between 1 and 9 without 6
        assertEquals(1, b3.getMinValue().getIntValue());
        assertEquals(9, b3.getMaxValue().getIntValue());
        assertEquals(8, b3.getDomainSize());
        assertFalse(b3.contains(new IntValue(6)));
        assertEquals(false, b3.isBound());
        assertEquals(false, b3.isDomainEmpty());
    }

}
