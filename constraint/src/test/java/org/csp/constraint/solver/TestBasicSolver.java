/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.Variable;
import org.csp.constraint.model.general.AllDiff;
import org.csp.constraint.model.integer.EqualToIntValue;
import org.csp.constraint.model.integer.EqualToSum;
import org.csp.constraint.model.integer.GreaterEqual;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.solver.Solver.SearchResult;
import org.junit.Test;

public class TestBasicSolver {

    @Test
    public void testProblemResolution() {

        // Create the problem
        IntegerProblem problem = new SingleThreadIntegerProblem();

        // Variables a, b and c
        IntVar a = problem.addVariable(1, 9, "a");
        IntVar b = problem.addVariable(1, 9, "b");
        IntVar c = problem.addVariable(1, 9, "c");

        // a >= b + 4
        GreaterEqual cst1 = new GreaterEqual(a, b, new IntValue(4));
        problem.add(cst1);

        // a = b + c
        EqualToSum cst2 = new EqualToSum(b, c, a);
        problem.add(cst2);

        // a = 8
        EqualToIntValue cst3 = new EqualToIntValue(a, new IntValue(8));
        problem.add(cst3);

        // All different
        List<Variable<IntValue>> variables = new ArrayList<Variable<IntValue>>();
        variables.add(a);
        variables.add(b);
        variables.add(c);
        AllDiff<IntValue> cst4 = new AllDiff<IntValue>(variables);
        problem.add(cst4);

        // Solve it !
        BasicSolver<IntValue> solver = new BasicSolver<IntValue>(problem);
        assertTrue(problem.startSearch());
        assertEquals(SearchResult.SOLUTION_FOUND, solver.branchAndBound());

        // And find the solution that minimize b
        problem.stopSearch();
        assertTrue(problem.startSearch());
        assertEquals(SearchResult.SOLUTION_FOUND, solver.branchAndBound(c));
        assertEquals(new IntValue(3), b.getValue());
        assertEquals(new IntValue(5), c.getValue());

        // Render the problem
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        SolverDotRenderer<IntValue> renderer = new SolverDotRenderer<IntValue>();
        renderer.render(problem, os);
        System.out.println(new String(os.toByteArray()));
    }

    @Test
    public void testInconsistentProblemResolution() {

        // Create the problem
        IntegerProblem problem = new SingleThreadIntegerProblem();
        int numberOfVariables = 5;

        // Create and add the variables to the problem
        List<Variable<IntValue>> variables = new ArrayList<Variable<IntValue>>();
        for (int index = 0; index < numberOfVariables; index++) {
            IntVar var = problem.addVariable(1, numberOfVariables - 1, "P" +
                    index);
            variables.add(var);
        }

        // All the variables must be different
        AllDiff<IntValue> cst = new AllDiff<IntValue>(variables);
        problem.add(cst);

        // Solve it !
        BasicSolver<IntValue> solver = new BasicSolver<IntValue>(problem);
        assertTrue(problem.startSearch());
        assertEquals(SearchResult.NO_SOLUTION, solver.branchAndBound());

        // Restart the search and give a limit of 3 fails
        problem.stopSearch();
        assertTrue(problem.startSearch());
        solver.setFailsLimit(3);
        assertEquals(SearchResult.FAILS_LIMIT, solver.branchAndBound());

        // Restart the search and give a limit of 3 check points
        problem.stopSearch();
        assertTrue(problem.startSearch());
        solver.setChoicePointsLimit(3);
        assertEquals(SearchResult.CHOICE_POINTS_LIMIT, solver.branchAndBound());

        // Restart the search and give a limit of 3 depth levels
        problem.stopSearch();
        assertTrue(problem.startSearch());
        solver.setDepthLimit(3);
        assertEquals(SearchResult.DEPTH_LIMIT, solver.branchAndBound());

    }

}
