/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.Variable;
import org.csp.constraint.model.general.AllDiff;
import org.csp.constraint.model.integer.EqualToIntValue;
import org.csp.constraint.model.integer.EqualToSum;
import org.csp.constraint.model.integer.GreaterEqual;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.junit.Test;

public class TestProblem {

    @Test
    public void testProblemManagement() {

        // Create the problem
        IntegerProblem problem = new SingleThreadIntegerProblem();

        // Variables a, b and c
        IntVar a = problem.addVariable(1, 9, "a");
        IntVar b = problem.addVariable(1, 9, "b");
        IntVar c = problem.addVariable(1, 9, "c");

        // a >= b + 4
        GreaterEqual cst1 = new GreaterEqual(a, b, new IntValue(4));
        problem.add(cst1);

        // a = b + c
        EqualToSum cst2 = new EqualToSum(a, b, c);
        problem.add(cst2);

        // a = 8
        EqualToIntValue cst3 = new EqualToIntValue(a, new IntValue(8));
        problem.add(cst3);

        // All different
        List<Variable<IntValue>> variables = new ArrayList<Variable<IntValue>>();
        variables.add(a);
        variables.add(b);
        variables.add(c);
        AllDiff<IntValue> cst4 = new AllDiff<IntValue>(variables);
        problem.add(cst4);

        // Check the problem
        assertEquals(0, problem.getChoicePointsCounter());
        assertEquals(0, problem.getFailsCounter());
        assertNotNull(problem.getFirstUnboundVariable());
        assertEquals(a, problem.getMostConstraintUnboundVariable());
        assertNotNull(problem.getMostReducedUnboundVariable());
        assertEquals(4, problem.getNumberOfConstraints());
        assertEquals(3, problem.getNumberOfVariables());
        assertEquals(1, problem.getStackDepth());
        assertTrue(problem.toString().length() > 20);

        // Manage the counters
        problem.increaseChoicePointsCounter();
        assertEquals(1, problem.getChoicePointsCounter());
        problem.increaseFailsCounter();
        assertEquals(1, problem.getFailsCounter());

        // Remove one variable and two constraints
        problem.remove(cst2);
        problem.remove(cst4);
        problem.remove(c);
        assertEquals(2, problem.getNumberOfConstraints());
        assertEquals(2, problem.getNumberOfVariables());

        // Clear the problem
        assertTrue(problem.clear());
        assertEquals(0, problem.getNumberOfConstraints());
        assertEquals(0, problem.getNumberOfVariables());
    }

    @Test
    public void testMultiThreadProblemResolution() {

        // Create the problem
        IntegerProblem problem = new SingleThreadIntegerProblem();
        testProblemResolution(problem);
    }

    private void testProblemResolution(IntegerProblem problem) {

        // Variables a, b and c
        IntVar a = problem.addVariable(1, 9, "a");
        IntVar b = problem.addVariable(1, 9, "b");
        IntVar c = problem.addVariable(1, 9, "c");

        // a >= b + 4
        GreaterEqual cst1 = new GreaterEqual(a, b, new IntValue(4));
        problem.add(cst1);

        // a = b + c
        EqualToSum cst2 = new EqualToSum(b, c, a);
        problem.add(cst2);

        // a = 8
        EqualToIntValue cst3 = new EqualToIntValue(a, new IntValue(8));
        problem.add(cst3);

        // All different
        List<Variable<IntValue>> variables = new ArrayList<Variable<IntValue>>();
        variables.add(a);
        variables.add(b);
        variables.add(c);
        AllDiff<IntValue> cst4 = new AllDiff<IntValue>(variables);
        problem.add(cst4);

        // Start search
        assertTrue(problem.startSearch());
        assertEquals(0, problem.getChoicePointsCounter());
        assertEquals(0, problem.getFailsCounter());
        assertEquals(b, problem.getFirstUnboundVariable());
        assertEquals(b, problem.getMostReducedUnboundVariable());
        assertEquals(2, problem.getStackDepth());

        // Choose poorly
        problem.store();
        b.setValue(new IntValue(4));
        assertFalse(problem.applyAC3(b));
        problem.backtrack();

        // Choose wisely
        problem.store();
        b.setValue(new IntValue(2));
        assertTrue(problem.applyAC3(b));
        assertNull(problem.getFirstUnboundVariable());
        assertNull(problem.getMostReducedUnboundVariable());
        assertEquals(3, problem.getStackDepth());

        // Stop the search
        problem.stopSearch();
        assertEquals(1, problem.getStackDepth());
    }

    @Test
    public void testProblemConsistencyMechanisms() {

        // Create the problem
        IntegerProblem problem = new MultiThreadIntegerProblem();
        testProblemConsistencyMechanisms(problem);
    }

    @Test
    public void testMultiThreadProblemConsistencyMechanisms() {

        // Create the problem
        IntegerProblem problem = new MultiThreadIntegerProblem();
        testProblemConsistencyMechanisms(problem);
    }

    private void testProblemConsistencyMechanisms(IntegerProblem problem) {

        // Variables a, b and c
        IntVar a = problem.addVariable(1, 9, "a");
        IntVar b = problem.addVariable(1, 9, "b");
        IntVar c = problem.addVariable(1, 9, "c");

        // a >= b + 4
        GreaterEqual cst1 = new GreaterEqual(a, b, new IntValue(4));

        // a = b + c
        EqualToSum cst2 = new EqualToSum(a, b, c);

        // a = 8
        EqualToIntValue cst3 = new EqualToIntValue(a, new IntValue(8));

        // All different
        List<Variable<IntValue>> variables = new ArrayList<Variable<IntValue>>();
        variables.add(a);
        variables.add(b);
        variables.add(c);
        AllDiff<IntValue> cst4 = new AllDiff<IntValue>(variables);

        // A variable cannot be added if the search in ongoing
        problem.startSearch();
        problem.addVariable("foo");
        assertEquals(3, problem.getNumberOfVariables());

        // Add variables and constraint while the problem is being defined
        problem.add(cst1);
        problem.add(cst2);
        problem.add(cst3);
        problem.add(cst4);
        assertEquals(0, problem.getNumberOfConstraints());

        // Cannot remove anything during search
        assertFalse(problem.remove(cst1));
        assertFalse(problem.remove(cst2));
        assertFalse(problem.remove(cst3));
        assertFalse(problem.remove(cst4));
        assertFalse(problem.remove(a));
        assertFalse(problem.remove(b));
        assertFalse(problem.remove(c));
        assertFalse(problem.clear());

        // Stop the search to add the constraint and try to remove a variable
        problem.stopSearch();
        problem.add(cst1);
        problem.add(cst2);
        problem.add(cst3);
        problem.add(cst4);
        problem.remove(a);
        assertEquals(3, problem.getNumberOfVariables());
        assertEquals(4, problem.getNumberOfConstraints());

        // Remove all the constraints and variables without the clear
        assertTrue(problem.remove(cst1));
        assertTrue(problem.remove(cst2));
        assertTrue(problem.remove(cst3));
        assertTrue(problem.remove(cst4));
        assertTrue(problem.remove(a));
        assertTrue(problem.remove(b));
        assertTrue(problem.remove(c));
    }

    @Test
    public void testInconsistentProblemResolution() {

        // Create the problem
        IntegerProblem problem = new SingleThreadIntegerProblem();
        testInconsistentProblemResolution(problem);
    }

    @Test
    public void testInconsistentMultiThreadProblemResolution() {

        // Create the problem
        IntegerProblem problem = new SingleThreadIntegerProblem();
        testInconsistentProblemResolution(problem);
    }

    private void testInconsistentProblemResolution(IntegerProblem problem) {

        // Variables a, b and c
        IntVar a = problem.addVariable(1, 9, "a");
        IntVar b = problem.addVariable(1, 9, "b");

        // a >= b + 4
        GreaterEqual cst1 = new GreaterEqual(a, b, new IntValue(4));
        problem.add(cst1);

        // a = 8
        EqualToIntValue cst2 = new EqualToIntValue(a, new IntValue(8));
        problem.add(cst2);

        // b = 6
        EqualToIntValue cst3 = new EqualToIntValue(b, new IntValue(6));
        problem.add(cst3);

        // Start search
        assertFalse(problem.startSearch());
    }

}
