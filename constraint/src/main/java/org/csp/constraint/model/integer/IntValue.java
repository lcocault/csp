/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import org.csp.constraint.model.Value;

/**
 * Value for an integer variable.
 */
public class IntValue implements Value<IntValue> {

    /**
     * Maximum value for the type IntValue (the quarter of the Integer size is
     * considered in order to make the "compare" function work effectively).
     */
    protected static final IntValue MAX_INT_VALUE = new IntValue(
            Integer.MAX_VALUE / 2 - 1);

    /**
     * Minimum value for the type IntValue (the quarter of the Integer size is
     * considered in order to make the "compare" function work effectively).
     */
    protected static final IntValue MIN_INT_VALUE = new IntValue(
            Integer.MIN_VALUE / 2 + 1);

    /** Value. */
    private int value_;

    /**
     * Constructor.
     * @param value
     *            Integer value
     */
    public IntValue(final int value) {
        value_ = value;
    }

    /**
     * Compute the difference of two integer variables.
     * @param first
     *            First argument
     * @param second
     *            Second argument
     * @return Difference of the two arguments
     */
    public static IntValue diff(final IntValue first, final IntValue second) {
        return new IntValue(first.value_ - second.value_);
    }

    /**
     * Compute the sum of two integer variables.
     * @param first
     *            First argument
     * @param second
     *            Second argument
     * @return Sum of the two arguments
     */
    public static IntValue sum(final IntValue first, final IntValue second) {
        return new IntValue(first.value_ + second.value_);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final IntValue other) {
        return value_ - other.value_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object other) {

        // Assume not equal
        boolean equal = false;

        if (other instanceof IntValue) {
            // Compare with an other IntValue
            equal = value_ == ((IntValue) other).value_;
        }

        return equal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDistance(final IntValue from) {
        return Math.abs(value_ - from.value_);
    }

    /**
     * Get the integer value.
     * @return Integer value
     */
    public int getIntValue() {
        return value_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return value_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue maxValue() {
        return MAX_INT_VALUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue minValue() {
        return MIN_INT_VALUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue nextValue() {
        return new IntValue(value_ + 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue previousValue() {
        return new IntValue(value_ - 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return Integer.toString(value_);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue value() {
        return this;
    }
}
