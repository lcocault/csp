/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

/**
 * Represents a continuous interval on integer values. The _min and _max values
 * are inside the interval. The interval is defined as [_min;_max].
 */
public class Interval<T extends Value<T>> implements Comparable<Interval<T>> {

    /**
     * Lower bound of the interval.
     */
    private T min_;

    /**
     * Upper bound of the interval.
     */
    private T max_;

    /**
     * Default constructor.
     * @param min
     *            Initial lower bound of the interval
     * @param max
     *            Initial upper bound of the interval
     */
    public Interval(final T min, final T max) {
        min_ = min;
        max_ = max;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final Interval<T> other) {
        return min_.compareTo(other.min_);
    }

    /**
     * True if the given value is greater than the lower bound and lesser than
     * the upper bound.
     * @param value
     *            to find in the interval
     * @return True if the value is in the interval
     */
    public boolean isInInterval(final T value) {
        return value != null && value.compareTo(min_) >= 0 &&
                value.compareTo(max_) <= 0;
    }

    /**
     * Returns the upper bound of the interval.
     * @return The maximum value of the interval
     */
    public T getMaxValue() {
        return max_;
    }

    /**
     * Returns the lower bound of the interval.
     * @return The minimum value of the interval
     */
    public T getMinValue() {
        return min_;
    }

    /**
     * Returns the number of elements in the interval.
     * @return The number of elements in the interval, 0 if it is empty
     */
    public int getSize() {
        return max_.getDistance(min_) + 1;
    }

    /**
     * True if the interval contains no value.
     * @return True if the interval is empty
     */
    public boolean isEmpty() {
        return getSize() == 0;
    }

    /**
     * Change the lower bound of the interval.
     * @param value
     *            New lower bound
     */
    public void setMinValue(final T value) {
        min_ = value;
    }

    /**
     * Change the upper bound of the interval.
     * @param value
     *            New upper bound
     */
    public void setMaxValue(final T value) {
        max_ = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "[" + min_ + ";" + max_ + "]";
    }

}
