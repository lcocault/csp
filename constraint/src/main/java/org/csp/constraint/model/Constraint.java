/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

/**
 * Represents a constraint of a constraint solving problem.
 */
public interface Constraint<T> {

    /**
     * Enable or disable the constraint propagation.
     * @param enable
     *            True if the constraint propagation is enabled, false otherwise
     */
    void enablePropagate(final boolean enable);

    /**
     * Get the name of the constraint.
     * @return The name of the constraint
     */
    String getName();

    /**
     * Tests if the constraint is binary.
     * @return True if the constraint is a binary constraint
     */
    boolean isBinary();

    /**
     * Tell if the propagation of the constraint is enabled.
     * @return True if the constraint propagation is enabled
     */
    boolean isPropagationEnabled();

    /**
     * Tests if the constraint is ternary.
     * @return True if the constraint is a ternary constraint
     */
    boolean isTernary();

    /**
     * Tests if the constraint is unary.
     * @return True if the constraint is an unary constraint
     */
    boolean isUnary();

    /**
     * Tests if the constraint is unsized.
     * @return True if the constraint is an unsized N-ary constraint
     */
    boolean isUnsized();

    /**
     * Propagate the constraint (reduce the domain of each variable associated
     * with the constraint).
     */
    void propagate();

    /**
     * Revise the constraint consistency.
     * @return True if the constraint is consistent (the domain of each variable
     *         associated with the constraint is not empty)
     */
    boolean revise();
};
