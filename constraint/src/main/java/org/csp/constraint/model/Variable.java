/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.List;

/**
 * Represents a variable of a constraint solving problem.
 */
public interface Variable<T> {

    /**
     * Add a new N-ary constraint referenced by the variable.
     * @param constraint
     *            Constraint to add
     */
    void addNAryConstraint(final NAryConstraint<T> constraint);

    /**
     * Test if the variable can be selected to be assigned.
     * @return True if the variable can be selected
     */
    boolean canBeSelected();

    /**
     * Check if the current domain of the variable contain the given value.
     * @param value
     *            Value to look for in the current domain
     * @return "true" if the domain contains the variable
     */
    boolean contains(final Value<T> value);

    /**
     * Disable selection of the variable until its domain has changed.
     */
    void disableSelection();

    /**
     * Get the current domain of the variable.
     * @return The current domain of the variable
     */
    Domain<T> getDomain();

    /**
     * Get the size of the domain.
     * @return The number of elements in the variable domain
     */
    int getDomainSize();

    /**
     * Get the first value of the domain. The notion "first" is meaningful only
     * if the values are ordered. Otherwise an arbitrary value is selected in
     * the domain.
     * @return The first value of the domain.
     * @throws EmptyDomainException
     *             The current domain is empty
     */
    Value<T> getFirstValue() throws EmptyDomainException;

    /**
     * Get the name of the variable.
     * @return Name of the variable
     */
    String getName();

    /**
     * Get the N-ary constraints associated with the variable.
     * @return The list of N-ary constraint associated with the variable
     */
    List<NAryConstraint<T>> getNAryConstraints();

    /**
     * Get the number of n-ary constraints associated with the variable.
     * @return The number of n-ary constraints
     */
    int getNAryConstraintsNumber();

    /**
     * Get the value of the variable when it is bound.
     * @return The value of the variable
     */
    Value<T> getValue();

    /**
     * Test if the variable is bound (the domain has only one value).
     * @return True if the variable is bound
     */
    boolean isBound();

    /**
     * Test if the variable has an empty domain (the domain contains no value).
     * @return True if the domain of the variable is empty
     */
    boolean isDomainEmpty();

    /**
     * Remove the value of the domain of the variable.
     * @param value
     *            Value to remove of the domain
     * @return True if the domain is changed
     */
    boolean removeValue(final Value<T> value);

    /**
     * Restore the more recently saved domain of the variable. The current value
     * of the domain is replaced.
     */
    void restoreDomain();

    /**
     * Save the current domain of the variable.
     */
    void saveDomain();

    /**
     * Set the value of the last "domain changed" and "can be selected"
     * indicators.
     * @param changed
     *            Tell if the domain has just changed
     * @return True if the domain has just changed
     */
    boolean setDomainChanged(final boolean changed);

    /**
     * Set the name of the variable.
     * @param name
     *            Name of the variable
     */
    void setName(final String name);

    /**
     * Set the value of the variable (reduce its domain to a single value).
     * @param value
     *            Unique value of the variable
     * @return True if the domain is changed
     */
    boolean setValue(final Value<T> value);
}
