/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a ternary constraint in a constraint solving problem To define a
 * new ternary constraint, inherit this class and define the propagate method
 * that must fill the list _recentChangedVariables.
 */
public abstract class TernaryConstraint<T> extends NAryConstraint<T> {

    /**
     * A ternary constraint is associated with three variables. This attribute
     * is the first variable.
     */
    private Variable<T> firstVariable_;

    /**
     * A ternary constraint is associated with three variables. This attribute
     * is the second variable.
     */
    private Variable<T> secondVariable_;

    /**
     * A ternary constraint is associated with three variables. This attribute
     * is the third variable.
     */
    private Variable<T> thirdVariable_;

    /**
     * Constructor of a ternary constraint. It references three variables given
     * as parameters.
     * @param name
     *            Name of the ternary constraint
     * @param first
     *            Reference of the first variable
     * @param second
     *            Reference of the second variable
     * @param third
     *            Reference of the third variable
     */
    public TernaryConstraint(final String name, final Variable<T> first,
            final Variable<T> second, final Variable<T> third) {
        super(name);
        firstVariable_ = first;
        secondVariable_ = second;
        thirdVariable_ = third;
        first.addNAryConstraint(this);
        second.addNAryConstraint(this);
        third.addNAryConstraint(this);
    }

    /**
     * Get the first variable.
     * @return First variable of the constraint
     */
    public Variable<T> getFirstVariable() {
        return firstVariable_;
    }

    /**
     * Get the second variable.
     * @return Second variable of the constraint
     */
    public Variable<T> getSecondVariable() {
        return secondVariable_;
    }

    /**
     * Get the third variable.
     * @return Third variable of the constraint
     */
    public Variable<T> getThirdVariable() {
        return thirdVariable_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Variable<T>> getVariables() {
        // Create a list of variables
        final List<Variable<T>> vars = new ArrayList<Variable<T>>();
        vars.add(firstVariable_);
        vars.add(secondVariable_);
        vars.add(thirdVariable_);
        // And return it
        return vars;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBinary() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTernary() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnary() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnsized() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVariableReferenced(final Variable<T> variable) {
        return firstVariable_ == variable || secondVariable_ == variable ||
                thirdVariable_ == variable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean revise() {

        try {
            // Initialize the list of reduced variables
            clearRecentChangedVariables();
            if (isPropagationEnabled()) {
                // This list must be updated by the propagation method
                propagate();
            }

            return !firstVariable_.getDomain().isEmpty() &&
                    !secondVariable_.getDomain().isEmpty() &&
                    !thirdVariable_.getDomain().isEmpty();

        } catch (EmptyDomainException e) {
            // If the domain has been emptied, return false
            return false;
        }
    }

}
