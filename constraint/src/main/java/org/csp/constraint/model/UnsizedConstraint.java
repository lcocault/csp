/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Represents an unsized N-ary constraint in a constraint solving problem To
 * define a new unsized N-ary constraint, inherit this class and define the
 * revise and propagate methods.
 */
public abstract class UnsizedConstraint<T> extends NAryConstraint<T> {

    /**
     * List of constrained variables.
     */
    private List<Variable<T>> variables_;

    /**
     * Constructor of an unsized N-ary constraint.
     * @param name
     *            Name of the unsized N-ary constraint
     * @param variables
     *            List of variables to add to the constraint
     */
    public UnsizedConstraint(final String name,
            final List<Variable<T>> variables) {
        super(name);
        variables_ = new ArrayList<Variable<T>>(variables);
        for (Variable<T> variable : variables_) {
            variable.addNAryConstraint(this);
        }
    }

    /**
     * Add a variable to the constraint.
     * @param variable
     *            Variable added to the constraint
     */
    protected void addVariable(final Variable<T> variable) {
        variables_.add(variable);
        variable.addNAryConstraint(this);
    }

    /**
     * Add a collection of variables to the constraint.
     * @param variables
     *            Variables added to the constraint
     */
    protected void addVariables(final Collection<Variable<T>> variables) {
        variables_.addAll(variables);
        for (Variable<T> variable : variables) {
            variable.addNAryConstraint(this);
        }
    }

    /**
     * Get the list of constrained variables.
     * @return The list of constrained variables
     */
    public final List<Variable<T>> getVariables() {
        return variables_;
    }

    /**
     * Get an iterator on the constrained variables.
     * @return Iterator constrained variables
     */
    public Iterator<Variable<T>> getVariablesIterator() {
        return variables_.iterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBinary() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTernary() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnary() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnsized() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVariableReferenced(final Variable<T> variable) {
        return variables_.contains(variable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean revise() {

        try {

            // Assumed consistency
            boolean consistent = true;

            // Initialize the list of reduced variables
            clearRecentChangedVariables();
            if (isPropagationEnabled()) {
                // This list must be updated by the propagation method
                propagate();
            }

            // Check the constraint consistency
            final Iterator<Variable<T>> variables = variables_.iterator();
            while (consistent && variables.hasNext()) {
                consistent = !variables.next().getDomain().isEmpty();
            }

            return consistent;

        } catch (EmptyDomainException e) {
            // If the domain has been emptied, return false
            return false;
        }
    }
}
