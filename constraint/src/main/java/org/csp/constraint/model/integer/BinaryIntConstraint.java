/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import org.csp.constraint.model.BinaryConstraint;

/**
 * Binary constraint between two variables implying a delta between their
 * values.
 */
public abstract class BinaryIntConstraint extends BinaryConstraint<IntValue> {

    /**
     * Delta between the variables.
     */
    private IntValue delta_;

    /**
     * Constructor of the constraint.
     * @param name
     *            Name of the constraint
     * @param first
     *            First variable
     * @param second
     *            Second variable
     * @param delta
     *            Delta between the variables
     */
    public BinaryIntConstraint(final String name, final IntVar first,
            final IntVar second, final IntValue delta) {
        super(name, first, second);
        delta_ = delta;
    }

    /**
     * Get the delta between the variables.
     * @return Delta between the variables
     */
    public IntValue getDelta() {
        return delta_;
    }

}
