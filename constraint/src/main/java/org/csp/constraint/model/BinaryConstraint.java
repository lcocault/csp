/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a binary constraint in a constraint solving problem. To define a
 * new binary constraint, inherit this class and define the propagate method
 * that must fill the list _recentChangedVariables.
 */
public abstract class BinaryConstraint<T> extends NAryConstraint<T> {

    /**
     * A binary constraint is associated with two variables. This attribute is
     * the first variable.
     */
    private Variable<T> firstVariable_;

    /**
     * A binary constraint is associated with two variables. This attribute is
     * the second variable.
     */
    private Variable<T> secondVariable_;

    /**
     * Constructor of a binary constraint. It references two variables given as
     * parameters.
     * @param name
     *            Name of the binary constraint
     * @param first
     *            Reference of the first variable
     * @param second
     *            Reference of the second variable
     */
    public BinaryConstraint(final String name, final Variable<T> first,
            final Variable<T> second) {
        super(name);
        firstVariable_ = first;
        secondVariable_ = second;
        first.addNAryConstraint(this);
        second.addNAryConstraint(this);
    }

    /**
     * Get the first variable.
     * @return First variable of the constraint
     */
    public Variable<T> getFirstVariable() {
        return firstVariable_;
    }

    /**
     * Get the second variable.
     * @return Second variable of the constraint
     */
    public Variable<T> getSecondVariable() {
        return secondVariable_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Variable<T>> getVariables() {
        // Create a list of variables
        final List<Variable<T>> vars = new ArrayList<Variable<T>>();
        vars.add(firstVariable_);
        vars.add(secondVariable_);
        // And return it
        return vars;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBinary() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTernary() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnary() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnsized() {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVariableReferenced(final Variable<T> variable) {
        return firstVariable_ == variable || secondVariable_ == variable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean revise() {

        try {
            // Initialize the list of reduced variables
            clearRecentChangedVariables();
            if (isPropagationEnabled()) {
                // This list must be updated by the propagation method
                propagate();
            }
            return !firstVariable_.getDomain().isEmpty() &&
                    !secondVariable_.getDomain().isEmpty();

        } catch (EmptyDomainException e) {
            // If the domain has been emptied, return false
            return false;
        }
    }

}
