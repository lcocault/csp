/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.general;

import org.csp.constraint.model.BinaryConstraint;
import org.csp.constraint.model.Variable;

/**
 * Binary constraint specifying that two variables are different.
 */
public class NotEqual<T> extends BinaryConstraint<T> {

    /**
     * Constructor for constraint "left != right".
     * @param left
     *            Left variable
     * @param right
     *            Right variable
     */
    public NotEqual(final Variable<T> left, final Variable<T> right) {
        super(left.getName() + " != " + right.getName(), left, right);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propagate() {

        // The current implementation of the propagation is not as efficient as
        // it could be.
        // TODO Enhancement

        // Get the two variables concerned
        final Variable<T> first = getFirstVariable();
        final Variable<T> second = getSecondVariable();

        // Propagate on the first variable
        if (second.isBound()) {
            if (first.removeValue(second.getValue())) {
                addRecentChangedVariable(first);
            }
        }

        // Propagate on the second variable.
        if (first.isBound()) {
            if (second.removeValue(first.getValue())) {
                addRecentChangedVariable(second);
            }
        }
    }

}
