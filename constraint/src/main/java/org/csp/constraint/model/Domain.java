/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.Iterator;

/**
 * Represents a domain of values.
 */
public interface Domain<T> {

    /**
     * Check if the domain contains the given value.
     * @param value
     *            Value to look for in the domain
     * @return "true" if the domain contains the variable
     */
    boolean contains(final Value<T> value);

    /**
     * Create a copy of the current object.
     * @return Copy of the domain
     */
    Domain<T> copy();

    /**
     * Get the first value of the domain.
     * @return The first value of the domain.
     * @throws EmptyDomainException
     *             Domain is empty
     */
    Value<T> getFirstValue() throws EmptyDomainException;

    /**
     * Returns the number of elements in the domain.
     * @return Number of elements in the domain
     */
    int getSize();

    /**
     * Get the value of the domain. This method is defined only for a bound
     * variable.
     * @return Unique value of the domain
     * @throws UnboundDomainException
     *             Cannot read the value from an unbound domain
     */
    Value<T> getValue() throws UnboundDomainException;

    /**
     * Returns true if the domain contains one and only one value.
     * @return True if the domain has one value
     */
    boolean hasSingleValue();

    /**
     * Returns true if the domain contains no value.
     * @return True if the domain is empty
     */
    boolean isEmpty();

    /**
     * True if the given value is in the domain.
     * @param value
     *            Value to test
     * @return True if the parameter value is in the domain
     */
    boolean isInDomain(Value<T> value);

    /**
     * Get an iterator on the domain values.
     * @return Iterator on the values
     */
    Iterator<T> iterator();

    /**
     * Reduce the domain to a set a values that are both in the current domain
     * and in the domain given as parameter.
     * @param other
     *            Domain to intersect
     * @return True if the domain has changed
     */
    boolean reduceToDomainIntersection(Domain<T> other);

    /**
     * Remove the value of the domain of the variable.
     * @param value
     *            Value to remove of the domain
     * @return True if the domain has changed
     */
    boolean removeValue(Value<T> value);

    /**
     * the value of the variable (reduce its domain to a single value).
     * @param value
     *            Unique value of the variable
     * @return True if the domain has changed
     */
    boolean setValue(Value<T> value);

}
