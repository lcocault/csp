/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import org.csp.constraint.model.Domain;
import org.csp.constraint.model.IntervalDomain;
import org.csp.constraint.model.VariableImpl;

/**
 * Represents an integer variable of a constraint solving problem.
 */
public class IntVarImpl extends VariableImpl<IntValue> implements IntVar {

    /**
     * Constructor.
     * @param minimum
     *            Minimum possible integer value for the variable
     * @param maximum
     *            Maximum possible integer value for the variable
     * @param name
     *            Name of the variable
     */
    public IntVarImpl(final int minimum, final int maximum, final String name) {
        super(name);

        // Domain
        final IntValue min = new IntValue(minimum);
        final IntValue max = new IntValue(maximum);
        final Domain<IntValue> domain = new IntervalDomain<IntValue>(min, max);
        addDomain(domain);

        // Flags corresponding to the domain
        addDomainChange(false);
        addSelectionFlag(true);
    }

    /**
     * Constructor.
     * @param name
     *            Name of the variable
     */
    public IntVarImpl(final String name) {
        super(name);

        // Domain
        final IntValue min = IntValue.MIN_INT_VALUE;
        final IntValue max = IntValue.MAX_INT_VALUE;
        final Domain<IntValue> domain = new IntervalDomain<IntValue>(min, max);
        addDomain(domain);

        // Flags corresponding to the domain
        addDomainChange(false);
        addSelectionFlag(true);
    }

    /**
     * Get the integer interval domain.
     * @return Domain for integer values
     */
    private IntervalDomain<IntValue> getIntDomain() {
        return (IntervalDomain<IntValue>) getDomain();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getMaxValue() {
        return getIntDomain().getMaxValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getMinValue() {
        return getIntDomain().getMinValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean reduceWithMaxValue(final IntValue newMax) {
        return setDomainChanged(getIntDomain().reduceWithMaxValue(newMax));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean reduceWithMinValue(final IntValue newMin) {
        return setDomainChanged(getIntDomain().reduceWithMinValue(newMin));
    }

}
