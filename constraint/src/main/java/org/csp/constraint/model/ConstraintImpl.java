/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

/**
 * Represents a constraint of a constraint solving problem.
 */
public abstract class ConstraintImpl<T> implements Constraint<T> {

    /** Name of the constraint. */
    private String name_;

    /** Enable the constraint to propagate. */
    private boolean enablePropagate_;

    /**
     * Constructor.
     * @param name
     *            Name of the constraint
     */
    public ConstraintImpl(final String name) {
        name_ = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enablePropagate(final boolean enable) {
        enablePropagate_ = enable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return name_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPropagationEnabled() {
        return enablePropagate_;
    }

    /**
     * Indicates if the given variable is referenced by the constraint, ie if
     * the constraint may reduce it.
     * @param variable
     *            Variable whose reference is checked
     * @return True if the variable is referenced by the constraint
     */
    abstract boolean isVariableReferenced(Variable<T> variable);

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return name_;
    }
};
