/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator on the values of an interval domain.
 */
class IntervalDomainIterator<T extends Value<T>> implements Iterator<T> {

    /**
     * Domain of values to iterate.
     */
    private IntervalDomain<T> domain_;

    /**
     * Current value.
     */
    private T value_;

    /**
     * Next value.
     */
    private T next_;

    /**
     * Constructor of the iterator.
     * @param domain
     *            Domain of values to iterate
     */
    public IntervalDomainIterator(final IntervalDomain<T> domain) {
        domain_ = domain;
        value_ = null;
        next_ = domain.getFirstValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext() {
        return next_ != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next() {

        // Value is the next value
        if (next_ == null) {
            throw new NoSuchElementException();
        }
        value_ = next_;

        // Compute the next one
        boolean finished = false;
        final Iterator<Interval<T>> index = domain_.getValues().iterator();
        while (index.hasNext() && !finished) {
            final Interval<T> interval = index.next();
            if (interval.isInInterval(value_)) {
                // Current value found : global iteration is over
                finished = true;

                if (interval.isInInterval(value_.nextValue())) {
                    // The next value is in the interval
                    next_ = value_.nextValue();
                } else if (index.hasNext()) {
                    // Get the next internal minimum
                    next_ = index.next().getMinValue();
                } else {
                    // No next value
                    next_ = null;
                }
            }
        }

        return value_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove() {
        if (value_ == null) {
            // Next has not been called yet
            throw new IllegalStateException();
        } else {
            // Remove the value
            domain_.removeValue(value_);
        }
    }

}
