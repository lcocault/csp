/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Implementation of a variable of a constraint solving problem.
 */
public abstract class VariableImpl<T> implements Variable<T> {

    /**
     * Table of the domains of the variable. Each domain of the table
     * corresponds to a state of the variable propagation.
     */
    private Stack<Domain<T>> domains_;

    /**
     * Table of domain change indicators. An indicator is true if the
     * corresponding domain (the domain that has the same index in the domains
     * table) has been changed since its initialization.
     */
    private Stack<Boolean> domainChanges_;

    /**
     * Table of flags that indicate whether or not the domain can be selected to
     * be assigned. An indicator is true if the corresponding domain (the domain
     * that has the same index in the domains table) can be selected to be
     * assigned. This indicator may be used in strategy that consist of
     * postponing a variable assignment when it fails.
     */
    private Stack<Boolean> canBeSelected_;

    /**
     * Table of the n-ary constraints associated with the variable.
     */
    private List<NAryConstraint<T>> nAryConstraints_;

    /** Name of the variable. */
    private String name_;

    /**
     * Constructor.
     * @param name
     *            Name of the variable
     */
    public VariableImpl(final String name) {
        name_ = name;
        domains_ = new Stack<Domain<T>>();
        domainChanges_ = new Stack<Boolean>();
        canBeSelected_ = new Stack<Boolean>();
        nAryConstraints_ = new ArrayList<NAryConstraint<T>>();
    }

    /**
     * Add a new domain to the current stack.
     * @param domain
     *            Domain to add to the stack
     */
    protected void addDomain(final Domain<T> domain) {
        domains_.add(domain);
    }

    /**
     * Add a new domain change to the current stack.
     * @param changed
     *            Domain change to add to the stack
     */
    protected void addDomainChange(final boolean changed) {
        domainChanges_.add(changed);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addNAryConstraint(final NAryConstraint<T> constraint) {
        nAryConstraints_.add(constraint);
    }

    /**
     * Add a new selection flag to the current stack.
     * @param canBeSelected
     *            Selection flag to add to the stack
     */
    protected void addSelectionFlag(final boolean canBeSelected) {
        canBeSelected_.add(canBeSelected);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canBeSelected() {
        return canBeSelected_.peek();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(final Value<T> value) {
        return getDomain().contains(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disableSelection() {
        canBeSelected_.pop();
        canBeSelected_.push(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Domain<T> getDomain() {
        return domains_.peek();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDomainSize() {
        return getDomain().getSize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Value<T> getFirstValue() throws EmptyDomainException {
        return getDomain().getFirstValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return name_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<NAryConstraint<T>> getNAryConstraints() {
        return nAryConstraints_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNAryConstraintsNumber() {
        return nAryConstraints_.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Value<T> getValue() {

        // Calling the method is legal if and only if the variable is bound
        if (!isBound()) {
            throw new UnboundDomainException();
        }

        return getFirstValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBound() {
        return getDomain().hasSingleValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDomainEmpty() {
        return getDomain().isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeValue(final Value<T> value) {
        return setDomainChanged(domains_.peek().removeValue(value));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void restoreDomain() {

        // This is only possible when previous domains exist
        if (domains_.size() > 1) {
            // Pop the unchanged domain to delete it (smart pointer)
            domains_.pop();
            domainChanges_.pop();
            canBeSelected_.pop();

            // The last domain must not be a reference to an implementation
            // referenced by other stored domains
            if (!domainChanges_.peek() && domains_.size() > 1) {
                // Create a copy of the last domain
                final Domain<T> lastDomain = getDomain().copy();
                // Pop the last domain
                domains_.pop();
                // The last domain can be freely modified
                domains_.push(lastDomain);
            }
        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveDomain() {

        // If the current domain has changed it must be kept as is. Otherwise
        // the older version, if it exists, can be referenced to avoid useless
        // memory use.
        if (!domainChanges_.peek() && domains_.size() > 1) {
            // Pop the unchanged domain to delete it
            domains_.pop();
            // Reference the previous domain
            domains_.push(domains_.peek());
        }

        // Create a copy of the last domain. The copy is initially considered as
        // not modified. The "can be selected" flag is not changed.
        final Domain<T> lastDomain = getDomain().copy();
        domains_.push(lastDomain);
        domainChanges_.push(false);
        canBeSelected_.push(canBeSelected_.peek());
    };

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean setDomainChanged(final boolean changed) {
        // Store the domain change
        if (!domainChanges_.isEmpty()) {
            final boolean previous = domainChanges_.pop();
            domainChanges_.push(previous || changed);
        }
        // A domain that has changed can always be selected
        if (changed) {
            canBeSelected_.pop();
            canBeSelected_.push(true);
        }
        return changed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setName(final String name) {
        name_ = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean setValue(final Value<T> value) {
        return setDomainChanged(domains_.peek().setValue(value));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return name_ + " / " + getDomain() + "(changed = " +
                domainChanges_.peek() + ")";
    }

}
