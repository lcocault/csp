/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

/**
 * Constraint specifying that one specific integer variable is equal to another
 * variable augmented by a constant delta.
 */
public class EqualToIntVar extends BinaryIntConstraint {

    /**
     * Constructor of the constraint "LEFT = RIGHT + DELTA".
     * @param left
     *            Left part of the comparison
     * @param right
     *            Right part of the comparison
     * @param delta
     *            Delta added to the right part
     */
    public EqualToIntVar(final IntVar left, final IntVar right,
            final IntValue delta) {
        super(left.getName() + "=" + right.getName() + "+" + delta, left,
                right, delta);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propagate() {

        // Propagate on the left part
        IntValue newMin = IntValue.sum(getRight().getMinValue(), getDelta());
        IntValue newMax = IntValue.sum(getRight().getMaxValue(), getDelta());
        final boolean leftChanged = getLeft().reduceWithMinValue(newMin) ||
                getLeft().reduceWithMaxValue(newMax);

        // Propagate on the right part
        newMin = IntValue.diff(getLeft().getMinValue(), getDelta());
        newMax = IntValue.diff(getLeft().getMaxValue(), getDelta());
        final boolean rightChanged = getRight().reduceWithMinValue(newMin) ||
                getRight().reduceWithMaxValue(newMax);

        // Update the list of changed variables
        if (leftChanged) {
            addRecentChangedVariable(getLeft());
        }
        if (rightChanged) {
            addRecentChangedVariable(getRight());
        }
    }

    /**
     * Get the left argument of the constraint.
     * @return First argument
     */
    protected IntVar getLeft() {
        return (IntVar) getFirstVariable();
    }

    /**
     * Get the right argument of the constraint.
     * @return Second argument
     */
    protected IntVar getRight() {
        return (IntVar) getSecondVariable();
    }

}
