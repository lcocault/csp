/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model.integer;

import org.csp.constraint.model.Variable;

/**
 * Represents an integer variable of a constraint solving problem.
 */
public interface IntVar extends Variable<IntValue> {

    /**
     * Get the maximum value.
     * @return Maximum value of the integer variable
     */
    IntValue getMaxValue();

    /**
     * Get the minimum value.
     * @return Minimum value of the integer variable
     */
    IntValue getMinValue();

    /**
     * Reduce the domain with a new maximum value.
     * @param newMax
     *            New maximum value
     * @return "true" if the domain has been reduced
     */
    boolean reduceWithMaxValue(final IntValue newMax);

    /**
     * Reduce the domain with a new minimum value.
     * @param newMin
     *            New minimum value
     * @return "true" if the domain has been reduced
     */
    boolean reduceWithMinValue(final IntValue newMin);

}
