/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a N-ary constraint in a constraint solving problem To define a new
 * N-ary constraint, inherit this class and define the revise and propagate
 * methods.
 */
public abstract class NAryConstraint<T> extends ConstraintImpl<T> {

    /**
     * List of variables whose domain has been modified by the last call to
     * revise.
     */
    private List<Variable<T>> recentChangedVariables_;

    /**
     * Constructor of a N-ary constraint.
     * @param name
     *            Name of the N-ary constraint
     */
    public NAryConstraint(final String name) {
        super(name);
        recentChangedVariables_ = new ArrayList<Variable<T>>();
    }

    /**
     * Add a recently changed variable.
     * @param changed
     *            Variable recently changed
     */
    protected void addRecentChangedVariable(final Variable<T> changed) {
        recentChangedVariables_.add(changed);
    }

    /**
     * Clear the list of recently changed variables.
     */
    protected void clearRecentChangedVariables() {
        recentChangedVariables_.clear();
    }

    /**
     * Get the list of recently changed variables.
     * @return The list of variables changed by the last propagation
     */
    public List<Variable<T>> getRecentChangedVariables() {
        return recentChangedVariables_;
    }

    /**
     * Get the variables connected by the constraint.
     * @return List of variables connected by the constraint
     */
    public abstract List<Variable<T>> getVariables();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVariableReferenced(final Variable<T> variable) {
        return recentChangedVariables_.contains(variable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnary() {
        return false;
    }

}
