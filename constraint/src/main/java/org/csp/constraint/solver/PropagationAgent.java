/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.NAryConstraint;
import org.csp.constraint.model.Variable;

/**
 * Agent to process constraint propagation in a multi-thread environment.
 */
public class PropagationAgent<T> implements Runnable {

    /** Poison constraint to stop the agent. */
    protected static final PoisonConstraint POISON = new PoisonConstraint();

    /** Problem hosting the constraints to propagate. */
    private MultiThreadProblem<T> problem_;

    /**
     * Constructor.
     * @param problem
     *            Problem hosting the constraints to propagate
     */
    public PropagationAgent(final MultiThreadProblem<T> problem) {
        problem_ = problem;
    }

    /**
     * While the method "stop" has not been called, get a constraint and
     * propagate it.
     */
    @Override
    public void run() {

        try {

            // Get the first constraint
            NAryConstraint<T> cst = problem_.takeConstraint();

            while (cst != POISON) {

                // The agent is active
                problem_.incrementActiveAgents();

                // Propagate the constraint
                final boolean consistent = cst.revise();

                if (consistent && problem_.isConsistent()) {

                    // Get the list of constraint to be revised once more
                    final List<Variable<T>> vars = new ArrayList<Variable<T>>(
                            cst.getRecentChangedVariables());
                    while (!vars.isEmpty()) {

                        // Get one changed variable
                        final Variable<T> var = vars.remove(0);

                        // Get the associated constraints to merge them with the
                        // current constraints to revise
                        final List<NAryConstraint<T>> varCsts = var
                                .getNAryConstraints();
                        for (NAryConstraint<T> cstToAdd : varCsts) {
                            problem_.addConstraintToRevise(cstToAdd);
                        }
                    }

                } else {
                    // The problem is now inconsistent
                    problem_.setInconsistent();
                }

                // The agent is no longer active
                problem_.decrementActiveAgents();

                // Get the next constraint
                cst = problem_.takeConstraint();
            }

        } catch (InterruptedException e) {

            // The thread should never be interrupted
            e.printStackTrace();
        }
    }

}
