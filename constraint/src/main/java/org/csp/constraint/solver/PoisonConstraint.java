/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.NAryConstraint;
import org.csp.constraint.model.Variable;

/**
 * Poison constraint to stop the propagation agents.
 */
public class PoisonConstraint<T> extends NAryConstraint<T> {

    /**
     * Constructor.
     */
    public PoisonConstraint() {
        super("Poison constraint");
    }

    /**
     * The constraint does not involve any variable.
     * @return An empty list
     */
    @Override
    public List<Variable<T>> getVariables() {
        return new ArrayList<Variable<T>>();
    }

    /**
     * The constraint does not involve any variable.
     * @return False since the constraint is 0-ary
     */
    @Override
    public boolean isBinary() {
        return false;
    }

    /**
     * The constraint does not involve any variable.
     * @return False since the constraint is 0-ary
     */
    @Override
    public boolean isTernary() {
        return false;
    }

    /**
     * The constraint does not involve any variable.
     * @return False since the constraint is 0-ary
     */
    @Override
    public boolean isUnsized() {
        return false;
    }

    /**
     * The constraint does not involve any variable.
     */
    @Override
    public void propagate() {
        // No propagation is done
    }

    /**
     * The constraint does not involve any variable.
     * @return True since there is chance this false constraint will fail
     */
    @Override
    public boolean revise() {
        return true;
    }

}
