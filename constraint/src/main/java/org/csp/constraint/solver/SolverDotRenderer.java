/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.csp.constraint.model.NAryConstraint;
import org.csp.constraint.model.Variable;

/**
 * DOT (GraphViz graph language) renderer for a constraint network.
 */
public class SolverDotRenderer<T> {

    /** Start of the graph. */
    private static final String GRAPH_START = "strict graph csp {\n";

    /** Delimiter of a dependency. */
    private static final String DELIMITER = " -- ";

    /** End of a dependency definition. */
    private static final String LINE_END = "\n";

    /** End of the graph. */
    private static final String GRAPH_END = "}\n";

    /**
     * Constructor.
     */
    public SolverDotRenderer() {
        // Nothing to initialize
    }

    /**
     * Render a problem to an output stream using the DOT syntax.
     * @param problem
     *            Problem to render
     * @param os
     *            Stream to which render the problem
     */
    public void render(final Problem<T> problem, final OutputStream os) {

        // Already processed variables
        final Set<Variable<T>> alreadyProcessed = new HashSet<Variable<T>>();

        // Printer
        final PrintWriter writer = new PrintWriter(os);
        writer.write(GRAPH_START);

        // Consider the variables of the problem
        final Iterator<Variable<T>> variables = problem.getVariables();
        while (variables.hasNext()) {

            // For each variable, identify the variables that are connected
            final Variable<T> variable = variables.next();
            final Set<Variable<T>> connected = new HashSet<Variable<T>>();
            alreadyProcessed.add(variable);
            for (NAryConstraint<T> constraint : variable.getNAryConstraints()) {
                connected.addAll(constraint.getVariables());
            }

            // Render the relations with other variables
            final Iterator<Variable<T>> conVariable = connected.iterator();

            // Open the line
            while (conVariable.hasNext()) {
                final Variable<T> con = conVariable.next();
                if (!alreadyProcessed.contains(con)) {
                    // Link the variable if it has not been processed before
                    writer.write(variable.getName());
                    writer.write(DELIMITER);
                    writer.write(con.getName());
                    writer.write(LINE_END);
                }
            }
        }

        // Flush the modifications
        writer.write(GRAPH_END);
        writer.flush();
    }

}
