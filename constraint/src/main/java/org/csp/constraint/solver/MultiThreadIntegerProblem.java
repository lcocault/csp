/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import org.csp.constraint.model.Variable;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.model.integer.IntVarImpl;

/**
 * Implementation of an integer problem based on multithread generic
 * implementation.
 */
public class MultiThreadIntegerProblem extends MultiThreadProblem<IntValue>
        implements IntegerProblem {

    /**
     * {@inheritDoc}
     */
    @Override
    public IntVar addVariable(final int minimum, final int maximum,
            final String name) {
        // Create a local implementation
        final IntVar variable = new IntVarImpl(minimum, maximum, name);
        // Add it to the current collection of variables of the problem
        add(variable);
        // And return the created variable so that it can be handled by the user
        return variable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Variable<IntValue> addVariable(final String name) {
        // Create a local implementation
        final IntVar variable = new IntVarImpl(name);
        // Add it to the current collection of variables of the problem
        add(variable);
        // And return the created variable so that it can be handled by the user
        return variable;
    }

}
