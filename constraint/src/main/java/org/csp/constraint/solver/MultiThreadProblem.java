/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.csp.constraint.model.NAryConstraint;

/**
 * Multi-thread problem.
 */
public abstract class MultiThreadProblem<T> extends ProblemImpl<T> {

    // TODO Make the number of agents configurable
    /** Number of threads to revise constraints. */
    private static final int THREAD_NB = 5;

    /** Queue of constraints to propagate. */
    private BlockingQueue<NAryConstraint<T>> toRevise_;

    /** Constraint propagation threads. */
    private List<PropagationAgent<T>> propagationAgents_;

    /** Current state of the problem. */
    private boolean consistent_;

    /** Number of active agents. */
    private int activeAgents_;

    /**
     * Add a constraint to the list of constraints to revise.
     * @param cstToAdd
     *            Constraint to add
     */
    protected void addConstraintToRevise(final NAryConstraint<T> cstToAdd) {
        if (isConsistent() && !toRevise_.contains(cstToAdd)) {
            toRevise_.add(cstToAdd);
        }
    }

    /**
     * Apply arc consistency 3 algorithm to the problem by delegating the
     * propagation of constraints to thread agents. The queue of constraints to
     * evaluate is initialized with the parameter. This consistency can be
     * applied only when the problem is being solved. If this method is called
     * before the start of the search it returns false.
     * @param constraints
     *            List of constraints to revise
     * @return True if the problem is being solved and arc consistent
     */
    @Override
    protected synchronized boolean applyAC3(
            final List<NAryConstraint<T>> constraints) {

        // Initialise the consistency indicator
        consistent_ = isSearching();

        // Initialize the list of constraints
        toRevise_.addAll(constraints);

        // The main thread must wait while the list of constraints to revise is
        // not empty or while there are still active agents that can complete
        // it.
        while (!toRevise_.isEmpty() || activeAgents_ > 0) {
            try {
                // Wait that the state change
                wait();
            } catch (InterruptedException e) {
                // The wait should never be interrupted
                e.printStackTrace();
            }
        }

        return consistent_;
    }

    /**
     * Decrement the number of active agents.
     */
    public synchronized void decrementActiveAgents() {
        activeAgents_--;
        notify();
    }

    /**
     * Increment the number of active agents.
     */
    public synchronized void incrementActiveAgents() {
        activeAgents_++;
        notify();
    }

    /**
     * Check if the current state of the problem is consistent.
     * @return "true" if the problem is consistent
     */
    public boolean isConsistent() {
        return consistent_;
    }

    /**
     * Signal that the current state of the problem is inconsistent.
     */
    public void setInconsistent() {
        // The problem is no more consistent
        consistent_ = false;
        // It is not necessary to evaluate other constraints
        toRevise_.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean startSearch() {

        // Create the constraints queue and the constraint propagation threads
        toRevise_ = new LinkedBlockingQueue<NAryConstraint<T>>();
        propagationAgents_ = new ArrayList<PropagationAgent<T>>();
        activeAgents_ = 0;
        for (int agentIndex = 0; agentIndex < THREAD_NB; agentIndex++) {

            // Create an agent
            final PropagationAgent<T> agent = new PropagationAgent<T>(this);
            propagationAgents_.add(agent);

            // And activate it
            new Thread(agent, "PropagationAgent" + agentIndex).start();
        }

        // Finally start the search as usual
        return super.startSearch();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopSearch() {

        // Stop the threads by sending a poison constraint
        for (int agentIndex = 0; agentIndex < THREAD_NB; agentIndex++) {
            toRevise_.add(PropagationAgent.POISON);
        }

        // Finally stop the search as usual
        super.stopSearch();
    }

    /**
     * Take a constraint from the list of constraints to propagate.
     * @return The taken constraint
     * @throws InterruptedException
     *             Blocking operation has been interrupted
     */
    protected NAryConstraint<T> takeConstraint() throws InterruptedException {
        return toRevise_.take();
    }

}
