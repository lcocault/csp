/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;

/**
 * Represents an integer constraint solving problem.
 */
public interface IntegerProblem extends Problem<IntValue> {

    /**
     * Add a new integer variable to the problem.
     * @param minimum
     *            Lower bound of the variable domain
     * @param maximum
     *            Upper bound of the variable domain
     * @param name
     *            Name of the variable
     * @return The variable added to the problem
     */
    IntVar addVariable(final int minimum, final int maximum, final String name);
}
