/* Copyright 2016 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.constraint.solver;

import java.util.Iterator;

import org.csp.constraint.model.Constraint;
import org.csp.constraint.model.Variable;

/**
 * Represents a constraint solving problem.
 */
public interface Problem<T> {

    /**
     * Add a constraint to the problem. A constraint can not be added while the
     * problem is being solved or if one of the referenced variables has not
     * been previously added to the problem. A constraint can not be added
     * twice.
     * @param constraint
     *            Constraint to add to the problem
     * @return True if the constraint has been added
     */
    boolean add(final Constraint<T> constraint);

    /**
     * Add a new variable to the problem.
     * @param name
     *            Name of the variable
     * @return The variable added to the problem
     */
    Variable<T> addVariable(final String name);

    /**
     * Apply arc consistency 3 algorithm to the problem. The list of constraints
     * to evaluate is initialized with constraints associated with the variable
     * given as parameter. This consistency can be applied only when the problem
     * is being solved. If this method is called before the start of the search
     * it returns false.
     * @param variable
     *            Variable for which the consistency must be checked
     * @return True if the problem is being solved and arc consistent
     */
    boolean applyAC3(final Variable<T> variable);

    /**
     * Restore the previous state of each variable.
     */
    void backtrack();

    /**
     * Clear the problem. All the added elements are removed. This method can
     * not be called while the problem is being solved.
     * @return True if the problem has been cleared
     */
    boolean clear();

    /**
     * Get the number of choice points in the current search session.
     * @return The number of choice points
     */
    int getChoicePointsCounter();

    /**
     * Get the number of fails in the current search session.
     * @return The number of fails
     */
    int getFailsCounter();

    /**
     * Get the first unbound variable of the problem.
     * @return The first unbound variable found in the list of variables (the
     *         order of the list is the order in which they have been added),
     *         null if no variable has been found
     */
    Variable<T> getFirstUnboundVariable();

    /**
     * Get the unbound variable with the greatest number of n-ary constraints.
     * Only the N-ary constraints are taken into account because the unary
     * constraints are not evaluated after the beginning of the search
     * @return The unbound variable with the greatest number of constraints,
     *         null if no unbound variable is found
     */
    Variable<T> getMostConstraintUnboundVariable();

    /**
     * Get the unbound variable with the smallest not empty domain.
     * @return The unbound variable with the smallest domain, null if no
     *         variable is unbound
     */
    Variable<T> getMostReducedUnboundVariable();

    /**
     * Get the number of declared constraints.
     * @return The number of constraints
     */
    int getNumberOfConstraints();

    /**
     * Get the number of declared variables.
     * @return The number of variables
     */
    int getNumberOfVariables();

    /**
     * Get an iterator on the variables of the problem.
     * @return Iterator on the problem variables
     */
    Iterator<Variable<T>> getVariables();

    /**
     * Get the current stack depth of the problem.
     * @return The accumulated number of calls to backtrack substracted to the
     *         accumulated number of calls to store.
     */
    int getStackDepth();

    /**
     * Increase the number of choice points.
     */
    void increaseChoicePointsCounter();

    /**
     * Increase the number of fails.
     */
    void increaseFailsCounter();

    /**
     * Remove a constraint of the problem. A constraint can not be removed while
     * the problem is being solved.
     * @param constraint
     *            Constraint to remove
     * @return True if the constraint has been removed
     */
    boolean remove(final Constraint<T> constraint);

    /**
     * Remove a variable of the problem. A variable can not be removed while the
     * problem is being solved or if the variable is referenced by one or more
     * constraints.
     * @param variable
     *            Variable to remove
     * @return True if the variable has been removed
     */
    boolean remove(final Variable<T> variable);

    /**
     * Start the search for a solution.
     * @return True when the problem is consistent. The search can be performed
     *         only when the problem is consistent. When the return is false,
     *         search is forbidden.
     */
    boolean startSearch();

    /**
     * Stop the search for a solution.
     */
    void stopSearch();

    /**
     * Store the current state of each variable.
     */
    void store();
}
