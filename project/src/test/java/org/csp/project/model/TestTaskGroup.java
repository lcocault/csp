/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.csp.project.model.precedence.Dependency;
import org.csp.project.model.task.Task;
import org.csp.project.model.task.TaskConstraint;
import org.junit.Test;

public class TestTaskGroup implements Visitor {

    /** Count for visits. */
    private int count_;

    @Test
    public void testBasic() {

        // Mixture
        Task task1 = new Task("T1");
        Task task2 = new Task("T2");

        // Constructor
        TaskGroup root = new TaskGroup("GROUP");
        TaskGroup child1 = new TaskGroup();
        TaskGroup child2 = new TaskGroup("CHILD2");

        // Getters
        assertEquals("GROUP", root.getName());
        assertEquals("NoName", child1.getName());
        assertTrue(root.getGroups().isEmpty());
        assertTrue(child1.getGroups().isEmpty());
        assertTrue(root.getTasks().isEmpty());
        assertTrue(child1.getTasks().isEmpty());

        // Setters
        root.setName("ROOT");
        child1.setName("CHILD1");
        assertEquals("ROOT", root.getName());
        assertEquals("CHILD1", child1.getName());

        // Structure the groups
        root.add(child1);
        root.add(child2);
        assertEquals(2, root.getGroups().size());

        // Add tasks
        child1.add(task1);
        child1.add(task2);
        assertEquals(2, child1.getTasks().size());

        // All tasks from the root
        List<Task> allTasks = new ArrayList<Task>();
        root.getAllTasks(allTasks);
        assertEquals(0, root.getTasks().size());
        assertEquals(2, allTasks.size());

        // Root copy
        TaskGroup copy = new TaskGroup("COPY");
        copy.setGroups(root.getGroups());
        copy.setTasks(allTasks);
        assertEquals(root.getGroups(), copy.getGroups());

        // Tasks may be duplicated in the getAllTasks return
        List<Task> allCopyTasks = new ArrayList<Task>();
        copy.getAllTasks(allCopyTasks);
        assertEquals(4, allCopyTasks.size());

        // Finally visit the root group
        count_ = 0;
        root.accept(this);
    }

    @Override
    public void visit(Dependency constraint) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(Project project) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(ProjectItem item) {

        // Should be visited only for groups
        assertTrue(item instanceof TaskGroup);
        TaskGroup group = (TaskGroup) item;

        // Task groups
        switch (count_) {
        case 0:
            assertEquals("ROOT", group.getName());
            break;
        case 1:
            assertEquals("CHILD1", group.getName());
            break;
        case 4:
            assertEquals("CHILD2", group.getName());
            break;
        default:
            fail();
        }

        count_++;
    }

    @Override
    public void visit(Task task) {

        // Tasks
        switch (count_) {
        case 2:
            assertEquals("T1", task.getName());
            break;
        case 3:
            assertEquals("T2", task.getName());
            break;
        default:
            fail();
        }

        count_++;
    }

    @Override
    public void visit(TaskConstraint constraint) {
        // Should never get here
        fail();
    }

}
