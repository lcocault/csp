/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.project.model.precedence.Dependency;
import org.csp.scheduler.model.Activity;
import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.ScheduleImpl;
import org.csp.scheduler.model.time.StartAt;
import org.junit.Test;

public class TestStartsAtConstraint implements Visitor {

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(0L);
        Date end = new Date(3600000L);
        Date date = new Date(1800000L);
        IntValue iStart = new IntValue(0);
        IntValue iEnd = new IntValue(3600);
        Task task = new Task("TASK");
        ProjectCalendar calendar = new ProjectCalendar(start, end, 1000L);
        Project project = new Project("PROJECT", calendar);
        Schedule schedule = new ScheduleImpl("SCHEDULE", iStart, iEnd);
        Activity activity = schedule.createActivity("ACT");
        task.setActivity(activity);

        // Constructor
        StartsAtConstraint cst = new StartsAtConstraint(start);
        StartsAtConstraint anonymousCst = new StartsAtConstraint();
        assertEquals(start, cst.getDate());
        assertNull(cst.getConstraint());
        assertNull(anonymousCst.getDate());
        assertNull(anonymousCst.getConstraint());

        // Setters
        cst.setDate(date);
        assertEquals(date, cst.getDate());

        // Create the constraint
        cst.createSchedulerConstraint(project, schedule, task);
        assertNotNull(cst.getConstraint());
        assertTrue(cst.getConstraint() instanceof StartAt);
        StartAt timeCst = (StartAt) cst.getConstraint();
        assertEquals(activity, timeCst.getActivity());
        assertNotNull(timeCst.getConstraint());

        // Propagate it
        assertTrue(schedule.startSearch());
        assertEquals(new IntValue(1800), activity.getStart().getValue());

        // Finally visit the constraint
        cst.accept(this);
    }

    @Override
    public void visit(Dependency constraint) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(Project project) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(ProjectItem item) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(Task task) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(TaskConstraint constraint) {
        assertTrue(constraint instanceof StartsAtConstraint);
    }

}
