/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.project.model.precedence.Dependency;
import org.csp.scheduler.model.Activity;
import org.csp.scheduler.model.ActivityImpl;
import org.junit.Test;

public class TestTask implements Visitor {

    /** Count for visits. */
    private int count_;

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(0L);
        Date end = new Date(3600000L);
        Activity activity = new ActivityImpl("ACT", new IntValue(0),
                new IntValue(3600));
        TaskConstraint cst1 = new StartsAtConstraint(start);
        TaskConstraint cst2 = new EndsAtConstraint(end);

        // Constructor
        Task anonymousTask = new Task();
        Task task = new Task("TASK");

        // Getters
        assertEquals("TASK", task.getName());
        assertEquals("NoName", anonymousTask.getName());
        assertNull(task.getStart());
        assertNull(task.getEnd());
        assertNull(task.getActivity());
        assertTrue(task.getConstraints().isEmpty());
        assertNull(anonymousTask.getStart());
        assertNull(anonymousTask.getEnd());
        assertNull(anonymousTask.getActivity());
        assertTrue(anonymousTask.getConstraints().isEmpty());

        // Change name
        anonymousTask.setName("NewName");
        assertEquals("NewName", anonymousTask.getName());

        // Set dates
        task.setStart(start);
        task.setEnd(end);
        assertEquals(start, task.getStart());
        assertEquals(end, task.getEnd());

        // Set the activity
        task.setActivity(activity);
        assertEquals(activity, task.getActivity());

        // Populate with constraints
        task.add(cst1);
        task.add(cst2);
        assertEquals(2, task.getConstraints().size());

        // Set the same constraint for both tasks
        anonymousTask.setConstraints(task.getConstraints());
        assertEquals(task.getConstraints(), anonymousTask.getConstraints());

        // Finally visit the task
        count_ = 0;
        task.accept(this);
    }

    @Override
    public void visit(Dependency constraint) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(Project project) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(ProjectItem item) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(Task task) {
        // Task visit
        switch (count_) {
        case 0:
            assertEquals("TASK", task.getName());
            break;
        default:
            fail();
        }

        count_++;
    }

    @Override
    public void visit(TaskConstraint constraint) {
        // Task constraint visit
        switch (count_) {
        case 1:
            assertTrue(constraint instanceof StartsAtConstraint);
            break;
        case 2:
            assertTrue(constraint instanceof EndsAtConstraint);
            break;
        default:
            fail();
        }

        count_++;
    }

}
