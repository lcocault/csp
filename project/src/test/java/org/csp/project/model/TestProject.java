/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;

import org.csp.project.model.precedence.Dependency;
import org.csp.project.model.precedence.StartsAtEndConstraint;
import org.csp.project.model.task.Task;
import org.csp.project.model.task.TaskConstraint;
import org.junit.Test;

public class TestProject implements Visitor {

    /** Count for visits. */
    private int count_;

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(0);
        Date end = new Date(3600000L);
        ProjectCalendar calendar = new ProjectCalendar(start, end, 1000);
        TaskGroup root = new TaskGroup("ROOT");
        Task task1 = new Task("T1");
        Task task2 = new Task("T2");
        Task task3 = new Task("T3");
        StartsAtEndConstraint cst1 = new StartsAtEndConstraint("T1", "T2", 0);
        StartsAtEndConstraint cst2 = new StartsAtEndConstraint("T2", "T3", 0);
        root.add(task1);
        root.add(task2);
        root.add(task3);

        // Constructor
        Project project = new Project("PROJECT", calendar);
        Project anonymousProject = new Project();

        // Getters
        assertEquals("PROJECT", project.getName());
        assertEquals(calendar, project.getCalendar());
        assertTrue(project.getDependencies().isEmpty());
        assertEquals("NoName", anonymousProject.getName());
        assertNull(anonymousProject.getCalendar());

        // Change the calendar
        anonymousProject.setCalendar(calendar);
        assertEquals(calendar, anonymousProject.getCalendar());

        // Add a task group
        project.add(root);

        // Add dependencies
        project.add(cst1);
        project.add(cst2);
        assertEquals(2, project.getDependencies().size());
        
        // Set dependencies
        anonymousProject.setDependencies(project.getDependencies());
        assertEquals(2, anonymousProject.getDependencies().size());

        // Finally visit the root group
        count_ = 0;
        project.accept(this);
    }

    @Override
    public void visit(Dependency constraint) {

        // Dependencies
        switch (count_) {
        case 6:
        case 7:
            assertTrue(constraint instanceof StartsAtEndConstraint);
            break;
        default:
            fail();
        }

        count_++;
    }

    @Override
    public void visit(Project project) {

        // Project
        switch (count_) {
        case 0:
            assertEquals("PROJECT", project.getName());
            break;
        default:
            fail();
        }

        count_++;
    }

    @Override
    public void visit(ProjectItem item) {

        // Should be visited only for groups
        assertTrue(item instanceof TaskGroup);
        TaskGroup group = (TaskGroup) item;

        // Task groups
        switch (count_) {
        case 1:
            assertEquals("PROJECT", group.getName());
            break;
        case 2:
            assertEquals("ROOT", group.getName());
            break;
        default:
            fail();
        }

        count_++;
    }

    @Override
    public void visit(Task task) {

        // Tasks
        switch (count_) {
        case 3:
            assertEquals("T1", task.getName());
            break;
        case 4:
            assertEquals("T2", task.getName());
            break;
        case 5:
            assertEquals("T3", task.getName());
            break;
        default:
            fail();
        }

        count_++;
    }

    @Override
    public void visit(TaskConstraint constraint) {
        // Should never get here
        fail();
    }

}
