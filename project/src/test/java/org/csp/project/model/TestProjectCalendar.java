/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.junit.Test;

public class TestProjectCalendar {

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(86400000L);
        Date end = new Date(86400000L + 3600000L);
        long slotDuration = 1000;

        // Constructor
        ProjectCalendar calendar = new ProjectCalendar(start, end, slotDuration);

        // Getters
        assertEquals(start, calendar.getStart());
        assertEquals(end, calendar.getEnd());
        assertEquals(slotDuration, calendar.getSlotDuration());
    }

    @Test
    public void testDefaultConstructor() {

        // Mixture
        Date now = new Date();
        Date start = new Date(86400000L);
        Date end = new Date(86400000L + 3600000L);
        long slotDuration = 1000;

        // Constructor
        ProjectCalendar calendar = new ProjectCalendar();

        // Default values
        assertEquals(0, calendar.getStart().getTime());
        assertTrue(calendar.getEnd().getTime() >= now.getTime());
        assertEquals(3600000, calendar.getSlotDuration());

        // Setters
        calendar.setStart(start);
        calendar.setEnd(end);
        calendar.setSlotDuration(slotDuration);
        assertEquals(start, calendar.getStart());
        assertEquals(end, calendar.getEnd());
        assertEquals(slotDuration, calendar.getSlotDuration());
    }

    @Test
    public void testConversion() {

        // Mixture
        Date start = new Date(86400000L);
        Date end = new Date(86400000L + 3600000L);
        long slotDuration = 1000;

        // Constructor
        ProjectCalendar calendar = new ProjectCalendar(start, end, slotDuration);

        // Convert a date into a slot number and vice-versa
        Date date = new Date(86400000L + 1800000L);
        assertEquals(1800, calendar.convertDate(date));
        assertEquals(date, calendar.convertDate(new IntValue(1800)));

        // Convert delay
        assertEquals(456, calendar.convertDelay(456789L));
    }

    @Test
    public void testValidity() {

        // Mixture
        Date start = new Date(86400000L);
        Date end = new Date(86400000L + 3600000L);
        long slotDuration = 1000;

        // Constructor
        ProjectCalendar calendar = new ProjectCalendar(start, end, slotDuration);

        // Valid and invalid delays
        long validDelay = 1800000L;
        long invalidDelay = 1800001L;
        assertTrue(calendar.isValidDelay(validDelay));
        assertFalse(calendar.isValidDelay(invalidDelay));

        // Valid and invalid dates
        Date validDate = new Date(86400000L + 1800000L);
        Date invalidDate = new Date(86400000L + 1800001L);
        assertTrue(calendar.isValidDate(validDate));
        assertFalse(calendar.isValidDate(invalidDate));
    }

}
