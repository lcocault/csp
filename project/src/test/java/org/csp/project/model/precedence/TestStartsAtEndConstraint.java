/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.precedence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.project.model.task.Task;
import org.csp.project.model.task.TaskConstraint;
import org.csp.scheduler.model.Activity;
import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.ScheduleImpl;
import org.csp.scheduler.model.precedence.EndAtStart;
import org.junit.Test;

public class TestStartsAtEndConstraint implements Visitor {

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(0L);
        Date end = new Date(3600000L);
        IntValue iStart = new IntValue(0);
        IntValue iEnd = new IntValue(3600);
        Task task1 = new Task("Task1");
        Task task2 = new Task("Task2");
        ProjectCalendar calendar = new ProjectCalendar(start, end, 1000L);
        Project project = new Project("PROJECT", calendar);
        Schedule schedule = new ScheduleImpl("SCHEDULE", iStart, iEnd);
        Activity a1 = schedule.createActivity("A1");
        Activity a2 = schedule.createActivity("A2");
        task1.setActivity(a1);
        task2.setActivity(a2);

        // Constructor
        StartsAtEndConstraint cst = new StartsAtEndConstraint("T1", "T2", 1000);
        StartsAtEndConstraint anonymousCst = new StartsAtEndConstraint();
        assertEquals(1000, cst.getDelay());
        assertEquals("T1", cst.getFirst());
        assertEquals("T2", cst.getSecond());
        assertNull(cst.getConstraint());
        assertEquals(0, anonymousCst.getDelay());
        assertNull(anonymousCst.getFirst());
        assertNull(anonymousCst.getSecond());
        assertNull(anonymousCst.getConstraint());

        // Setters
        cst.setDelay(2000);
        assertEquals(2000, cst.getDelay());
        cst.setFirst("Task1");
        assertEquals("Task1", cst.getFirst());
        cst.setSecond("Task2");
        assertEquals("Task2", cst.getSecond());

        // Create the constraint
        cst.createSchedulerConstraint(project, schedule, task1, task2);
        assertNotNull(cst.getConstraint());
        assertTrue(cst.getConstraint() instanceof EndAtStart);
        EndAtStart timeCst = (EndAtStart) cst.getConstraint();
        assertEquals(a1, timeCst.getFirst());
        assertEquals(a2, timeCst.getSecond());
        assertNotNull(timeCst.getConstraint());

        // Propagate it
        assertTrue(schedule.startSearch());
        assertEquals(IntValue.sum(a1.getEnd().getMaxValue(), new IntValue(2)),
                a2.getStart().getMaxValue());

        // Finally visit the constraint
        cst.accept(this);
    }

    @Override
    public void visit(Dependency constraint) {
        assertTrue(constraint instanceof StartsAtEndConstraint);
    }

    @Override
    public void visit(Project project) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(ProjectItem item) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(Task task) {
        // Should never get here
        fail();
    }

    @Override
    public void visit(TaskConstraint constraint) {
        // Should never get here
        fail();
    }

}
