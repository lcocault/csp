/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.precedence.StartsAtEndConstraint;
import org.csp.project.model.task.Task;
import org.junit.Test;

public class TestScheduleChecker {

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(0);
        Date badStart = new Date(1);
        Date end = new Date(3600000L);
        Date badEnd = new Date(3600001L);
        ProjectCalendar validCalendar = new ProjectCalendar(start, end, 1000);
        ProjectCalendar invalidCalendar = new ProjectCalendar(start, end, 1001);
        ProjectCalendar dummyCalendar = new ProjectCalendar(end, start, 1000);
        Task task1 = new Task("T1");
        Task task2 = new Task("T2");
        Task invalidTask = new Task("T3");
        StartsAtEndConstraint validDependency = new StartsAtEndConstraint("T1",
                "T2", 0);
        StartsAtEndConstraint invalidDependency = new StartsAtEndConstraint(
                "PORTNAWAK1", "PORTNAWAK2", 123);
        Project validProject = new Project("VALID", validCalendar);
        Project invalidProject = new Project("INVALID", invalidCalendar);
        Project dummyProject = new Project("DUMMY", dummyCalendar);
        validProject.add(task1);
        validProject.add(task2);
        invalidProject.add(invalidTask);
        invalidProject.add(invalidTask);
        validProject.add(validDependency);
        invalidProject.add(validDependency);
        invalidProject.add(invalidDependency);
        task1.setStart(start);
        task1.setEnd(end);
        invalidTask.setStart(badStart);
        invalidTask.setEnd(badEnd);

        // Snapshot manager
        ScheduleChecker checker = new ScheduleChecker();

        // Check the project
        assertTrue(checker.check(validProject));
        assertFalse(checker.check(dummyProject));
        assertFalse(checker.check(invalidProject));

        // Check the warnings
        List<String> warnings = checker.getWarnings();
        assertEquals(9, warnings.size()); // Invalid start and end dates for T3
                                          // are duplicated because the task T3
                                          // is duplicated
        assertTrue(warnings.contains("Unknown task PORTNAWAK1"));
        assertTrue(warnings.contains("Unknown task PORTNAWAK2"));
        assertTrue(warnings.contains("Invalid delay 123"));
        assertTrue(warnings.contains("Invalid project calendar"));
        assertTrue(warnings.contains("Invalid start date for T3"));
        assertTrue(warnings.contains("Invalid end date for T3"));
        assertTrue(warnings.contains("Duplicate task T3"));
    }

}
