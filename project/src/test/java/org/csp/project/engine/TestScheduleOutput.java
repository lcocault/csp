/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.engine;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.TimeZone;

import org.csp.project.model.Project;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.TaskGroup;
import org.csp.project.model.precedence.Dependency;
import org.csp.project.model.precedence.StartsAtEndConstraint;
import org.csp.project.model.task.StartsAtConstraint;
import org.csp.project.model.task.Task;
import org.junit.Before;
import org.junit.Test;

public class TestScheduleOutput {

    @Before
    public void setup() {
        // Environment for date display
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    }

    @Test
    public void testTaskOutput() {

        // Mixture
        Task task = new Task("T1");
        task.setStart(new Date(0));
        task.setEnd(new Date(3600000));

        // Output generator
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ScheduleOutput so = new ScheduleOutput(bos);

        // Produce output
        so.visit(task);

        // Check the result
        assertEquals(
                "[TASK] T1 : Thu Jan 01 00:00:00 GMT 1970 - Thu Jan 01 01:00:00 GMT 1970\n",
                bos.toString());
    }

    @Test
    public void testDependencyOutput() {

        // Mixture
        Dependency cst = new StartsAtEndConstraint();

        // Output generator
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ScheduleOutput so = new ScheduleOutput(bos);

        // Produce output
        so.visit(cst);

        // Nothing expected yet
        assertEquals("", bos.toString());
    }

    @Test
    public void testProjectOutput() {

        // Mixture
        Project project = new Project();

        // Output generator
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ScheduleOutput so = new ScheduleOutput(bos);

        // Produce output
        so.visit(project);

        // Nothing expected yet
        assertEquals("", bos.toString());
    }

    @Test
    public void testProjectItemOutput() {

        // Mixture
        ProjectItem item = new TaskGroup();

        // Output generator
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ScheduleOutput so = new ScheduleOutput(bos);

        // Produce output
        so.visit(item);

        // Nothing expected yet
        assertEquals("", bos.toString());
    }

    @Test
    public void testTaskConstraintOutput() {

        // Mixture
        StartsAtConstraint cst = new StartsAtConstraint();

        // Output generator
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ScheduleOutput so = new ScheduleOutput(bos);

        // Produce output
        so.visit(cst);

        // Nothing expected yet
        assertEquals("", bos.toString());
    }

}
