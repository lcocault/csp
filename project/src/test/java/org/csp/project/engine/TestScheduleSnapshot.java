/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.engine;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.precedence.StartsAtEndConstraint;
import org.csp.project.model.task.StartsAtConstraint;
import org.csp.project.model.task.Task;
import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.ScheduleImpl;
import org.junit.Test;

public class TestScheduleSnapshot {

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(0);
        Date end = new Date(3600000L);
        ProjectCalendar calendar = new ProjectCalendar(start, end, 1000);
        Task task1 = new Task("T1");
        Task task2 = new Task("T2");
        Task task3 = new Task("T3");
        StartsAtEndConstraint cst1 = new StartsAtEndConstraint("T1", "T2", 0);
        StartsAtEndConstraint cst2 = new StartsAtEndConstraint("T2", "T3", 0);
        StartsAtConstraint cst3 = new StartsAtConstraint(new Date(0));
        Project project = new Project("PROJECT", calendar);
        Schedule schedule = new ScheduleImpl("SCHEDULE", new IntValue(0),
                new IntValue(3600));
        project.add(task1);
        project.add(task2);
        project.add(task3);
        project.add(cst1);
        project.add(cst2);
        task1.setActivity(schedule.createActivity("A1"));
        task2.setActivity(schedule.createActivity("A2"));
        task3.setActivity(schedule.createActivity("A3"));
        task1.add(cst3);
        task1.setStart(start);
        task1.setEnd(end);
        task2.setStart(start);
        task2.setEnd(end);
        task3.setStart(start);
        task3.setEnd(end);

        // Snapshot manager
        ScheduleSnapshot so = new ScheduleSnapshot();

        // Change the variables associated with the task activities
        task1.getActivity().getStart().setValue(new IntValue(0));
        task1.getActivity().getEnd().setValue(new IntValue(900));
        task2.getActivity().getStart().setValue(new IntValue(900));
        task2.getActivity().getEnd().setValue(new IntValue(1800));
        task3.getActivity().getStart().setValue(new IntValue(1800));
        task3.getActivity().getEnd().setValue(new IntValue(3600));

        // Produce output
        project.accept(so);

        // Check the result
        assertEquals(0, task1.getStart().getTime());
        assertEquals(900000, task1.getEnd().getTime());
        assertEquals(900000, task2.getStart().getTime());
        assertEquals(1800000, task2.getEnd().getTime());
        assertEquals(1800000, task3.getStart().getTime());
        assertEquals(3600000, task3.getEnd().getTime());
    }

}
