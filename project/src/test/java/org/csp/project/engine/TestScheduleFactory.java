/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.precedence.StartsAtEndConstraint;
import org.csp.project.model.task.StartsAtConstraint;
import org.csp.project.model.task.Task;
import org.junit.Test;

public class TestScheduleFactory {

    @Test
    public void testBasic() {

        // Mixture
        Date start = new Date(0);
        Date end = new Date(3600000L);
        ProjectCalendar calendar = new ProjectCalendar(start, end, 1000);
        Task task1 = new Task("T1");
        Task task2 = new Task("T2");
        Task task3 = new Task("T3");
        StartsAtEndConstraint cst1 = new StartsAtEndConstraint("T1", "T2", 0);
        StartsAtEndConstraint cst2 = new StartsAtEndConstraint("T2", "T3", 0);
        StartsAtConstraint cst3 = new StartsAtConstraint(new Date(0));
        Project project = new Project("PROJECT", calendar);
        project.add(task1);
        project.add(task2);
        project.add(task3);
        project.add(cst1);
        project.add(cst2);
        task1.add(cst3);
        task1.setStart(start);
        task1.setEnd(end);
        task2.setStart(start);
        task2.setEnd(end);
        task3.setStart(start);
        task3.setEnd(end);

        // Schedule factory
        ScheduleFactory factory = new ScheduleFactory();

        // Create the schedule
        project.accept(factory);

        // Check the result
        assertNotNull(factory.getSchedule());
        assertNotNull(task1.getActivity());
        assertEquals(new IntValue(0), task1.getActivity().getStart()
                .getMinValue());
        assertEquals(new IntValue(3600), task1.getActivity().getStart()
                .getMaxValue());
        assertNotNull(task2.getActivity());
        assertNotNull(task3.getActivity());
        assertNotNull(cst1.getConstraint());
        assertNotNull(cst2.getConstraint());
        assertNotNull(cst3.getConstraint());
    }

}
