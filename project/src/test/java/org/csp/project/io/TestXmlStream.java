/* Copyright 2015 LEGA team
 * Licensed to LEGA team under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. LEGA team licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.precedence.StartsAtEndConstraint;
import org.csp.project.model.task.StartsAtConstraint;
import org.csp.project.model.task.Task;
import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.ScheduleImpl;
import org.junit.Test;

public class TestXmlStream {

    @Test
    public void testXmlOutput() {
        try {

            // Mixture
            Date start = new Date(0);
            Date end = new Date(3600000L);
            ProjectCalendar calendar = new ProjectCalendar(start, end, 1000);
            Task task1 = new Task("T1");
            Task task2 = new Task("T2");
            Task task3 = new Task("T3");
            StartsAtEndConstraint cst1 = new StartsAtEndConstraint("T1", "T2",
                    0);
            StartsAtEndConstraint cst2 = new StartsAtEndConstraint("T2", "T3",
                    0);
            StartsAtConstraint cst3 = new StartsAtConstraint(new Date(0));
            Project project = new Project("PROJECT", calendar);
            Schedule schedule = new ScheduleImpl("SCHEDULE", new IntValue(0),
                    new IntValue(3600));
            project.add(task1);
            project.add(task2);
            project.add(task3);
            project.add(cst1);
            project.add(cst2);
            task1.setActivity(schedule.createActivity("A1"));
            task2.setActivity(schedule.createActivity("A2"));
            task3.setActivity(schedule.createActivity("A3"));
            task1.add(cst3);
            task1.setStart(start);
            task1.setEnd(end);
            task2.setStart(start);
            task2.setEnd(end);
            task3.setStart(start);
            task3.setEnd(end);

            // Render it in XML
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            XmlStreamWriter writer = new XmlStreamWriter();
            writer.write(project, os);

            // Parse it
            ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
            XmlStreamReader reader = new XmlStreamReader();
            Project copy = reader.read(is);

            // Check project attributes
            assertEquals("PROJECT", copy.getName());
            assertNotNull(copy.getCalendar());
            assertEquals(start, copy.getCalendar().getStart());
            assertEquals(end, copy.getCalendar().getEnd());

            // Check project dependencies
            assertEquals(2, copy.getDependencies().size());
            assertTrue(copy.getDependencies().get(0) instanceof StartsAtEndConstraint);
            assertEquals("T1", ((StartsAtEndConstraint) copy.getDependencies()
                    .get(0)).getFirst());
            assertEquals("T2", ((StartsAtEndConstraint) copy.getDependencies()
                    .get(0)).getSecond());
            assertEquals(0, ((StartsAtEndConstraint) copy.getDependencies()
                    .get(0)).getDelay());
            assertTrue(copy.getDependencies().get(1) instanceof StartsAtEndConstraint);

            // Check project tasks
            assertEquals(3, copy.getTasks().size());
            assertEquals("T1", copy.getTasks().get(0).getName());
            assertEquals(1, copy.getTasks().get(0).getConstraints().size());
            assertTrue(copy.getTasks().get(0).getConstraints().get(0) instanceof StartsAtConstraint);
            assertEquals(0, ((StartsAtConstraint) copy.getTasks().get(0)
                    .getConstraints().get(0)).getDate().getTime());

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

}
