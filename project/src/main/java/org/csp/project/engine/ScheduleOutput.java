/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.engine;

import java.io.OutputStream;
import java.io.PrintWriter;

import org.csp.project.model.Project;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.project.model.precedence.Dependency;
import org.csp.project.model.task.Task;
import org.csp.project.model.task.TaskConstraint;

/**
 * Display the content of a project. The factory is an instance of a visitor.
 */
public class ScheduleOutput implements Visitor {

    /** Print writer. */
    private PrintWriter writer_;

    /**
     * Constructor.
     * @param os
     *            Output stream to write
     */
    public ScheduleOutput(final OutputStream os) {
        writer_ = new PrintWriter(os);
    }

    /**
     * Process a dependency. {@inheritDoc}
     */
    @Override
    public void visit(final Dependency constraint) {
        // Nothing to do
    }

    /**
     * Process a project. {@inheritDoc}
     */
    @Override
    public void visit(final Project project) {
        // Nothing to do
    }

    /**
     * Generic implementation for a project item. {@inheritDoc}
     */
    @Override
    public void visit(final ProjectItem item) {
        // Nothing to do
    }

    /**
     * Process a task. {@inheritDoc}
     */
    @Override
    public void visit(final Task task) {

        // Display the task
        writer_.println("[TASK] " + task.getName() + " : " + task.getStart() +
                " - " + task.getEnd());
        writer_.flush();
    }

    /**
     * Process a constraint. {@inheritDoc}
     */
    @Override
    public void visit(final TaskConstraint constraint) {
        // Nothing to do
    }

}
