/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.engine;

import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.project.model.precedence.Dependency;
import org.csp.project.model.task.Task;
import org.csp.project.model.task.TaskConstraint;

import org.csp.constraint.model.integer.IntValue;

/**
 * Create a snapshot of a project by assigning reference values to variables.
 * The snapshot computer is an instance of a visitor.
 */
public class ScheduleSnapshot implements Visitor {

    /**
     * Project being processed.
     */
    private Project project_;

    /**
     * Process a dependency. {@inheritDoc}
     */
    @Override
    public void visit(final Dependency constraint) {
        // Nothing to do
    }

    /**
     * Process a project. {@inheritDoc}
     */
    @Override
    public void visit(final Project project) {
        // Keep the project reference
        project_ = project;
    }

    /**
     * Generic implementation for a project item. {@inheritDoc}
     */
    @Override
    public void visit(final ProjectItem item) {
        // Nothing to do
    }

    /**
     * Process a task. {@inheritDoc}
     */
    @Override
    public void visit(final Task task) {

        // Calendar
        final ProjectCalendar cal = project_.getCalendar();

        // Assign the start and end date of the task
        final IntValue start = task.getActivity().getStart().getValue().value();
        final IntValue end = task.getActivity().getEnd().getValue().value();
        task.setStart(cal.convertDate(start));
        task.setEnd(cal.convertDate(end));
    }

    /**
     * Process a constraint. {@inheritDoc}
     */
    @Override
    public void visit(final TaskConstraint constraint) {
        // Nothing to do
    }

}
