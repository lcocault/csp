/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.engine;

import java.util.HashMap;
import java.util.Map;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.project.model.precedence.Dependency;
import org.csp.project.model.task.Task;
import org.csp.project.model.task.TaskConstraint;
import org.csp.scheduler.model.Activity;
import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.ScheduleImpl;

/**
 * Build a scheduler object from the project. The factory is an instance of a
 * visitor.
 */
public class ScheduleFactory implements Visitor {

    /**
     * Project as input of the factory.
     */
    private Project project_;

    /**
     * Schedule built by the factory.
     */
    private Schedule schedule_;

    /**
     * Last visited task.
     */
    private Task currentTask_;

    /**
     * Association map between task names and task.
     */
    private Map<String, Task> tasks_;

    /**
     * Constructor.
     */
    public ScheduleFactory() {
        tasks_ = new HashMap<String, Task>();
    }

    /**
     * Get the schedule built.
     * @return Schedule built
     */
    public Schedule getSchedule() {
        return schedule_;
    }

    /**
     * Process a dependency. {@inheritDoc}
     */
    @Override
    public void visit(final Dependency constraint) {

        // Get the activities in the association map
        final Task first = tasks_.get(constraint.getFirst());
        final Task second = tasks_.get(constraint.getSecond());

        // Create the constraint
        constraint
                .createSchedulerConstraint(project_, schedule_, first, second);
    }

    /**
     * Process a project. {@inheritDoc}
     */
    @Override
    public void visit(final Project project) {

        // Compute start and end of the schedule
        final ProjectCalendar cal = project.getCalendar();
        final IntValue start = new IntValue((int) cal.convertDate(cal
                .getStart()));
        final IntValue end = new IntValue((int) cal.convertDate(cal.getEnd()));

        // Store attributes
        project_ = project;
        schedule_ = new ScheduleImpl(project.getName(), start, end);
    }

    /**
     * Generic implementation for a project item. {@inheritDoc}
     */
    @Override
    public void visit(final ProjectItem item) {
        // Nothing to do
    }

    /**
     * Process a task. {@inheritDoc}
     */
    @Override
    public void visit(final Task task) {

        // Create the activity
        final Activity activity = schedule_.createActivity(task.getName());
        task.setActivity(activity);

        // Register the current task
        currentTask_ = task;
        tasks_.put(task.getName(), task);
    }

    /**
     * Process a constraint. {@inheritDoc}
     */
    @Override
    public void visit(final TaskConstraint constraint) {
        constraint.createSchedulerConstraint(project_, schedule_, currentTask_);
    }

}
