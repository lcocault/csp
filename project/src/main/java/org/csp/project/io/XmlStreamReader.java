/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.io;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.csp.project.model.Project;

/**
 * Read an XML stream to load the model of the scheduling engine.
 */
public class XmlStreamReader {

    /**
     * Read an XML and create the corresponding project.
     * @param xmlStream
     *            XML stream to read
     * @return The corresponding project model
     * @throws JAXBException
     *             Impossible to parse the XML input stream
     */
    public Project read(final InputStream xmlStream) throws JAXBException {

        // JAXB tools
        final JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);

        // Deserialize the data model
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        final Project project = (Project) jaxbUnmarshaller.unmarshal(xmlStream);

        return project;
    }
}
