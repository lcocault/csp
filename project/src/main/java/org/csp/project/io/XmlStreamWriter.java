/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.io;

import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.csp.project.model.Project;

/**
 * Write the model of the scheduling engine into an XML stream.
 */
public class XmlStreamWriter {

    /**
     * Write a scheduling project into an XML stream.
     * @param project
     *            Project model to write
     * @param xmlStream
     *            XML stream to write
     * @throws JAXBException
     *             Impossible to write out the project model
     */
    public void write(final Project project, final OutputStream xmlStream)
        throws JAXBException {

        // JAXB tools
        final JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // Output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        // Serialize the data model
        jaxbMarshaller.marshal(project, xmlStream);
    }
}
