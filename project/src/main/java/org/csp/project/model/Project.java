/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.csp.project.model.precedence.Dependency;

/**
 * Project to schedule.
 */
@XmlRootElement
public class Project extends TaskGroup {

    /** Calendar of the project. */
    private ProjectCalendar calendar_;

    /** List of dependencies of the project. */
    private List<Dependency> dependencies_;

    /**
     * Constructor.
     */
    public Project() {
        dependencies_ = new ArrayList<Dependency>();
    }

    /**
     * Constructor.
     * @param name
     *            Name of the project
     * @param calendar
     *            Calendar of the project
     */
    public Project(final String name, final ProjectCalendar calendar) {
        super(name);
        calendar_ = calendar;
        dependencies_ = new ArrayList<Dependency>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final Visitor visitor) {

        // Visit the project
        visitor.visit(this);

        // Visit the project as a group
        super.accept(visitor);

        // Visit dependencies (must be after the visit of the groups of tasks so
        // that the references to tasks in the dependcies are valid).
        for (Dependency dependency : dependencies_) {
            dependency.accept(visitor);
        }

    }

    /**
     * Add a new dependency in the project.
     * @param dependency
     *            Dependency to add
     */
    public void add(final Dependency dependency) {
        dependencies_.add(dependency);
    }

    /**
     * Get the project calendar.
     * @return Calendar of the project
     */
    public ProjectCalendar getCalendar() {
        return calendar_;
    }

    /**
     * Get the list of dependencies.
     * @return Project dependencies
     */
    public List<Dependency> getDependencies() {
        return dependencies_;
    }

    /**
     * Set the project calendar.
     * @param calendar
     *            Calendar of the project
     */
    @XmlElement
    public void setCalendar(final ProjectCalendar calendar) {
        calendar_ = calendar;
    }

    /**
     * Set the list of dependencies.
     * @param dependencies
     *            Project dependencies
     */
    @XmlElement
    public void setDependencies(final List<Dependency> dependencies) {
        dependencies_ = dependencies;
    }

}
