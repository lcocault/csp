/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.precedence;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.csp.project.model.Project;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.project.model.task.Task;
import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.precedence.PrecedenceConstraint;

/**
 * Dependency between tasks.
 */
@XmlSeeAlso({ StartsAtEndConstraint.class })
public abstract class Dependency implements ProjectItem {

    /** Name of the first task in the dependency. */
    private String first_;

    /** Name of the second task in the dependency. */
    private String second_;

    /** Delay between the activities dates. */
    private long delay_;

    /** Precedence constraint from the schedule. */
    private PrecedenceConstraint constraint_;

    /**
     * Constructor.
     */
    public Dependency() {
        first_ = null;
        second_ = null;
        delay_ = 0;
        constraint_ = null;
    }

    /**
     * Constructor.
     * @param first
     *            Name of the first task in the dependency
     * @param second
     *            Name of the second task in the dependency
     * @param delay
     *            between the activities dates
     */
    public Dependency(final String first, final String second, final long delay) {
        first_ = first;
        second_ = second;
        delay_ = delay;
        constraint_ = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final Visitor visitor) {
        // Visit the current object
        visitor.visit(this);
    }

    /**
     * Create the constraint in the schedule.
     * @param project
     *            Project in the frame of which the constraint is declared
     * @param schedule
     *            Schedule to create the constraint
     * @param first
     *            First task associated with the constraint
     * @param second
     *            Second task associated with the constraint
     */
    public abstract void createSchedulerConstraint(final Project project,
            final Schedule schedule, final Task first, final Task second);

    /**
     * Get the precedence constraint associated with that dependency.
     * @return Time constraint
     */
    public PrecedenceConstraint getConstraint() {
        return constraint_;
    }

    /**
     * Get the delay between the activities dates.
     * @return Delay between the activities
     */
    @XmlElement
    public long getDelay() {
        return delay_;
    }

    /**
     * Get the name of the first task in the dependency.
     * @return Name of the first task
     */
    @XmlElement
    public String getFirst() {
        return first_;
    }

    /**
     * Get the name of the second task in the dependency.
     * @return Name of the second task
     */
    @XmlElement
    public String getSecond() {
        return second_;
    }

    /**
     * Set the precedence constraint of the dependency.
     * @param constraint
     *            Precedence constraint
     */
    protected void setConstraint(final PrecedenceConstraint constraint) {
        constraint_ = constraint;
    }

    /**
     * Set the delay of the dependency.
     * @param delay
     *            Delay of the dependency
     */
    public void setDelay(final long delay) {
        delay_ = delay;
    }

    /**
     * Set the name of the first task in the dependency.
     * @param first
     *            Name of the first task
     */
    public void setFirst(final String first) {
        first_ = first;
    }

    /**
     * Set the name of the second task in the dependency.
     * @param second
     *            Name of the second task
     */
    public void setSecond(final String second) {
        second_ = second;
    }

}
