/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.scheduler.model.Activity;

/**
 * Task to schedule.
 */
public class Task implements ProjectItem {

    /**
     * Name of the project.
     */
    private String name_;

    /**
     * Computed start date.
     */
    private Date start_;

    /**
     * Computed end date.
     */
    private Date end_;

    /**
     * Constraints of the task.
     */
    private List<TaskConstraint> constraints_;

    /**
     * Activity associated with the task.
     */
    private Activity activity_;

    /**
     * Constructor.
     */
    public Task() {
        name_ = "NoName";
        constraints_ = new ArrayList<TaskConstraint>();
    }

    /**
     * Constructor.
     * @param name
     *            Name of the project
     */
    public Task(final String name) {
        name_ = name;
        constraints_ = new ArrayList<TaskConstraint>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final Visitor visitor) {

        // First visit the task
        visitor.visit(this);

        // Then visit the constraints
        for (TaskConstraint constraint : constraints_) {
            visitor.visit(constraint);
        }
    }

    /**
     * Add a new constraint to the task.
     * @param constraint
     *            Constraint to add
     */
    public void add(final TaskConstraint constraint) {
        constraints_.add(constraint);
    }

    /**
     * Get the activity associated with the task.
     * @return Activity associated with the task
     */
    public Activity getActivity() {
        return activity_;
    }

    /**
     * Get the constraints associated with the task.
     * @return List of constraints
     */
    public List<TaskConstraint> getConstraints() {
        return constraints_;
    }

    /**
     * Get the end date of the task.
     * @return End date of the task
     */
    public Date getEnd() {
        return end_;
    }

    /**
     * Get the name of the task.
     * @return Name of the task
     */
    public String getName() {
        return name_;
    }

    /**
     * Get the start date of the task.
     * @return Start date of the task
     */
    public Date getStart() {
        return start_;
    }

    /**
     * Set the activity corresponding to the task.
     * @param activity
     *            Activity to associate with the task
     */
    @XmlTransient
    public void setActivity(final Activity activity) {
        activity_ = activity;
    }

    /**
     * Set the constraints associated with the task.
     * @param constraint
     *            List of constraints
     */
    @XmlElement
    public void setConstraints(final List<TaskConstraint> constraint) {
        constraints_ = constraint;
    }

    /**
     * Set the end date of the task.
     * @param end
     *            End date of the task
     */
    @XmlElement
    public void setEnd(final Date end) {
        end_ = end;
    }

    /**
     * Set the name of the task.
     * @param name
     *            Name of the task
     */
    @XmlElement
    public void setName(final String name) {
        name_ = name;
    }

    /**
     * Set the start date of the task.
     * @param start
     *            Start date of the task
     */
    @XmlElement
    public void setStart(final Date start) {
        start_ = start;
    }

}
