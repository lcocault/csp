/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.csp.constraint.model.integer.IntValue;

/**
 * Project calendar used to convert dates into "time slots". Indeed, the
 * accuracy of the project planning is not necessary the same as the Java date.
 */
@XmlRootElement
public class ProjectCalendar {

    /** Project start date. */
    private Date start_;

    /** Project end date. */
    private Date end_;

    /** Duration of a time slot. */
    private long slotDuration_;

    /**
     * Constructor.
     */
    public ProjectCalendar() {
        start_ = new Date(0);
        end_ = new Date();
        slotDuration_ = 3600000L;
    }

    /**
     * Constructor.
     * @param start
     *            Project start date
     * @param end
     *            Project end date
     * @param slotDuration
     *            Duration of a slot in milliseconds
     */
    public ProjectCalendar(final Date start, final Date end,
            final long slotDuration) {
        start_ = start;
        end_ = end;
        slotDuration_ = slotDuration;
    }

    /**
     * Convert a date into a time slot number.
     * @param date
     *            Date to convert
     * @return Number of schedule slot
     */
    public long convertDate(final Date date) {
        return (date.getTime() - start_.getTime()) / slotDuration_;
    }

    /**
     * Convert a slot number into a date.
     * @param slot
     *            Time slot to convert
     * @return Converted date
     */
    public Date convertDate(final IntValue slot) {
        return new Date(start_.getTime() + slot.getIntValue() * slotDuration_);
    }

    /**
     * Convert a delay into a number of slots.
     * @param delay
     *            Delay to convert
     * @return Number of slots corresponding to the delay
     */
    public int convertDelay(final long delay) {
        return (int) (delay / slotDuration_);
    }

    /**
     * Get the end date of the project.
     * @return End date of the project
     */
    public Date getEnd() {
        return end_;
    }

    /**
     * Get the duration of the time slot.
     * @return Duration of the schedule time slot
     */
    public long getSlotDuration() {
        return slotDuration_;
    }

    /**
     * Get the start date of the project.
     * @return Start date of the project
     */
    public Date getStart() {
        return start_;
    }

    /**
     * Check if the given date is valid.
     * @param date
     *            Date to check
     * @return "true" if the date is valid, in other words if the delta with the
     *         start date is a valid delay
     */
    public boolean isValidDate(final Date date) {
        return isValidDelay(date.getTime() - start_.getTime());
    }

    /**
     * Test if the delay is valid.
     * @param delay
     *            Delay to check
     * @return "true" is the delay is valid (an integer number of slots)
     */
    public boolean isValidDelay(final long delay) {
        return (delay % slotDuration_) == 0;
    }

    /**
     * Set the end date of the project.
     * @param end
     *            End date of the project
     */
    @XmlElement
    public void setEnd(final Date end) {
        end_ = end;
    }

    /**
     * Set the duration of the time slot.
     * @param slotDuration
     *            Duration of the schedule time slot
     */
    @XmlElement
    public void setSlotDuration(final long slotDuration) {
        slotDuration_ = slotDuration;
    }

    /**
     * Set the start date of the project.
     * @param start
     *            Start date of the project
     */
    @XmlElement
    public void setStart(final Date start) {
        start_ = start;
    }

}
