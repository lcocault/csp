/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.task;

import java.util.Date;

import org.csp.constraint.model.integer.IntValue;
import org.csp.project.model.Project;
import org.csp.scheduler.model.Schedule;

/**
 * An activity starts at a specific date.
 */
public class StartsAtConstraint extends FixedDateConstraint {

    /**
     * Constructor.
     */
    public StartsAtConstraint() {
    }

    /**
     * Constructor.
     * @param date
     *            Fixed date associated with the constraint
     */
    public StartsAtConstraint(final Date date) {
        super(date);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createSchedulerConstraint(final Project project,
            final Schedule schedule, final Task task) {
        final int date = (int) project.getCalendar().convertDate(getDate());
        setConstraint(schedule.startAt(task.getActivity(), new IntValue(date)));
    }
}
