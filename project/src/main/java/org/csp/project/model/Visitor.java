/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model;

import org.csp.project.model.precedence.Dependency;
import org.csp.project.model.task.Task;
import org.csp.project.model.task.TaskConstraint;

/**
 * Visitor of project items.
 */
public interface Visitor {

    /**
     * Visit a dependency constraint.
     * @param constraint
     *            Constraint to visit
     */
    void visit(final Dependency constraint);

    /**
     * Visit a project.
     * @param project
     *            Project to visit
     */
    void visit(final Project project);

    /**
     * Visit a project item.
     * @param item
     *            Item to visit
     */
    void visit(final ProjectItem item);

    /**
     * Visit a task.
     * @param task
     *            Task to visit
     */
    void visit(final Task task);

    /**
     * Visit a task constraint.
     * @param constraint
     *            Constraint to visit
     */
    void visit(final TaskConstraint constraint);
}
