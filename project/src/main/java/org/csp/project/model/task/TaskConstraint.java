/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.task;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.csp.project.model.Project;
import org.csp.project.model.ProjectItem;
import org.csp.project.model.Visitor;
import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.time.TimeConstraint;

/**
 * Constraint applicable to a task.
 */
@XmlSeeAlso({ StartsAtConstraint.class, EndsAtConstraint.class })
public abstract class TaskConstraint implements ProjectItem {

    /** Time constraint from the schedule. */
    private TimeConstraint constraint_;

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final Visitor visitor) {
        // Visit the current object
        visitor.visit(this);
    }

    /**
     * Create the constraint in the schedule.
     * @param project
     *            Project in the frame of which the constraint is declared
     * @param schedule
     *            Schedule to create the constraint
     * @param task
     *            Task associated with the constraint
     */
    public abstract void createSchedulerConstraint(final Project project,
            final Schedule schedule, final Task task);

    /**
     * Get the time constraint associated with that task constraint.
     * @return Time constraint
     */
    public TimeConstraint getConstraint() {
        return constraint_;
    }

    /**
     * Set the time constraint associated with that task constraint.
     * @param constraint
     *            Time constraint
     */
    protected void setConstraint(final TimeConstraint constraint) {
        constraint_ = constraint;
    }

}
