/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model.task;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

/**
 * Fixed date constraint applicable to a task.
 */
public abstract class FixedDateConstraint extends TaskConstraint {

    /** Fixed date associated with the constraint. */
    private Date date_;

    /**
     * Constructor.
     */
    public FixedDateConstraint() {
        date_ = null;
    }

    /**
     * Constructor.
     * @param date
     *            Fixed date associated with the constraint
     */
    public FixedDateConstraint(final Date date) {
        date_ = date;
    }

    /**
     * Get the date of the constraint.
     * @return Date of the constraint
     */
    public Date getDate() {
        return date_;
    }

    /**
     * Set the date of the constraint.
     * @param date
     *            Date of the constraint
     */
    @XmlElement
    public void setDate(final Date date) {
        date_ = date;
    }

}
