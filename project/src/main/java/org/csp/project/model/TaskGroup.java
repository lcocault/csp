/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.project.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.csp.project.model.task.Task;

/**
 * A group of tasks in a project.
 */
@XmlRootElement
public class TaskGroup implements ProjectItem {

    /**
     * Name of the group.
     */
    private String name_;

    /**
     * Main groups.
     */
    private List<TaskGroup> groups_;

    /**
     * Tasks.
     */
    private List<Task> tasks_;

    /**
     * Constructor.
     */
    public TaskGroup() {
        name_ = "NoName";
        groups_ = new ArrayList<TaskGroup>();
        tasks_ = new ArrayList<Task>();
    }

    /**
     * Constructor.
     * @param name
     *            Name of the task group
     */
    public TaskGroup(final String name) {
        name_ = name;
        groups_ = new ArrayList<TaskGroup>();
        tasks_ = new ArrayList<Task>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void accept(final Visitor visitor) {

        // Visit the group
        visitor.visit(this);

        // Visit subgroups
        for (TaskGroup group : groups_) {
            group.accept(visitor);
        }

        // Visit tasks
        for (Task task : tasks_) {
            task.accept(visitor);
        }
    }

    /**
     * Add a task to the group.
     * @param task
     *            Task to add
     */
    public void add(final Task task) {
        tasks_.add(task);
    }

    /**
     * Add a subgroup to the group.
     * @param group
     *            Group to add
     */
    public void add(final TaskGroup group) {
        groups_.add(group);
    }

    /**
     * Get all the tasks recursively.
     * @param tasks
     *            List of tasks to complete. In order to avoid the creation of a
     *            list at each recursive call, the list is given as an in-out
     *            parameter.
     */
    public void getAllTasks(final List<Task> tasks) {
        // Direct tasks
        tasks.addAll(tasks_);
        // Then tasks of groups
        for (TaskGroup group : groups_) {
            group.getAllTasks(tasks);
        }
    }

    /**
     * Get the list of subgroups.
     * @return List of subgroups
     */
    public List<TaskGroup> getGroups() {
        return groups_;
    }

    /**
     * Get the name of the group.
     * @return Name of the group
     */
    public String getName() {
        return name_;
    }

    /**
     * Get the list of tasks.
     * @return List of tasks
     */
    public List<Task> getTasks() {
        return tasks_;
    }

    /**
     * Set the list of subgroups.
     * @param groups
     *            List of subgroups
     */
    @XmlElement
    public void setGroups(final List<TaskGroup> groups) {
        groups_ = groups;
    }

    /**
     * Set the name of the group.
     * @param name
     *            Name of the group
     */
    @XmlElement
    public void setName(final String name) {
        name_ = name;
    }

    /**
     * Set the list of tasks.
     * @param tasks
     *            List of tasks
     */
    @XmlElement
    public void setTasks(final List<Task> tasks) {
        tasks_ = tasks;
    }
}
