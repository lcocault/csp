package org.csp.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import org.csp.project.engine.ScheduleChecker;
import org.csp.project.engine.ScheduleFactory;
import org.csp.project.engine.ScheduleOutput;
import org.csp.project.engine.ScheduleSnapshot;
import org.csp.project.io.XmlStreamReader;
import org.csp.project.io.XmlStreamWriter;
import org.csp.project.model.Project;
import org.csp.project.model.ProjectCalendar;
import org.csp.project.model.TaskGroup;
import org.csp.project.model.precedence.StartsAtEndConstraint;
import org.csp.project.model.task.EndsAtConstraint;
import org.csp.project.model.task.StartsAtConstraint;
import org.csp.project.model.task.Task;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.solver.BasicSolver;
import org.csp.constraint.solver.Problem;
import org.csp.constraint.solver.Solver;

public class Engine {

    /**
     * @param args
     */
    public static void main(String[] args) {

        // Lets's work GMT to make it easier
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        // Create a project
        ProjectCalendar calendar = new ProjectCalendar(new Date(0), new Date(
                86400000), 3600000);
        Project project = new Project("MyProject", calendar);
        TaskGroup g1 = new TaskGroup("G1");
        TaskGroup g2 = new TaskGroup("G2");

        // Tasks
        Task t11 = new Task("T11");
        Task t12 = new Task("T12");
        Task t21 = new Task("T21");
        Task t22 = new Task("T22");
        project.add(g1);
        project.add(g2);
        g1.add(t11);
        g1.add(t12);
        g2.add(t21);
        g2.add(t22);
        t11.add(new StartsAtConstraint(new Date(0)));
        t12.add(new EndsAtConstraint(new Date(7200000)));
        project.add(new StartsAtEndConstraint("T12", "T21", 3600000));
        project.add(new StartsAtEndConstraint("T21", "T22", 7200000));

        try {

            // Check the schedule
            ScheduleChecker checker = new ScheduleChecker();
            if (checker.check(project)) {
                System.out.println("=== Valid project ===");
            }

            // Write a file
            XmlStreamWriter writer = new XmlStreamWriter();
            File file = new File("test.xml");
            writer.write(project, new FileOutputStream(file));

            // Read a file
            XmlStreamReader reader = new XmlStreamReader();
            Project read = reader.read(new FileInputStream(file));

            // Visit the project
            ScheduleFactory factory = new ScheduleFactory();
            read.accept(factory);

            // Solve the problem
            Problem<IntValue> pb = factory.getSchedule();
            Solver<IntValue> solver = new BasicSolver<IntValue>(pb);
            if (pb.startSearch() &&
                    solver.branchAndBound() == Solver.SearchResult.SOLUTION_FOUND) {

                // Set values
                ScheduleSnapshot snap = new ScheduleSnapshot();
                read.accept(snap);

                // Display the project
                ScheduleOutput output = new ScheduleOutput(System.out);
                read.accept(output);

                // Write the XML file one more
                file.delete();
                writer.write(read, new FileOutputStream(file));

                // Read it again
                read = reader.read(new FileInputStream(file));
                read.accept(output);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
