/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.time;

import org.csp.scheduler.model.Activity;

import org.csp.constraint.model.UnaryConstraint;
import org.csp.constraint.model.integer.IntValue;

/**
 * Represents a constraint on one date or on the duration of an activity.
 */
public abstract class TimeConstraint {

    /**
     * Activity concerned by the time constraint.
     */
    private Activity activity_;

    /**
     * Constraint on the activity.
     */
    private UnaryConstraint<IntValue> constraint_;

    /**
     * Constructor.
     * @param activity
     *            Activity concerned by the time constraint
     * @param constraint
     *            Constraint on the activity
     */
    public TimeConstraint(final Activity activity,
            final UnaryConstraint<IntValue> constraint) {
        activity_ = activity;
        constraint_ = constraint;
    }

    /**
     * Get the activity associated with the constraint.
     * @return Activity of the constraint
     */
    public Activity getActivity() {
        return activity_;
    }

    /**
     * Get the unary constraint associated with the time constraint.
     * @return Unary constraint
     */
    public UnaryConstraint<IntValue> getConstraint() {
        return constraint_;
    }
}
