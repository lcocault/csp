/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model;

import org.csp.constraint.model.Constraint;
import org.csp.constraint.model.integer.EqualToSum;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.model.integer.IntVarImpl;

/**
 * Implementation of an activity.
 */
public class ActivityImpl implements Activity {

    /**
     * Name of the activity.
     */
    private String name_;

    /**
     * Start date (number of "intervals" elapsed between the start of the
     * activity and the beginning of the schedule).
     */
    private IntVar start_;

    /**
     * Stop date (number of "intervals" elapsed between the end of the activity
     * and the beginning of the schedule).
     */
    private IntVar end_;

    /**
     * Duration of the activity in "intervals".
     */
    private IntVar duration_;

    /**
     * Internal duration constraint (between dates and duration).
     */
    private EqualToSum durationConstraint_;

    /**
     * Constructor with no date specified.
     * @param name
     *            Name of the activity
     * @param scheduleStart
     *            Start date for the schedule
     * @param scheduleEnd
     *            End date for the schedule
     */
    public ActivityImpl(final String name, final IntValue scheduleStart,
            final IntValue scheduleEnd) {

        // Schedule limits
        final int min = scheduleStart.getIntValue();
        final int max = scheduleEnd.getIntValue();

        // Name activity
        name_ = name;

        // Create the internal variables
        start_ = new IntVarImpl(min, max, name + "/start");
        end_ = new IntVarImpl(min, max, name + "/end");
        duration_ = new IntVarImpl(0, max - min, name + "/duration");

        // Create the internal constraints
        durationConstraint_ = new EqualToSum(start_, duration_, end_);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntVar getDuration() {
        return duration_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Constraint<IntValue> getDurationConstraint() {
        return durationConstraint_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getEarliestEndTime() {
        return end_.getMinValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getEarliestStartTime() {
        return start_.getMinValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntVar getEnd() {
        return end_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getLatestEndTime() {
        return end_.getMaxValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getLatestStartTime() {
        return start_.getMaxValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getMinProcessingTime() {
        return duration_.getMinValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return name_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntVar getStart() {
        return start_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntVar getProcessingTime() {
        // In the case of a non interruptible activity, the workload is exactly
        // the same as the duration.
        return duration_;
    }

}
