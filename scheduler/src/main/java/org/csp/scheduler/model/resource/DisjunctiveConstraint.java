/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.resource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.csp.constraint.model.InvalidAccessException;
import org.csp.constraint.model.UnsizedConstraint;
import org.csp.constraint.model.Variable;
import org.csp.constraint.model.integer.IntValue;
import org.csp.scheduler.model.Activity;

/**
 * Represents a simple disjunctive constraint. The algorithm implemented should
 * be applicable to both preemptive and non-preemptive problems.
 */
class DisjunctiveConstraint extends UnsizedConstraint<IntValue> {

    /** Activities concerned by the constraint. */
    private Set<Activity> activities_;

    /**
     * Constructor.
     * @param resource
     *            Unary resource the constraint apply to
     */
    public DisjunctiveConstraint(final UnaryResource resource) {
        super(resource.getName(), new ArrayList<Variable<IntValue>>());

        // Create a collection of activities
        activities_ = new HashSet<Activity>();
    }

    /**
     * Add the activity to the constraint.
     * @param activity
     *            Activity added
     */
    public void addActivity(final Activity activity) {
        if (isPropagationEnabled()) {
            // The activity cannot be added once the constraint propagation is
            // enabled
            throw new InvalidAccessException("Cannot add an activity to " +
                    toString() + " when propagation is enabled");
        } else {
            // Add the activity
            activities_.add(activity);
            addVariable(activity.getStart());
            addVariable(activity.getEnd());
            addVariable(activity.getProcessingTime());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propagate() {

        // Copy the list of activities in an array
        final Activity[] activities = new Activity[activities_.size()];
        activities_.toArray(activities);

        // Propagate in the chronological order
        propagateDirect(activities);
        // Propagate in the reverse chronological order
        propagateReverse(activities);
    }

    /**
     * Propagate in the chronological order. The general principle of this part
     * of the simple implementation of the constraint consists in computing, for
     * each activity, the minimum processing time of activities that must
     * execute before it. The new start time of the each activity is computed
     * with the start time of the first activity and the computed processing
     * time.
     * @param activities
     *            List of activities
     */
    private void propagateDirect(final Activity[] activities) {

        // Run through the list of activities
        for (int i = 0; i < activities.length; i++) {

            // The minimum processing time designates the minimum processing
            // time that must be applied guaranteed before the current activity.
            int minProcessingTime = 0;

            // The first activity (minimum earliest start time).
            Activity first = activities[i];
            final IntValue eet = activities[i].getEarliestEndTime();

            // Iterate on all the activities
            for (int j = 0; j < activities.length; j++) {

                // Update the processing time
                if (i != j &&
                        activities[j].getLatestEndTime().compareTo(eet) <= 0) {
                    minProcessingTime += activities[j].getMinProcessingTime()
                            .getIntValue();
                }

                // Update the first activity
                if (activities[j].getEarliestStartTime().compareTo(
                        first.getEarliestStartTime()) < 0) {
                    first = activities[j];
                }
            }

            // Change the minimum value of the current activity with the minimum
            // processing time of activities that must run before the current
            // activity.
            final IntValue newMin = new IntValue(first.getEarliestStartTime()
                    .getIntValue() + minProcessingTime);
            activities[i].getStart().reduceWithMinValue(newMin);
        }
    }

    /**
     * Propagate in the reverse chronological order. The general principle of
     * this part of the simple implementation of the constraint consists in
     * computing, for each activity, the minimum processing time of activities
     * that must execute after it. The new end time of the current activity is
     * computed with the end time of the last activity and the computed
     * processing time.
     * @param activities
     *            List of activities
     */
    private void propagateReverse(final Activity[] activities) {

        // Run through the list of activities
        for (int i = 0; i < activities.length; i++) {

            // The minimum processing time designates the minimum processing
            // time that must be applied guaranteed after the current activity.
            int minProcessingTime = 0;

            // The last activity (maximum latest end time).
            Activity last = activities[i];
            final IntValue lst = activities[i].getLatestStartTime();

            // Iterate on all the activities
            for (int j = 0; j < activities.length; j++) {

                // Update the processing time
                if (i != j &&
                        activities[j].getEarliestStartTime().compareTo(lst) >= 0) {
                    minProcessingTime += activities[j].getMinProcessingTime()
                            .getIntValue();
                }

                // Update the last activity
                if (activities[j].getLatestEndTime().compareTo(
                        last.getLatestEndTime()) > 0) {
                    last = activities[j];
                }
            }

            // Change the maximum value of the current activity with the minimum
            // processing time of activities that must run after the current
            // activity.
            final IntValue newMax = new IntValue(last.getLatestEndTime()
                    .getIntValue() - minProcessingTime);
            activities[i].getEnd().reduceWithMaxValue(newMax);
        }
    }
}
