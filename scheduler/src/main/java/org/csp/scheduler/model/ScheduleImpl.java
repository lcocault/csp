/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model;

import java.util.ArrayList;
import java.util.List;

import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.solver.SingleThreadIntegerProblem;
import org.csp.scheduler.model.precedence.EndAtStart;
import org.csp.scheduler.model.precedence.EndBeforeStart;
import org.csp.scheduler.model.precedence.PrecedenceConstraint;
import org.csp.scheduler.model.resource.UnaryResource;
import org.csp.scheduler.model.time.EndAt;
import org.csp.scheduler.model.time.StartAt;
import org.csp.scheduler.model.time.TimeConstraint;

/**
 * Represents a schedule. A schedule is an integer problem in which the
 * variables are the start and end dates of the tasks.
 */
public class ScheduleImpl extends SingleThreadIntegerProblem implements Schedule {

    /** Name of the schedule. */
    private String name_;

    /** Start of the schedule. */
    private IntValue start_;

    /** End of the schedule. */
    private IntValue end_;

    /** List of the schedule activities. */
    private List<Activity> activities_;

    /** List of precedence constraints. */
    private List<PrecedenceConstraint> precedences_;

    /** List of time constraints. */
    private List<TimeConstraint> times_;

    /**
     * Constructor.
     * @param name
     *            Name of the schedule
     * @param start
     *            Start of the schedule
     * @param end
     *            End of the schedule
     */
    public ScheduleImpl(final String name, final IntValue start,
            final IntValue end) {
        name_ = name;
        start_ = start;
        end_ = end;
        activities_ = new ArrayList<Activity>();
        precedences_ = new ArrayList<PrecedenceConstraint>();
        times_ = new ArrayList<TimeConstraint>();
    }

    /**
     * Add a precedence constraint to the schedule.
     * @param constraint
     *            Constraint added to the schedule
     */
    private void add(final PrecedenceConstraint constraint) {
        add(constraint.getConstraint());
    }

    /**
     * Add a time constraint to the schedule.
     * @param constraint
     *            Constraint added to the schedule
     */
    private void add(final TimeConstraint constraint) {
        add(constraint.getConstraint());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Activity createActivity(final String name) {

        // Create and register the activity
        final Activity activity = new ActivityImpl(name_ + "/" + name, start_,
                end_);
        activities_.add(activity);

        // Register the variables of the activity
        add(activity.getStart());
        add(activity.getEnd());
        add(activity.getDuration());

        // Register the constraints of the activity
        add(activity.getDurationConstraint());
        // TODO add(activity.getWorkloadConstraint());

        return activity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UnaryResource createUnaryResource(final String name) {
        return new UnaryResource(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeConstraint endsAt(final Activity activity, final IntValue date) {

        // Create the constraint
        final TimeConstraint cst = new EndAt(activity, date);

        // Register the constraint
        times_.add(cst);
        add(cst);

        return cst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrecedenceConstraint endsAtStart(final Activity before,
            final Activity after, final IntValue delay) {

        // Create the constraint
        final PrecedenceConstraint cst = new EndAtStart(before, after, delay);

        // Register the constraint
        precedences_.add(cst);
        add(cst);

        return cst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrecedenceConstraint endsBeforeStart(final Activity before,
            final Activity after, final IntValue delay) {

        // Create the constraint
        final PrecedenceConstraint cst = new EndBeforeStart(before, after,
                delay);

        // Register the constraint
        precedences_.add(cst);
        add(cst);

        return cst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeConstraint startAt(final Activity activity, final IntValue date) {

        // Create the constraint
        final TimeConstraint cst = new StartAt(activity, date);

        // Register the constraint
        times_.add(cst);
        add(cst);

        return cst;
    }

}
