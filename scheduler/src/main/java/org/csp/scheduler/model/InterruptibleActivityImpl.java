/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model;

import org.csp.constraint.model.Constraint;
import org.csp.constraint.model.integer.GreaterEqual;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;
import org.csp.constraint.model.integer.IntVarImpl;

/**
 * Implementation of an interruptible activity.
 */
public class InterruptibleActivityImpl extends ActivityImpl implements InterruptibleActivity {

    /**
     * Processing time of the activity in "intervals".
     */
    private IntVar processingTime_;

    /**
     * Internal processing time constraint (between processing time and
     * duration).
     */
    private GreaterEqual processingTimeConstraint_;

    /**
     * Constructor with no date specified.
     * @param name
     *            Name of the activity
     * @param scheduleStart
     *            Start date for the schedule
     * @param scheduleEnd
     *            End date for the schedule
     */
    public InterruptibleActivityImpl(final String name,
            final IntValue scheduleStart, final IntValue scheduleEnd) {

        // Call activity constructor
        super(name, scheduleStart, scheduleEnd);

        // Schedule limits
        final int min = scheduleStart.getIntValue();
        final int max = scheduleEnd.getIntValue();

        // Create the internal variables
        processingTime_ = new IntVarImpl(0, max - min, name + "/processingTime");

        // Create the internal constraints
        processingTimeConstraint_ = new GreaterEqual(getDuration(),
                processingTime_, new IntValue(0));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntValue getMinProcessingTime() {
        return processingTime_.getMinValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IntVar getProcessingTime() {
        return processingTime_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Constraint<IntValue> getProcessingTimeConstraint() {
        return processingTimeConstraint_;
    }

}
