/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model;

import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.solver.Problem;
import org.csp.scheduler.model.precedence.PrecedenceConstraint;
import org.csp.scheduler.model.resource.UnaryResource;
import org.csp.scheduler.model.time.TimeConstraint;

/**
 * Represents a schedule. A schedule is an integer problem in which the
 * variables are the start and end dates of the tasks.
 */
public interface Schedule extends Problem<IntValue> {

    /**
     * Create a new activity in the schedule.
     * @param name
     *            Name of the activity
     * @return Name of the activity
     */
    Activity createActivity(final String name);

    /**
     * Create a new unary resource (or disjunctive resource).
     * @param name
     *            Name of the resource
     * @return Unary resource
     */
    UnaryResource createUnaryResource(final String name);

    /**
     * Create a constraint to make an activity end when another one starts.
     * @param activity
     *            Activity that ends at a specific date
     * @param date
     *            Date at which the activity ends
     * @return The time constraint on the activity
     */
    TimeConstraint endsAt(final Activity activity, final IntValue date);

    /**
     * Create a constraint to make an activity end when another one starts.
     * @param before
     *            Activity that ends before the start of the other one
     * @param after
     *            Activity that starts after the end of the other one
     * @param delay
     *            Fix delay between the end of the first activity and the start
     *            of the other one
     * @return The precedence constraint between the two activities
     */
    PrecedenceConstraint endsAtStart(final Activity before,
            final Activity after, final IntValue delay);

    /**
     * Create a constraint to make an activity end before another one starts.
     * @param before
     *            Activity that ends before the start of the other one
     * @param after
     *            Activity that starts after the end of the other one
     * @param delay
     *            Fix delay between the end of the first activity and the start
     *            of the other one
     * @return The precedence constraint between the two activities
     */
    PrecedenceConstraint endsBeforeStart(final Activity before,
            final Activity after, final IntValue delay);

    /**
     * Create a constraint to make an activity start at a specific date.
     * @param activity
     *            Activity that starts at a specific date
     * @param date
     *            Date at which the activity starts
     * @return The time constraint on the activity
     */
    TimeConstraint startAt(final Activity activity, final IntValue date);
}
