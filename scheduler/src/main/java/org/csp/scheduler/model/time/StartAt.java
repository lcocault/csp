/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.time;

import org.csp.scheduler.model.Activity;

import org.csp.constraint.model.integer.EqualToIntValue;
import org.csp.constraint.model.integer.IntValue;

/**
 * Represents a constraint on the start of an activity.
 */
public class StartAt extends TimeConstraint {

    /**
     * Constructor.
     * @param activity
     *            Activity constrained by a start date
     * @param start
     *            Fixed value for the activity's start date
     */
    public StartAt(final Activity activity, final IntValue start) {
        super(activity, new EqualToIntValue(activity.getStart(), start));
    }
}
