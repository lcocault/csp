/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.precedence;

import org.csp.scheduler.model.Activity;

import org.csp.constraint.model.BinaryConstraint;
import org.csp.constraint.model.integer.IntValue;

/**
 * Represents a precedence constraint between two activities.
 */
public abstract class PrecedenceConstraint {

    /**
     * First activity concerned by the precedence constraint.
     */
    private Activity first_;

    /**
     * Second activity concerned by the precedence constraint.
     */
    private Activity second_;

    /**
     * Constraint between the two activities.
     */
    private BinaryConstraint<IntValue> constraint_;

    /**
     * Constructor.
     * @param first
     *            First activity concerned by the precedence constraint
     * @param second
     *            Second activity concerned by the precedence constraint
     * @param constraint
     *            Constraint between the activities
     */
    public PrecedenceConstraint(final Activity first, final Activity second,
            final BinaryConstraint<IntValue> constraint) {
        first_ = first;
        second_ = second;
        constraint_ = constraint;
    }

    /**
     * Get the binary constraint associated with the precedence constraint.
     * @return Binary constraint
     */
    public BinaryConstraint<IntValue> getConstraint() {
        return constraint_;
    }

    /**
     * Get the first activity concerned by the precedence constraint.
     * @return First activity
     */
    public Activity getFirst() {
        return first_;
    }

    /**
     * Get the second activity concerned by the precedence constraint.
     * @return Second activity
     */
    public Activity getSecond() {
        return second_;
    }

}
