/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.precedence;

import org.csp.scheduler.model.Activity;

import org.csp.constraint.model.integer.GreaterEqual;
import org.csp.constraint.model.integer.IntValue;

/**
 * Represents a precedence constraint between two activities : the end of the
 * first one is before the start of the second one (with a minimum delay between
 * the two variables).
 */
public class EndBeforeStart extends PrecedenceConstraint {

    /**
     * Constructor.
     * @param before
     *            Activity that ends before the start of the other one
     * @param after
     *            Activity that starts after the end of the other one
     * @param delay
     *            Minimum delay between the end of the first activity and the
     *            start of the other one
     */
    public EndBeforeStart(final Activity before, final Activity after,
            final IntValue delay) {
        super(before, after, new GreaterEqual(after.getStart(),
                before.getEnd(), delay));
    }
}
