/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model;

import org.csp.constraint.model.Constraint;
import org.csp.constraint.model.integer.IntValue;
import org.csp.constraint.model.integer.IntVar;

/**
 * Represents an activity to schedule.
 */
public interface Activity {

    /**
     * Get the duration of the activity.
     * @return Duration variable
     */
    IntVar getDuration();

    /**
     * Get the duration constraint.
     * @return Constraint between the dates and the duration
     */
    Constraint<IntValue> getDurationConstraint();

    /**
     * Get the earliest end time of the activity.
     * @return Earliest End Time (noted EET)
     */
    IntValue getEarliestEndTime();

    /**
     * Get the earliest start time of the activity.
     * @return Earliest Start Time (noted EST)
     */
    IntValue getEarliestStartTime();

    /**
     * Get the end of the activity.
     * @return End variable
     */
    IntVar getEnd();

    /**
     * Get the latest end time of the activity.
     * @return Latest End Time (noted LET)
     */
    IntValue getLatestEndTime();

    /**
     * Get the latest start time of the activity.
     * @return Latest Start Time (noted LST)
     */
    IntValue getLatestStartTime();

    /**
     * Get the minimum processing time of the activity.
     * @return Minimum processing time
     */
    IntValue getMinProcessingTime();

    /**
     * Get the name of the activity.
     * @return Name of the activity
     */
    String getName();

    /**
     * Get the start of the activity.
     * @return Start variable
     */
    IntVar getStart();

    /**
     * Get the workload of the activity.
     * @return Workload variable
     */
    IntVar getProcessingTime();

}
