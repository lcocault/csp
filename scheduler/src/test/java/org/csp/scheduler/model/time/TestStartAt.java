/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.time;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.csp.constraint.model.integer.EqualToIntValue;
import org.csp.constraint.model.integer.IntValue;
import org.csp.scheduler.model.Activity;
import org.csp.scheduler.model.ActivityImpl;
import org.junit.Test;

public class TestStartAt {

    @Test
    public void testBasic() {

        // Mixture
        IntValue start = new IntValue(6);
        IntValue end = new IntValue(24);
        Activity activity = new ActivityImpl("A", start, end);
        IntValue date = new IntValue(15);

        // Constructor
        StartAt cst = new StartAt(activity, date);

        // Test getters
        assertEquals(activity, cst.getActivity());
        assertTrue(cst.getConstraint() instanceof EqualToIntValue);
        assertEquals("A/start=15", cst.getConstraint().toString());
    }

}
