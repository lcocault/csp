/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.resource;

import static org.junit.Assert.assertTrue;

import org.csp.constraint.model.integer.IntValue;
import org.csp.scheduler.model.Activity;
import org.csp.scheduler.model.ActivityImpl;
import org.junit.Test;

public class TestAscLatestEndTimeComparator {

    @Test
    public void testBasic() {

        // Mixture
        IntValue aStart = new IntValue(1);
        IntValue aEnd = new IntValue(9);
        IntValue bStart = new IntValue(3);
        IntValue bEnd = new IntValue(9);
        IntValue cStart = new IntValue(1);
        IntValue cEnd = new IntValue(8);
        Activity a = new ActivityImpl("A", aStart, aEnd);
        Activity b = new ActivityImpl("B", bStart, bEnd);
        Activity c = new ActivityImpl("C", cStart, cEnd);

        // Comparator
        AscLatestEndTimeComparator comparator = new AscLatestEndTimeComparator();

        // Compare !
        assertTrue(comparator.compare(a, b) == 0);
        assertTrue(comparator.compare(a, c) > 0);
        assertTrue(comparator.compare(c, b) < 0);

    }

}
