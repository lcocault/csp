/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model.resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.csp.constraint.model.InvalidAccessException;
import org.csp.constraint.model.integer.IntValue;
import org.csp.scheduler.model.Activity;
import org.csp.scheduler.model.ActivityImpl;
import org.junit.Test;

public class TestDisjunctiveConstraint {

    @Test
    public void testActivitiesManagement() {

        // Mixture
        UnaryResource resource = new UnaryResource("Res");
        IntValue aStart = new IntValue(1);
        IntValue aEnd = new IntValue(5);
        IntValue bStart = new IntValue(4);
        IntValue bEnd = new IntValue(7);
        IntValue cStart = new IntValue(5);
        IntValue cEnd = new IntValue(9);
        Activity a = new ActivityImpl("A", aStart, aEnd);
        Activity b = new ActivityImpl("B", bStart, bEnd);
        Activity c = new ActivityImpl("C", cStart, cEnd);

        // Constraint
        DisjunctiveConstraint cst = new DisjunctiveConstraint(
                resource);

        // Test constraint properties
        assertEquals("Res", cst.getName());

        // Add activities
        cst.addActivity(a);
        cst.addActivity(b);
        // TODO Test activities

        // Attempt to add/remove an activity with a propagating constraint
        cst.enablePropagate(true);
        try {
            cst.addActivity(c);
            fail();
        } catch (InvalidAccessException e) {
            // Nominal path
        }

    }

    @Test
    public void testPropagation() {

        // Mixture
        UnaryResource resource = new UnaryResource("Res");
        IntValue aStart = new IntValue(1);
        IntValue aEnd = new IntValue(5);
        IntValue bStart = new IntValue(4);
        IntValue bEnd = new IntValue(7);
        IntValue cStart = new IntValue(5);
        IntValue cEnd = new IntValue(10);
        IntValue minDuration = new IntValue(3);
        IntValue maxDuration = new IntValue(4);
        Activity a = new ActivityImpl("A", aStart, aEnd);
        Activity b = new ActivityImpl("B", bStart, bEnd);
        Activity c = new ActivityImpl("C", cStart, cEnd);
        a.getDuration().reduceWithMinValue(minDuration);
        a.getDuration().reduceWithMaxValue(maxDuration);
        b.getDuration().reduceWithMinValue(minDuration);
        b.getDuration().reduceWithMaxValue(maxDuration);
        c.getDuration().reduceWithMinValue(minDuration);
        c.getDuration().reduceWithMaxValue(maxDuration);

        // Propagate the activity internal constraints
        a.getDurationConstraint().enablePropagate(true);
        a.getDurationConstraint().revise();
        b.getDurationConstraint().enablePropagate(true);
        b.getDurationConstraint().revise();
        c.getDurationConstraint().enablePropagate(true);
        c.getDurationConstraint().revise();

        // Constraint
        DisjunctiveConstraint cst = new DisjunctiveConstraint(
                resource);

        // Add activities
        cst.addActivity(a);
        cst.addActivity(b);
        cst.addActivity(c);

        // Propagate the constraint
        cst.enablePropagate(true);
        assertTrue(cst.revise());

        // Second round
        a.getDurationConstraint().revise();
        b.getDurationConstraint().revise();
        c.getDurationConstraint().revise();
        assertTrue(cst.revise());        
        
        // Check assignments
        assertEquals(new IntValue(1), a.getStart().getValue());
        assertEquals(new IntValue(4), a.getEnd().getValue());
        assertEquals(new IntValue(4), b.getStart().getValue());
        assertEquals(new IntValue(7), b.getEnd().getValue());
        assertEquals(new IntValue(7), c.getStart().getValue());
        assertEquals(new IntValue(10), c.getEnd().getValue());
    }

    @Test
    public void testPropagationFail() {

        // Mixture
        UnaryResource resource = new UnaryResource("Res");
        IntValue aStart = new IntValue(1);
        IntValue aEnd = new IntValue(5);
        IntValue bStart = new IntValue(4);
        IntValue bEnd = new IntValue(6);
        IntValue minDuration = new IntValue(3);
        IntValue maxDuration = new IntValue(4);
        Activity a = new ActivityImpl("A", aStart, aEnd);
        Activity b = new ActivityImpl("B", bStart, bEnd);
        a.getDuration().reduceWithMinValue(minDuration);
        a.getDuration().reduceWithMaxValue(maxDuration);
        b.getDuration().reduceWithMinValue(minDuration);
        b.getDuration().reduceWithMaxValue(maxDuration);

        // Propagate the activity internal constraints
        a.getDurationConstraint().enablePropagate(true);
        a.getDurationConstraint().revise();
        b.getDurationConstraint().enablePropagate(true);
        b.getDurationConstraint().revise();

        // Constraint
        DisjunctiveConstraint cst = new DisjunctiveConstraint(
                resource);

        // Add activities
        cst.addActivity(a);
        cst.addActivity(b);

        // Propagate the constraint
        cst.enablePropagate(true);
        assertFalse(cst.revise());

    }

}
