/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.csp.scheduler.model.Schedule;
import org.csp.scheduler.model.resource.UnaryResource;
import org.junit.Test;
import org.csp.constraint.model.integer.IntValue;

public class TestSchedule {

    @Test
    public void testTimeConstraints() {

        // Create a schedule
        Schedule schedule = new ScheduleImpl("TEST", new IntValue(0),
                new IntValue(10));

        // Add activities to the schedule
        Activity a1 = schedule.createActivity("A1");
        Activity a2 = schedule.createActivity("A2");
        Activity a3 = schedule.createActivity("A3");

        // Set the start of A1
        IntValue start = new IntValue(1);
        schedule.startAt(a1, start);

        // Set the end of A2
        IntValue end = new IntValue(9);
        schedule.endsAt(a2, end);

        // Create precedence constraints between A1 and A2
        IntValue delay = new IntValue(2);
        schedule.endsAtStart(a1, a2, delay);

        // Create precedence constraints between A1 and A3
        schedule.endsBeforeStart(a1, a3, delay);

        // Propagate constraints
        assertTrue(schedule.startSearch());

        // ...0.1.2.3.4.5.6.7.8.9.0
        //
        // A^...x..................
        // A$...x.x.x.x.x.x.x.x.x.x
        //
        // B^.......x.x.x.x.x.x.x..
        // B$...................x..
        //
        // C^.......x.x.x.x.x.x.x.x
        // C$.......x.x.x.x.x.x.x.x

        // Check the activities
        assertEquals(start, a1.getStart().getValue());
        assertEquals(new IntValue(7), a1.getEnd().getMaxValue());
        assertEquals(end, a2.getEnd().getValue());
        assertEquals(new IntValue(3), a2.getStart().getMinValue());
        assertEquals(new IntValue(3), a3.getStart().getMinValue());
        assertEquals(new IntValue(10), a3.getEnd().getMaxValue());
    }

    @Test
    public void testResourceConstraints() {

        // Create a schedule
        Schedule schedule = new ScheduleImpl("TEST", new IntValue(0),
                new IntValue(10));

        // Add resource to the schedule
        UnaryResource unary = schedule.createUnaryResource("UR");
        assertEquals("UR", unary.getName());

        // TODO Complete the test with disjunctive scheduling
    }
}
