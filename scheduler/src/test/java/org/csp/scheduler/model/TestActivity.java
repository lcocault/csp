/* Copyright 2015 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.csp.scheduler.model;

import static org.junit.Assert.assertEquals;

import org.csp.constraint.model.integer.IntValue;
import org.junit.Test;

public class TestActivity {

    @Test
    public void testBasic() {

        // Mixture
        IntValue start = new IntValue(6);
        IntValue end = new IntValue(24);
        IntValue durationMin = new IntValue(0);
        IntValue durationMax = new IntValue(18);

        // Constructor
        Activity activity = new ActivityImpl("Coding", start, end);

        // Test getters
        assertEquals("Coding", activity.getName());
        assertEquals(start, activity.getEarliestEndTime());
        assertEquals(start, activity.getEarliestStartTime());
        assertEquals(end, activity.getLatestEndTime());
        assertEquals(end, activity.getLatestStartTime());
        assertEquals(start, activity.getStart().getMinValue());
        assertEquals(end, activity.getStart().getMaxValue());
        assertEquals(start, activity.getEnd().getMinValue());
        assertEquals(end, activity.getEnd().getMaxValue());
        assertEquals(durationMin, activity.getDuration().getMinValue());
        assertEquals(durationMax, activity.getDuration().getMaxValue());
        assertEquals(durationMin, activity.getMinProcessingTime());
        assertEquals(activity.getDuration(), activity.getProcessingTime());
        assertEquals("Coding/start+Coding/duration=Coding/end", activity
                .getDurationConstraint().toString());
    }

}
